package com.mbayennapp.diamalayetv.Tab;

import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mbayennapp.diamalayetv.Model.Fondateur;
import com.mbayennapp.diamalayetv.R;
import com.mbayennapp.diamalayetv.Utils.ConnexionInternet;

/**
 * Created by bfali on 27/01/2017.
 */

public class tabFondateur extends Fragment {
    private TextView txt_titre;
    private TextView txt_description;
    private ConnexionInternet connexionInternet;
    /*===================================================================*/
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private DocumentReference dbuser;
    private Fondateur fondateur;
    /*===================================================================*/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tabfondateur, container, false);
        txt_description = (TextView) rootView.findViewById(R.id.txt_description_fondateur);
        txt_titre = (TextView)rootView.findViewById(R.id.txt_titre_fondateur);
        /*=================================================================*/
        Typeface typeFace = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeueLTStd_Roman.otf");
        Typeface typegold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeuecondenced.otf");
        txt_description.setTypeface(typeFace);
        txt_titre.setTypeface(typegold);
        /*=================================================================*/
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        /*=================================================================*/
        connexionInternet = new ConnexionInternet();
        if (!connexionInternet.isConnectedInternet(getActivity())){
            final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(getActivity());
            alertDialogBuilder.setMessage("Votre appareil n'est pas connecté à internet. Pour continuer à utiliser l'application, vérifier cotre connexion.");

            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.hide();
                    getActivity().finish();
                }
            });

            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }else {
            get_fondateur();
        }
        return rootView;
    }
    /*===========================================================*/
    private void get_fondateur(){
        //Query query = db.collection("Articles").orderBy("date", Query.Direction.DESCENDING);
        DocumentReference d_fond = db.collection("Confrerie").document("fondateur");
        d_fond.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                DocumentSnapshot fond = task.getResult();
                if (!fond.getId().isEmpty()) {
                    fondateur = new Fondateur();
                    fondateur.setTitre(fond.get("titre").toString());
                    fondateur.setDescription(fond.get("description").toString());
                    fondateur.setImage(fond.get("image").toString());
                }
                txt_titre.setText(fondateur.getTitre());
                txt_description.setText(fondateur.getDescription());
            }
        });
    }
    /*===========================================================*/
}
