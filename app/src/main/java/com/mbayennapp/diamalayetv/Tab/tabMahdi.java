package com.mbayennapp.diamalayetv.Tab;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mbayennapp.diamalayetv.Adapter.Mahdi_Adapter;
import com.mbayennapp.diamalayetv.Model.Mahdi;
import com.mbayennapp.diamalayetv.R;
import com.mbayennapp.diamalayetv.Utils.ConnexionInternet;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bfali on 27/01/2017.
 */

public class tabMahdi extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    private String lien = "https://www.mabcompany.com/JL_API/";
    private String token = "layenne2017";
    private SwipeRefreshLayout swipe_mahdi;
    private ListView list_mahdis;
    private List<Mahdi> mahdis = new ArrayList<>();
    private Mahdi_Adapter adapter;
    private ConnexionInternet connexionInternet;
    private Mahdi mahdi;
    /*=====================================================================*/
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private DocumentReference dbuser;
    /*=====================================================================*/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tabmahdi, container, false);
        swipe_mahdi = (SwipeRefreshLayout)rootView.findViewById(R.id.swipe_mahdi);
        list_mahdis = (ListView)rootView.findViewById(R.id.list_mahdi);
        /*=================================================================*/
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        /*=================================================================*/
        connexionInternet = new ConnexionInternet();
        if (!connexionInternet.isConnectedInternet(getActivity())){
            final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(getActivity());
            alertDialogBuilder.setMessage("Votre appareil n'est pas connecté à internet. Pour continuer à utiliser l'application, vérifier cotre connexion.");

            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.hide();
                    getActivity().finish();
                }
            });

            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }else {
            swipe_mahdi.setOnRefreshListener(tabMahdi.this);
            swipe_mahdi.setColorSchemeColors(Color.parseColor("#01315a"));
            swipe_mahdi.setRefreshing(true);

            get_Mahdi();
            get_Coran();
            get_Sunna();

             /*===================================================================*/
            new CountDownTimer(1000,500){

                @Override
                public void onTick(long l) {

                }

                @Override
                public void onFinish() {
                    adapter = new Mahdi_Adapter(getActivity(),mahdis);
                    list_mahdis.setAdapter(adapter);
                    swipe_mahdi.setRefreshing(false);
                }
            }.start();
        }
        return rootView;
    }

    /*===========================================================*/
    private void get_Mahdi(){
        //Query query = db.collection("Articles").orderBy("date", Query.Direction.DESCENDING);
        DocumentReference d_fond = db.collection("Confrerie").document("Mahdi");
        d_fond.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                DocumentSnapshot fond = task.getResult();
                if (!fond.getId().isEmpty()) {
                    mahdi = new Mahdi();
                    mahdi.setTitre(fond.get("titre").toString());
                    mahdi.setContent(fond.get("content").toString());
                    mahdis.add(mahdi);
                }
            }
        });
    }
    private void get_Coran(){
        //Query query = db.collection("Articles").orderBy("date", Query.Direction.DESCENDING);
        DocumentReference d_fond = db.collection("Confrerie").document("Mahdi_Coran");
        d_fond.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                DocumentSnapshot fond = task.getResult();
                if (!fond.getId().isEmpty()) {
                    mahdi = new Mahdi();
                    mahdi.setTitre(fond.get("titre").toString());
                    mahdi.setContent(fond.get("content").toString());
                    mahdis.add(mahdi);
                }
            }
        });
    }
    private void get_Sunna(){
        //Query query = db.collection("Articles").orderBy("date", Query.Direction.DESCENDING);
        DocumentReference d_fond = db.collection("Confrerie").document("Mahdi_Sunna");
        d_fond.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                DocumentSnapshot fond = task.getResult();
                if (!fond.getId().isEmpty()) {
                    mahdi = new Mahdi();
                    mahdi.setTitre(fond.get("titre").toString());
                    mahdi.setContent(fond.get("content").toString());
                    mahdis.add(mahdi);
                }
            }
        });
    }
    /*===========================================================*/

    @Override
    public void onRefresh() {
        swipe_mahdi.postDelayed(new Runnable() {
            @Override
            public void run() {
                mahdis.clear();
                get_Mahdi();
                get_Coran();
                get_Sunna();
                adapter.notifyDataSetChanged();
                swipe_mahdi.setRefreshing(false);
            }
        },2000);
    }
}
