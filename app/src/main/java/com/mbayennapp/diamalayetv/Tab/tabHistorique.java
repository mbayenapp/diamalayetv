package com.mbayennapp.diamalayetv.Tab;

import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mbayennapp.diamalayetv.Model.Historique;
import com.mbayennapp.diamalayetv.R;
import com.mbayennapp.diamalayetv.Utils.ConnexionInternet;

/**
 * Created by bfali on 27/01/2017.
 */

public class tabHistorique extends Fragment {
    private TextView txt_context_historique;
    private TextView txt_content;
    private ConnexionInternet connexionInternet;
    /*===================================================================*/
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private DocumentReference dbuser;
    private Historique historique;
    /*===================================================================*/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tabhistorique, container, false);
        /*=================================================================*/
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        /*=================================================================*/
        connexionInternet = new ConnexionInternet();
        if (!connexionInternet.isConnectedInternet(getActivity())){
            final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(getActivity());
            alertDialogBuilder.setMessage("Votre appareil n'est pas connecté à internet. Pour continuer à utiliser l'application, vérifier cotre connexion.");

            alertDialogBuilder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    android.app.AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.hide();
                    getActivity().finish();
                }
            });

            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }else {
            txt_context_historique = (TextView)rootView.findViewById(R.id.txt_context_historique);
            txt_content = (TextView) rootView.findViewById(R.id.txt_historique);
            /*=================================================================*/
            Typeface typeFace = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeueLTStd_Roman.otf");
            Typeface typegold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeuecondenced.otf");
            txt_content.setTypeface(typeFace);
            txt_context_historique.setTypeface(typegold);
            /*=================================================================*/
            get_historique();
        }
        return rootView;
    }
    /*===========================================================*/
    private void get_historique(){
        //Query query = db.collection("Articles").orderBy("date", Query.Direction.DESCENDING);
        DocumentReference d_fond = db.collection("Confrerie").document("Historique");
        d_fond.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                DocumentSnapshot fond = task.getResult();
                if (!fond.getId().isEmpty()) {
                    historique = new Historique();
                    historique.setContent(fond.get("content").toString());
                }
                txt_content.setText(historique.getContent());
            }
        });
    }
    /*===========================================================*/
}
