package com.mbayennapp.diamalayetv.Helpers;

import android.content.Context;
import android.content.SharedPreferences;

import com.mbayennapp.diamalayetv.Model.Users;

/**
 * Created by Mbaye on 15/08/2017.
 */

public class SessionManagerLogin {
    private static String TAG = SessionManagerLogin.class.getSimpleName();
    // Shared Preferences
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    // Shared pref mode
    int PRIVATE_MODE = 0;
    // Shared preferences file name
    private static final String PREF_NAME = "LoginUserDetails";
    private static final String KEY_IS_LOGGEDIN = "statutUser";

    public SessionManagerLogin(Context _context) {
        this._context = _context;
        pref = _context.getSharedPreferences(PREF_NAME,PRIVATE_MODE);
    }

    public void storeUserData(Users users){
        editor = pref.edit();
        editor.putString("uid",users.getId());
        editor.putString("tel",users.getTel());
        editor.putString("nom",users.getNom());
        editor.putString("prenom",users.getPrenom());
        editor.putString("adresse",users.getAdresse());
        editor.putString("codepostal",users.getCode_postal());
        editor.putString("indicatif",users.getIndicatif());
        editor.putString("pays",users.getPays());
        editor.putString("ville",users.getVille());
        editor.putString("email",users.getEmail());
        editor.putString("profil",users.getProfil());
        editor.putString("sexe",users.getSexe());
        editor.putString("type",users.getType());
        editor.commit();
    }
    public Users getLoggedInUser(){
        String uid = pref.getString("uid","");
        String tel = pref.getString("tel","");
        String nom = pref.getString("nom","");
        String prenom = pref.getString("prenom","");
        String adresse = pref.getString("adresse","");
        String codepostal = pref.getString("codepostal","");
        String indicatif = pref.getString("indicatif","");
        String pays = pref.getString("pays","");
        String ville = pref.getString("ville","");
        String profil = pref.getString("profil","");
        String email = pref.getString("email","");
        String sexe = pref.getString("sexe","");
        String type = pref.getString("type","");
        Users users = new Users(uid,nom,prenom,indicatif,tel,adresse,codepostal,ville,pays,email,profil,sexe,type);
        return users;
    }

    public void manageProfil(Users users){
        editor = pref.edit();
        editor.putString("tel",users.getTel());
        editor.putString("nom",users.getNom());
        editor.putString("prenom",users.getPrenom());
        editor.putString("adresse",users.getAdresse());
        editor.putString("indicatif",users.getIndicatif());
        editor.putString("sexe",users.getSexe());
        editor.commit();
    }

    public void manageImgProfil(String profil){
        editor = pref.edit();
        editor.putString("profil",profil);
        editor.putString("type","profil");
        editor.commit();
    }

    public void setLoggedInUserLogin(boolean loggedIn){
        editor = pref.edit();
        editor.putBoolean("LoggedIn",loggedIn);
        editor.commit();
    }
    public void clearUserLoginData(){
        editor = pref.edit();
        editor.clear();
        editor.commit();
    }
    public boolean isLoggedIn(){
        String tel = getLoggedInUser().getEmail();
        boolean statut = pref.getBoolean(KEY_IS_LOGGEDIN, false);
        if (tel != ""){
            statut = pref.getBoolean(KEY_IS_LOGGEDIN, true);
        }
        return statut;
    }
}
