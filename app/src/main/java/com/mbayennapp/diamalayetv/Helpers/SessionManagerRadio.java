package com.mbayennapp.diamalayetv.Helpers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by bfali on 27/01/2017.
 */

public class SessionManagerRadio {
    private static String TAG = SessionManagerRadio.class.getSimpleName();
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "RadioDetails";
    private static final String KEY_IS_LOGGEDIN = "statutRadio";

    public SessionManagerRadio(Context _context) {
        this._context = _context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
    }
    public void storeStatutRadio(String statut){
        editor = pref.edit();
        editor.putString("statut",statut);
        editor.commit();
    }
    public String getStatutRadio(){
        String statut = pref.getString("statut","");
        return statut;
    }
    public void setLoggedInUser(boolean loggedIn){
        editor = pref.edit();
        editor.putBoolean("LoggedIn",loggedIn);
        editor.commit();
    }
    public void clearStatutRadion(){
        editor = pref.edit();
        editor.clear();
        editor.commit();
    }
    public boolean isLoggedIn(){
        String statut = getStatutRadio();
        boolean st = pref.getBoolean(KEY_IS_LOGGEDIN, false);
        if (!statut.equals("")){
            st = pref.getBoolean(KEY_IS_LOGGEDIN, true);
        }
        return st;
    }
}
