package com.mbayennapp.diamalayetv.Helpers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Mbaye on 06/09/2017.
 */

public class SessionManagerRegister {
    private static String TAG = SessionManagerRegister.class.getSimpleName();
    // Shared Preferences
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    // Shared pref mode
    int PRIVATE_MODE = 0;
    // Shared preferences file name
    private static final String PREF_NAME = "LoginRegisterDetails";
    private static final String KEY_IS_LOGGEDIN = "statutRegister";

    public SessionManagerRegister(Context _context) {
        this._context = _context;
        pref = _context.getSharedPreferences(PREF_NAME,PRIVATE_MODE);
    }

    public void storeRegisterData(String tel){
        editor = pref.edit();
        editor.putString("tel",tel);
        editor.putString("step","one");
        editor.commit();
    }
    public  String getLoggedInRegisterTel(){
        String tel = pref.getString("tel","");
        return tel;
    }
    public String getLoggedInRegisterStep(){
        String step = pref.getString("step","");
        return step;
    }

    public void manageRegisterStep(String step){
        editor = pref.edit();
        editor.putString("step",step);
        editor.commit();
    }

    public void setLoggedInUserLogin(boolean loggedIn){
        editor = pref.edit();
        editor.putBoolean("LoggedIn",loggedIn);
        editor.commit();
    }
    public void clearRegisterData(){
        editor = pref.edit();
        editor.clear();
        editor.commit();
    }
    public boolean isLoggedIn(){
        String tel = getLoggedInRegisterTel();
        boolean statut = pref.getBoolean(KEY_IS_LOGGEDIN, false);
        if (tel != ""){
            statut = pref.getBoolean(KEY_IS_LOGGEDIN, true);
        }
        return statut;
    }
}
