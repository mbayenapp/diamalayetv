package com.mbayennapp.diamalayetv.Fragments;

import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mbayennapp.diamalayetv.Model.Issas;
import com.mbayennapp.diamalayetv.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Issa.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Issa#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Issa extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private String lien = "https://www.mabcompany.com/JL_API/";
    private String token = "layenne2017";
    private ImageView img_issa,img_tempe;
    private TextView txt_titre_issa,txt_content_issa;
    private SwipeRefreshLayout swipe_issa;
    private Issas issa;
    /*=====================================================================*/
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private DocumentReference dbuser;
    /*=====================================================================*/
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public Issa() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Issa.
     */
    // TODO: Rename and change types and number of parameters
    public static Issa newInstance(String param1, String param2) {
        Issa fragment = new Issa();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_issa, container, false);
        /*===================================================================*/
        img_issa = (ImageView)view.findViewById(R.id.img_issa);
        img_tempe = (ImageView)view.findViewById(R.id.img_tempe);
        txt_titre_issa = (TextView)view.findViewById(R.id.txt_titre_issa);
        txt_content_issa = (TextView)view.findViewById(R.id.txt_content_issa);
        swipe_issa = (SwipeRefreshLayout)view.findViewById(R.id.swipe_issa);
        /*===================================================================*/
        /*=================================================================*/
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        /*=================================================================*/
        Typeface typeFace = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeueLTStd_Roman.otf");
        Typeface typegold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeuecondenced.otf");
        txt_titre_issa.setTypeface(typegold);
        txt_content_issa.setTypeface(typeFace);
        /*===================================================================*/
        swipe_issa.setOnRefreshListener(Issa.this);
        swipe_issa.setColorSchemeColors(Color.parseColor("#01315a"));
        swipe_issa.setRefreshing(true);
        get_Issa();
        /*===================================================================*/
        return view;
    }

    private void get_Issa(){
        //Query query = db.collection("Articles").orderBy("date", Query.Direction.DESCENDING);
        db.collection("Issa").document("1").get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                DocumentSnapshot fond = task.getResult();
                if (!fond.getId().isEmpty()) {
                    try {
                        issa = new Issas();
                        issa.setTitre(fond.get("titre").toString());
                        issa.setContent(fond.get("content").toString());
                        issa.setImage(fond.get("image").toString());
                        issa.setLogo(fond.get("logo").toString());

                        String link_img = issa.getImage();
                        Glide.with(Issa.this).load(link_img).crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(img_issa);
                        String link = issa.getLogo();
                        Glide.with(Issa.this).load(link).crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(img_tempe);
                        txt_titre_issa.setText(issa.getTitre() + " (PSL)");
                        txt_content_issa.setText(issa.getContent());
                        swipe_issa.setRefreshing(false);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRefresh() {
        swipe_issa.postDelayed(new Runnable() {
            @Override
            public void run() {
                swipe_issa.setRefreshing(false);
            }
        },2000);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    /*=====================================================================*/
}
