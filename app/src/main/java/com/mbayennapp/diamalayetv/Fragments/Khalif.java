package com.mbayennapp.diamalayetv.Fragments;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.mbayennapp.diamalayetv.Activity.See_khalif;
import com.mbayennapp.diamalayetv.Adapter.Khalifs_Adapter;
import com.mbayennapp.diamalayetv.Model.Khalifs;
import com.mbayennapp.diamalayetv.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Khalif.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Khalif#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Khalif extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    private String lien = "https://www.mabcompany.com/JL_API/";
    private String token = "layenne2017";
    private SwipeRefreshLayout swipeKhalif;
    private ListView list_khalif;
    private Khalifs_Adapter adapter;
    private List<Khalifs> khalifs = new ArrayList<>();
    private Khalifs khalif;

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private DocumentReference darticles;
    /*=========================================================*/
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public Khalif() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Khalif.
     */
    // TODO: Rename and change types and number of parameters
    public static Khalif newInstance(String param1, String param2) {
        Khalif fragment = new Khalif();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_khalif, container, false);
        /*=========================================================*/
        swipeKhalif = (SwipeRefreshLayout)view.findViewById(R.id.swipeKhalif);
        list_khalif = (ListView)view.findViewById(R.id.list_khalif);
        /*=========================================================*/
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        /*=========================================================*/
        swipeKhalif.setOnRefreshListener(this);
        swipeKhalif.setColorSchemeColors(Color.parseColor("#01315a"));
        swipeKhalif.setRefreshing(true);
        get_khalifs();
        /*=========================================================*/
        list_khalif.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                final Khalifs kh = khalifs.get(i);
                Intent intent = new Intent(getContext(), See_khalif.class);
                intent.putExtra("id", kh.getId());
                intent.putExtra("nom", kh.getNom());
                intent.putExtra("date_in", kh.getDate_in());
                intent.putExtra("date_out", kh.getDate_out());
                intent.putExtra("description", kh.getDescription());
                intent.putExtra("photo", kh.getPhoto());
                startActivity(intent);
            }
        });
        /*=========================================================*/
        return view;
    }

    private void get_khalifs(){
        //Query query = db.collection("Articles").orderBy("date", Query.Direction.DESCENDING);
        db.collection("Khalifs").orderBy("date_in", Query.Direction.DESCENDING).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        if (!document.getId().isEmpty()) {
                                khalif = new Khalifs();
                                khalif.setDate_in(document.get("date_in").toString());
                                khalif.setDate_out(document.get("date_out").toString());
                                khalif.setDescription(document.get("description").toString());
                                khalif.setPhoto(document.get("photo").toString());
                                khalif.setNom(document.get("nom").toString());
                                khalifs.add(khalif);
                        }
                    }
                    if (!khalifs.isEmpty()){
                        adapter = new Khalifs_Adapter(getContext(), khalifs);
                        list_khalif.setAdapter(adapter);
                        swipeKhalif.setRefreshing(false);
                    }
                } else {
                    //Log.d(TAG, "Error: " + task.getException());
                }
            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRefresh() {
        swipeKhalif.postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeKhalif.setRefreshing(false);
            }
        },2000);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    /*===================================================================*/

}
