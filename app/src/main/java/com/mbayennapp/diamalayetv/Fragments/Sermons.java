package com.mbayennapp.diamalayetv.Fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.mbayennapp.diamalayetv.Adapter.Sermons_Adapter;
import com.mbayennapp.diamalayetv.Model.Sermon;
import com.mbayennapp.diamalayetv.R;
import com.mbayennapp.diamalayetv.Utils.ConnexionInternet;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Sermons.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Sermons#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Sermons extends Fragment {
    private static final String TAG = Sermons.class.getSimpleName();
    private String lien = "https://www.mabcompany.com/JL_API/";
    private String token = "layenne2017";
    private ConnexionInternet connexionInternet;
    private LinearLayout l_progress,l_sermon;
    private ListView list_sermons;
    private List<Sermon> sermons = new ArrayList<>();
    private Sermon sermon;
    private Sermons_Adapter adapter;

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private DocumentReference darticles;
    /*===========================================================*/
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public Sermons() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Sermons.
     */
    // TODO: Rename and change types and number of parameters
    public static Sermons newInstance(String param1, String param2) {
        Sermons fragment = new Sermons();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sermons, container, false);
        l_progress = (LinearLayout)view.findViewById(R.id.l_progress_mention);
        l_sermon = (LinearLayout)view.findViewById(R.id.l_sermon);
        list_sermons = (ListView)view.findViewById(R.id.list_sermons);
        /*============================================================*/
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        /*============================================================*/
        get_sermons();
        /*============================================================*/
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    private void get_sermons(){
        //Query query = db.collection("Articles").orderBy("date", Query.Direction.DESCENDING);
        db.collection("Sermons").orderBy("id", Query.Direction.ASCENDING).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        if (!document.getId().isEmpty()) {
                            try {
                                sermon = new Sermon();
                                sermon.setId(Integer.parseInt(document.get("id").toString()));
                                sermon.setIntroduction(document.get("introduction").toString());
                                sermon.setTitre(document.get("titre").toString());
                                sermon.setTitre_intitule(document.get("titre_intitule").toString());
                                sermon.setText_intitule(document.get("text_intitule").toString());
                                sermon.setProprietaire(document.get("proprietaire").toString());
                                sermon.setSermon(document.get("sermon").toString());
                                sermon.setTitre_sermon(document.get("titre_sermon").toString());
                                sermons.add(sermon);
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    }
                    if (!sermons.isEmpty()){
                        adapter = new Sermons_Adapter(getContext(),sermons);
                        list_sermons.setAdapter(adapter);
                        l_progress.setVisibility(View.GONE);
                        l_sermon.setVisibility(View.VISIBLE);
                    }
                } else {
                    //Log.d(TAG, "Error: " + task.getException());
                }
            }
        });
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
