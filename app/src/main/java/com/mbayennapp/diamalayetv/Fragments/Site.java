package com.mbayennapp.diamalayetv.Fragments;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.mbayennapp.diamalayetv.Activity.See_site;
import com.mbayennapp.diamalayetv.Adapter.Site_Adapter;
import com.mbayennapp.diamalayetv.Model.Sites;
import com.mbayennapp.diamalayetv.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Site.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Site#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Site extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    private String lien = "https://www.mabcompany.com/JL_API/";
    private String token = "layenne2017";
    private ListView list_site;
    private SwipeRefreshLayout swipe_site;
    private List<Sites> sites = new ArrayList<>();
    private Sites site;
    private Site_Adapter adapter;
    /*==================================================================*/
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private DocumentReference dvideos;
    /*=================================================================*/
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public Site() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Site.
     */
    // TODO: Rename and change types and number of parameters
    public static Site newInstance(String param1, String param2) {
        Site fragment = new Site();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sites, container, false);
        /*===============================================================*/
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        /*===============================================================*/
        list_site = (ListView)view.findViewById(R.id.list_site);
        swipe_site = (SwipeRefreshLayout)view.findViewById(R.id.swipe_site);
        /*===============================================================*/
        //sites = get_sites();
        /*===============================================================*/
        swipe_site.setOnRefreshListener(this);
        swipe_site.setColorSchemeColors(Color.parseColor("#01315a"));
        swipe_site.setRefreshing(true);
        get_allsites();

        list_site.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Sites site = sites.get(i);
                Intent intent = new Intent(getContext(), See_site.class);
                intent.putExtra("id",site.getId());
                intent.putExtra("titre",site.getTitre());
                intent.putExtra("description",site.getDescription());
                intent.putExtra("photo",site.getImage());
                startActivity(intent);
            }
        });
        /*===============================================================*/
        return view;
    }

    /*====================================================================*/
    private void get_allsites(){
        db.collection("Sites").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        site = new Sites();
                        site.setDescription(document.get("description").toString());
                        site.setImage(document.get("image").toString());
                        site.setTitre(document.get("titre").toString());
                        sites.add(site);
                    }
                    adapter = new Site_Adapter(getContext(), sites);
                    list_site.setAdapter(adapter);
                    swipe_site.setRefreshing(false);
                } else {
                    //Log.d(TAG, "Error: " + task.getException());
                }
            }
        });
    }
    /*=======================================================================*/

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRefresh() {
        swipe_site.postDelayed(new Runnable() {
            @Override
            public void run() {
                sites.clear();
                get_allsites();
                adapter.notifyDataSetChanged();
                swipe_site.setRefreshing(false);
            }
        },2000);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    /*==================================================================*/
    private List<Sites> get_sites(){
        List<Sites> sites = new ArrayList<>();
        String result ="";
        Sites site;
        OkHttpClient client = new OkHttpClient();
        //
        RequestBody body = new FormBody.Builder()
                .add("token",token)
                .build();

        final Request request = new Request.Builder()
                .url(lien+"Sites/list_sites.php")
                .addHeader("Content-Type","text/json; Charset=UTF-8")
                .post(body)
                .build();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Response response = null;
        try {
            response = client.newCall(request).execute();
            if (response.code() == 200) {
                result = response.body().string();
                if (result != null) {
                    JSONArray jsonArray = new JSONArray(result);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        site = new Sites();
                        site.setId(jsonObject.optInt("id"));
                        site.setTitre(jsonObject.optString("titre"));
                        site.setDescription(jsonObject.optString("description"));
                        site.setImage(jsonObject.optString("image"));
                        sites.add(site);
                    }
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (response != null){
                response.body().close();
            }
        }
        return sites;
    }
}
