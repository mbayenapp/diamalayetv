package com.mbayennapp.diamalayetv.Fragments;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.mbayennapp.diamalayetv.Activity.See_more_article;
import com.mbayennapp.diamalayetv.Activity.See_more_video;
import com.mbayennapp.diamalayetv.Adapter.Actuality_Adapter;
import com.mbayennapp.diamalayetv.Model.Articles;
import com.mbayennapp.diamalayetv.R;
import com.mbayennapp.diamalayetv.Utils.ConnexionInternet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link home_fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link home_fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class home_fragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    /*===============================================================*/
    private String lien = "https://www.mabcompany.com/JL_API/";
    private String token = "layenne2017";
    private SwipeRefreshLayout swipe_actualite;
    private ListView list_actualite;
    private List<Articles> articles = new ArrayList<>();
    private Articles article;
    private ConnexionInternet connexionInternet;
    private Actuality_Adapter adapter;
    private static final String TAG = home_fragment.class.getSimpleName();
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private DocumentReference darticles;
    private ShimmerFrameLayout mShimmerViewContainer;
    /*===============================================================*/

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public home_fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment home_fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static home_fragment newInstance(String param1, String param2) {
        home_fragment fragment = new home_fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        /*================================================================================*/
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        /*================================================================================*/
        swipe_actualite = (SwipeRefreshLayout) view.findViewById(R.id.swipe_actualite);
        list_actualite = (ListView) view.findViewById(R.id.lst_actualite);
        connexionInternet = new ConnexionInternet();
        swipe_actualite.setOnRefreshListener(this);
        swipe_actualite.setColorSchemeColors(Color.parseColor("#00796b"));
        //swipe_actualite.setRefreshing(true);
        mShimmerViewContainer = (ShimmerFrameLayout) view.findViewById(R.id.shimmer_view_container);
        if (connexionInternet.isConnectedInternet(getActivity())) {
            //Retourner toutes les articles
            get_all_articles();
        }
        /*================================================================================*/
        if (connexionInternet.isConnectedInternet(getActivity())) {
            //articles = get_Articles();

            //Action du click sur un item de la listView
            /*============================================================*/
            list_actualite.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Articles article = articles.get(i);
                    if (article.getType().equals("article")) {
                        //Aller vers l'activité voire plus d'un article
                        Intent intent = new Intent(getContext(), See_more_article.class);
                        intent.putExtra("adid", article.getAdid());
                        intent.putExtra("date_sent", article.getDate_sent());
                        intent.putExtra("titre", article.getTitre());
                        intent.putExtra("content", article.getContent());
                        intent.putExtra("lieu", article.getLieu());
                        intent.putExtra("photo", article.getPhoto());
                        intent.putExtra("source", article.getSource());
                        intent.putExtra("id_video", article.getId_video());
                        intent.putExtra("type", article.getType());
                        startActivity(intent);
                    } else {
                        //Aller vers l'activité voire plus d'une vidéo
                        Intent intent = new Intent(getContext(), See_more_video.class);
                        intent.putExtra("adid", article.getAdid());
                        intent.putExtra("date_sent", article.getDate_sent());
                        intent.putExtra("titre", article.getTitre());
                        intent.putExtra("content", article.getContent());
                        intent.putExtra("lieu", article.getLieu());
                        intent.putExtra("photo", article.getPhoto());
                        intent.putExtra("source", article.getSource());
                        intent.putExtra("id_video", article.getId_video());
                        startActivity(intent);
                    }
                }
            });
            /*============================================================*/
        }
        return view;
    }

    /*=====================================================================*/
    //La méthode qui retourne la liste des articles
    private void get_all_articles() {
        //Query query = db.collection("Articles").orderBy("date", Query.Direction.DESCENDING);
        db.collection("Articles").orderBy("date", Query.Direction.DESCENDING).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    Log.d(TAG, "Nbr articles: " + task.getResult().size());
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        article = new Articles();
                        article.setAdid(document.getId());
                        article.setDate_sent(document.get("date").toString());
                        article.setTitre(document.get("titre").toString());
                        article.setContent(document.get("content").toString());
                        article.setPhoto(document.get("photo").toString());
                        article.setSource(document.get("source").toString());
                        article.setLieu(document.get("lieu").toString());
                        article.setType(document.get("type").toString());
                        article.setId_video(document.get("id_video").toString());
                        articles.add(article);
                    }
                    if (!articles.isEmpty()) {
                        adapter = new Actuality_Adapter(getContext(), articles);
                        list_actualite.setAdapter(adapter);
                        new CountDownTimer(2000, 1000) {

                            @Override
                            public void onTick(long l) {

                            }

                            @Override
                            public void onFinish() {
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);
                                swipe_actualite.setRefreshing(false);
                                list_actualite.setVisibility(View.VISIBLE);
                            }
                        }.start();
                    }
                } else {
                    Log.d(TAG, "Error: " + task.getException());
                }
            }
        });
    }
    /*=====================================================================*/

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRefresh() {
        swipe_actualite.postDelayed(new Runnable() {
            @Override
            public void run() {
                articles.clear();
                get_all_articles();
                adapter.notifyDataSetChanged();
                swipe_actualite.setRefreshing(false);
            }
        }, 2000);

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    /*========================================================================*/
    //La méthode qui retoune la liste des actualité
    private List<Articles> get_Articles() {
        List<Articles> articles = new ArrayList<>();
        Articles article;
        String result = "";
        OkHttpClient client = new OkHttpClient();
        RequestBody body = new FormBody.Builder()
                .add("token", token)
                .build();

        final Request request = new Request.Builder()
                .url(lien + "Articles/list_articles.php")
                .addHeader("Content-Type", "text/json; Charset=UTF-8")
                .post(body)
                .build();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Response response = null;
        try {
            response = client.newCall(request).execute();
            result = response.body().string();
            Log.d(TAG, "Articles: " + result);
            if (result != null) {
                JSONArray jsonArray = new JSONArray(result);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    article = new Articles();
                    article.setId(jsonObject.optInt("id"));
                    article.setDate_sent(jsonObject.optString("date_sent"));
                    article.setTitre(jsonObject.optString("titre"));
                    article.setContent(jsonObject.optString("content"));
                    article.setLieu(jsonObject.optString("lieu"));
                    article.setPhoto(jsonObject.optString("photo"));
                    article.setSource(jsonObject.optString("source"));
                    article.setType(jsonObject.optString("type"));
                    article.setId_video(jsonObject.optString("id_video"));
                    articles.add(article);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (response != null) {
                response.body().close();
            }
        }
        return articles;
    }


    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mShimmerViewContainer.stopShimmerAnimation();
    }
}
