package com.mbayennapp.diamalayetv.Fragments;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mbayennapp.diamalayetv.Activity.Start_emission;
import com.mbayennapp.diamalayetv.Activity.start_video;
import com.mbayennapp.diamalayetv.Adapter.Emission_Adapter;
import com.mbayennapp.diamalayetv.Model.Emissions;
import com.mbayennapp.diamalayetv.R;
import com.mbayennapp.diamalayetv.Utils.ConnexionInternet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Emission.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Emission#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Emission extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = start_video.class.getSimpleName();
    private String lien = "https://www.mabcompany.com/JL_API/";
    private String token = "layenne2017";
    private ConnexionInternet connexionInternet;
    private ImageView img_last_emission;
    private TextView title_emission;
    private SwipeRefreshLayout swipe_emission;
    private LinearLayout l_sect_last_emission;
    private ListView lst_other_emission;
    private List<Emissions> emissions;
    private Emission_Adapter adapter;
    private JSONObject j_message;
    /*==========================================================*/
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public Emission() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Emission.
     */
    // TODO: Rename and change types and number of parameters
    public static Emission newInstance(String param1, String param2) {
        Emission fragment = new Emission();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_emission, container, false);
        /*===============================================================*/
        title_emission = (TextView)view.findViewById(R.id.title_emission);
        img_last_emission = (ImageView)view.findViewById(R.id.img_last_emission);
        swipe_emission = (SwipeRefreshLayout)view.findViewById(R.id.swipe_emission);
        l_sect_last_emission = (LinearLayout)view.findViewById(R.id.l_sect_last_emission);
        lst_other_emission = (ListView)view.findViewById(R.id.lst_other_emission);
        /*===============================================================*/
        Typeface typeFace = Typeface.createFromAsset(getActivity().getAssets(), "fonts/century-gothic.ttf");
        Typeface typegold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/century-gothic-gold.ttf");
        title_emission.setTypeface(typegold);
        /*===============================================================*/
        connexionInternet = new ConnexionInternet();
        /*===============================================================*/
        swipe_emission.setOnRefreshListener(this);
        swipe_emission.setColorSchemeColors(Color.parseColor("#00796b"));
        swipe_emission.setRefreshing(true);
        if (connexionInternet.isConnectedInternet(getActivity())) {
            JSONObject j_last = get_last_emisssion();
            String code = j_last.optString("code");
            if (code.equals("1")) {
                j_message = j_last.optJSONObject("message");
                title_emission.setText(j_message.optString("titre"));
                String link_img = lien + "webroot/emission/" + j_message.optString("photo");
                Glide.with(getContext()).load(link_img).centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL).into(img_last_emission);
                emissions = get_other_emissions(j_message.optInt("id"));
                adapter = new Emission_Adapter(getContext(),emissions);
                lst_other_emission.setAdapter(adapter);
                new CountDownTimer(1500, 500) {

                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        swipe_emission.setRefreshing(false);
                        l_sect_last_emission.setVisibility(View.VISIBLE);
                        lst_other_emission.setVisibility(View.VISIBLE);
                    }
                }.start();
                setListViewHeightBasedOnChildren(lst_other_emission);
            }
            img_last_emission.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getContext(), Start_emission.class);
                    intent.putExtra("id",j_message.optInt("id"));
                    intent.putExtra("date_emi",j_message.optString("date_emi"));
                    intent.putExtra("titre",j_message.optString("titre"));
                    intent.putExtra("desciption",j_message.optString("description"));
                    intent.putExtra("duree",j_message.optString("duree"));
                    intent.putExtra("animateur",j_message.optString("animateur"));
                    intent.putExtra("fichier",j_message.optString("fichier"));
                    intent.putExtra("photo",j_message.optString("photo"));
                    startActivity(intent);
                }
            });
            lst_other_emission.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Emissions emission = emissions.get(i);
                    Intent intent = new Intent(getContext(), Start_emission.class);
                    intent.putExtra("id",emission.getId());
                    intent.putExtra("date_emi",emission.getDate_emi());
                    intent.putExtra("titre",emission.getTitre());
                    intent.putExtra("desciption",emission.getDescription());
                    intent.putExtra("duree",emission.getDuree());
                    intent.putExtra("animateur",emission.getEnimateur());
                    intent.putExtra("fichier",emission.getFichier());
                    intent.putExtra("photo",emission.getPhoto());
                    startActivity(intent);
                }
            });
        }
        /*===============================================================*/
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    /*@Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }*/

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRefresh() {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    /*====================================================*/
    //Modifier la hauteur de mon listView
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
    /*=====================================================================*/
    //La méthode qui retourne les autres emissions
    private List<Emissions> get_other_emissions(int id){
        List<Emissions> emissions = new ArrayList<>();
        Emissions emission;
        String result = "";
        String code = "";
        OkHttpClient client = new OkHttpClient();
        //
        RequestBody body = new FormBody.Builder()
                .add("token",token)
                .add("id", String.valueOf(id))
                .build();

        final Request request = new Request.Builder()
                .url(lien+"Emissions/get_other_emission.php")
                .addHeader("Content-Type","text/json; Charset=UTF-8")
                .post(body)
                .build();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        try {
            Response response = client.newCall(request).execute();
            result = response.body().string();
            if (result != null) {
                Log.d(TAG, "autres vidéos:" + result);
                if (result != null) {
                    JSONArray jsonArray = new JSONArray(result);
                    for (int i = 0; i < jsonArray.length();i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        emission = new Emissions();
                        emission.setId(jsonObject.optInt("id"));
                        emission.setPhoto(jsonObject.optString("photo"));
                        emission.setTitre(jsonObject.optString("titre"));
                        emission.setDate_ajout(jsonObject.optString("date_ajout"));
                        emission.setDate_emi(jsonObject.optString("date_emi"));
                        emission.setDuree(jsonObject.optString("duree"));
                        emission.setDescription(jsonObject.optString("description"));
                        emission.setEnimateur(jsonObject.optString("animateur"));
                        emission.setFichier(jsonObject.getString("fichier"));
                        emissions.add(emission);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return emissions;
    }
    //La méthode qui récupére la dernière émission
    private JSONObject get_last_emisssion(){
        JSONObject jsonObject = null;
        OkHttpClient client = new OkHttpClient();
        RequestBody body = new FormBody.Builder()
                .add("token",token)
                .build();

        final Request request = new Request.Builder()
                .url(lien+"Emissions/get_last_emission.php")
                .addHeader("Content-Type","text/json; Charset=UTF-8")
                .post(body)
                .build();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Response response = null;
        try {
            response = client.newCall(request).execute();
            jsonObject = new JSONObject(response.body().string());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            response.body().close();
        }
        return jsonObject;
    }
}
