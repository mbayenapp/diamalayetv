package com.mbayennapp.diamalayetv.Fragments;

import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.mbayennapp.diamalayetv.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Terms.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Terms#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Terms extends Fragment {
    private String lien = "https://www.mabcompany.com/JL_API/";
    private String token = "layenne2017";
    private LinearLayout l_progress_mention,l_web_mention;
    private WebView web_mention;
    /*===================================================================*/
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public Terms() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Terms.
     */
    // TODO: Rename and change types and number of parameters
    public static Terms newInstance(String param1, String param2) {
        Terms fragment = new Terms();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_terms, container, false);
        /*======================================================================*/
        l_progress_mention = (LinearLayout)view.findViewById(R.id.l_progress_mention);
        l_web_mention = (LinearLayout)view.findViewById(R.id.l_web_mention);
        web_mention = (WebView)view.findViewById(R.id.web_mention);
        /*======================================================================*/
        Typeface typeFace = Typeface.createFromAsset(getActivity().getAssets(), "fonts/century-gothic.ttf");
        Typeface typegold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/century-gothic-gold.ttf");
        /*======================================================================*/
        web_mention.loadUrl(lien+"mentions.php");
        web_mention.setHorizontalScrollBarEnabled(false);
        new CountDownTimer(3000,1000){

            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                l_progress_mention.setVisibility(View.GONE);
                l_web_mention.setVisibility(View.VISIBLE);
            }
        }.start();
        /*======================================================================*/
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    /*@Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }*/

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
