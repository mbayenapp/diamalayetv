package com.mbayennapp.diamalayetv.Fragments;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.mbayennapp.diamalayetv.Activity.Accompte;
import com.mbayennapp.diamalayetv.Activity.Apropos;
import com.mbayennapp.diamalayetv.Activity.Login;
import com.mbayennapp.diamalayetv.Helpers.SessionManagerLogin;
import com.mbayennapp.diamalayetv.Model.Langues;
import com.mbayennapp.diamalayetv.Model.Users;
import com.mbayennapp.diamalayetv.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Parametre.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Parametre#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Parametre extends Fragment {
    private String lien = "https://www.mabcompany.com/JL_API/";
    private String token = "layenne2017";
    private static final String TAG = Parametre.class.getSimpleName();
    private SessionManagerLogin sessionManagerLogin;
    private LinearLayout l_sect_user_on_line;
    private CircleImageView img_user_on_line;
    private TextView txt_name_user_online,txt_tel_user_online,txt_evaluate,
            txt_politique,txt_apropos_of_our,txt_version,txt_version_name,
            txt_langue,txt_langue_choice,txt_param_account,txt_model,
            txt_model_phone,txt_version_android,txt_version_android_text;
    private MaterialRippleLayout l_sect_accomte,l_sect_apropos;
    /*============================================================*/
    private String link = "";
    /*============================================================*/
    private List<Langues>  langues = new ArrayList<>();
    private Langues langue;
    /*============================================================*/
    private Users user;
    private TelephonyManager tm;
    /*============================================================*/
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private DocumentReference dbuser;
    /*============================================================*/
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public Parametre() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Parametre.
     */
    // TODO: Rename and change types and number of parameters
    public static Parametre newInstance(String param1, String param2) {
        Parametre fragment = new Parametre();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_parametre, container, false);
        /*=========================================================================*/
        l_sect_user_on_line = (LinearLayout)view.findViewById(R.id.l_sect_user_on_line);
        img_user_on_line = (CircleImageView)view.findViewById(R.id.img_user_on_line);
        txt_name_user_online = (TextView)view.findViewById(R.id.txt_name_user_online);
        txt_tel_user_online = (TextView)view.findViewById(R.id.txt_tel_user_online);
        txt_evaluate = (TextView)view.findViewById(R.id.txt_evaluate);
        txt_politique = (TextView)view.findViewById(R.id.txt_politique);
        txt_apropos_of_our = (TextView)view.findViewById(R.id.txt_apropos_of_our);
        txt_version = (TextView)view.findViewById(R.id.txt_version);
        txt_version_name = (TextView)view.findViewById(R.id.txt_version_name);
        txt_langue = (TextView)view.findViewById(R.id.txt_langue);
        txt_langue_choice = (TextView)view.findViewById(R.id.txt_langue_choice);
        txt_param_account = (TextView)view.findViewById(R.id.txt_param_account);
        l_sect_accomte = (MaterialRippleLayout)view.findViewById(R.id.l_sect_accomte);
        l_sect_apropos = (MaterialRippleLayout)view.findViewById(R.id.l_sect_apropos);
        txt_model = (TextView)view.findViewById(R.id.txt_model);
        txt_model_phone = (TextView)view.findViewById(R.id.txt_model_phone);
        txt_version_android = (TextView)view.findViewById(R.id.txt_version_android);
        txt_version_android_text = (TextView)view.findViewById(R.id.txt_version_android_text);
        /*=========================================================================*/
        sessionManagerLogin = new SessionManagerLogin(getContext());
        /*=========================================================================*/
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        /*=========================================================================*/
        Typeface typeFace = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeueLTStd_Roman.otf");
        Typeface typegold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeuecondenced.otf");
        txt_name_user_online.setTypeface(typegold);
        txt_tel_user_online.setTypeface(typeFace);
        txt_evaluate.setTypeface(typeFace);
        txt_politique.setTypeface(typeFace);
        txt_apropos_of_our.setTypeface(typeFace);
        txt_version.setTypeface(typeFace);
        txt_version_name.setTypeface(typeFace);
        txt_langue.setTypeface(typeFace);
        txt_langue_choice.setTypeface(typeFace);
        txt_param_account.setTypeface(typeFace);
        txt_model.setTypeface(typeFace);
        txt_model_phone.setTypeface(typeFace);
        txt_version_android.setTypeface(typeFace);
        txt_version_android_text.setTypeface(typeFace);
         /*===============================================================*/
        PackageManager pm = getActivity().getPackageManager();
        PackageInfo pi = null;
        try {
            try {
                pi = pm.getPackageInfo(getActivity().getPackageName(), 0);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            tm = (TelephonyManager) getContext().getSystemService(getContext().TELEPHONY_SERVICE);
            try {
                txt_version_name.setText(pi.versionName);
                txt_model_phone.setText(Build.MODEL);
                txt_version_android_text.setText(Build.VERSION.RELEASE);
            }catch (Exception e){
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        l_sect_apropos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), Apropos.class));
            }
        });
         /*===============================================================*/
        txt_name_user_online.setText(sessionManagerLogin.getLoggedInUser().getPrenom() + " " + sessionManagerLogin.getLoggedInUser().getNom());
        if (!sessionManagerLogin.getLoggedInUser().getTel().equals("")) {
            txt_tel_user_online.setText(sessionManagerLogin.getLoggedInUser().getTel());
        }
        link = sessionManagerLogin.getLoggedInUser().getProfil();
        Glide.with(getContext()).load(link).centerCrop().crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(img_user_on_line);
        /*=========================================================================*/
        if (sessionManagerLogin.isLoggedIn()){
            l_sect_user_on_line.setVisibility(View.VISIBLE);
        }else {
            l_sect_user_on_line.setVisibility(View.GONE);
        }
        l_sect_accomte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sessionManagerLogin.isLoggedIn()) {
                    Intent  intent = new Intent(getContext(),Accompte.class);
                    intent.putExtra("id",sessionManagerLogin.getLoggedInUser().getId());
                    intent.putExtra("nom",sessionManagerLogin.getLoggedInUser().getNom());
                    intent.putExtra("prenom",sessionManagerLogin.getLoggedInUser().getPrenom());
                    intent.putExtra("photo",sessionManagerLogin.getLoggedInUser().getProfil());
                    intent.putExtra("adresse",sessionManagerLogin.getLoggedInUser().getAdresse());
                    intent.putExtra("email",sessionManagerLogin.getLoggedInUser().getEmail());
                    intent.putExtra("tel",sessionManagerLogin.getLoggedInUser().getTel());
                    intent.putExtra("ville",sessionManagerLogin.getLoggedInUser().getVille());
                    intent.putExtra("pays",sessionManagerLogin.getLoggedInUser().getPays());
                    intent.putExtra("sexe",sessionManagerLogin.getLoggedInUser().getSexe());
                    intent.putExtra("activity","parametre");
                    startActivity(intent);
                    //getActivity().finish();
                }else {
                  startActivity(new Intent(getContext(), Login.class));
                  getActivity().finish();
                }
            }
        });
        /*=========================================================================*/
        get_langues();
        /*=========================================================================*/
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    /*@Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }*/

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    /*=========================================================================*/
    private Users get_User(String tel){
        String result = "";
        String code = "";
        Users user = new Users();
        OkHttpClient client = new OkHttpClient();
        //
        RequestBody body = new FormBody.Builder()
                .add("token",token)
                .add("tel",tel)
                .build();

        final Request request = new Request.Builder()
                .url(lien+"Users/get_user.php")
                .addHeader("Content-Type","text/json; Charset=UTF-8")
                .post(body)
                .build();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        try {
            Response response = client.newCall(request).execute();
            result = response.body().string();
            if (result != null) {
                JSONObject jsonObject = new JSONObject(result);
                Log.d(TAG, "Info user:" + jsonObject);
                code = jsonObject.optString("code");
                if (code.equals("1")){
                    JSONObject j_data = jsonObject.optJSONObject("data");
                    user.setId(j_data.optString("id"));
                    user.setNom(j_data.optString("nom"));
                    user.setPrenom(j_data.optString("prenom"));
                    user.setAdresse(j_data.optString("adresse"));
                    user.setSexe(j_data.optString("sexe"));
                    user.setIndicatif(j_data.optString("indicatif"));
                    user.setTel(j_data.optString("tel"));
                    user.setEmail(j_data.optString("email"));
                    user.setPassword(j_data.optString("password"));
                    user.setVille(j_data.optString("ville"));
                    user.setCode_postal(j_data.optString("code_postal"));
                    user.setProfil(j_data.optString("profil"));
                    user.setCouverture(j_data.optString("couverture"));
                    user.setPays(j_data.optString("pays"));
                    user.setEtat(j_data.optString("etat"));
                    user.setStatut(j_data.optString("statut"));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return user;
    }

    /* La méthode qui retourne les langues de l'application */
    private void get_langues(){
        db.collection("public").document("parameters").collection("langues").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                for (QueryDocumentSnapshot document : task.getResult()) {
                    Log.d(TAG,"Langue: "+document.get("_"+document.getId()));
                    if (document.getId().equals("FR")){
                        txt_langue_choice.setText(document.get("_"+document.getId()).toString());
                    }
                    langue = new Langues();
                    langue.setKey(document.getId());
                    langue.setValeur(document.get("_"+document.getId()).toString());
                    langues.add(langue);
                }
                if (langues.size() > 0){
                    Log.d(TAG,"nbr langue : "+langues.size());
                }
            }
        });
    }
    /* Fin */
}
