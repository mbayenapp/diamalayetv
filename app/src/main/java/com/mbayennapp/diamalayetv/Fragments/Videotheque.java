package com.mbayennapp.diamalayetv.Fragments;

import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.mbayennapp.diamalayetv.Adapter.Videotheque_Adapter;
import com.mbayennapp.diamalayetv.Model.Videos;
import com.mbayennapp.diamalayetv.R;
import com.mbayennapp.diamalayetv.Utils.ConnexionInternet;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Videotheque.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Videotheque#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Videotheque extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = Videotheque.class.getSimpleName();
    private RecyclerView r_videos;
    private Videotheque_Adapter adapter;
    private ConnexionInternet connexionInternet;
    private List<Videos> videos = new ArrayList<>();
    private Videos video;
    private SwipeRefreshLayout swipe_actualite;
    /*==================================================================*/
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private DocumentReference dvideos;
    /*==================================================================*/
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public Videotheque() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Videotheque.
     */
    // TODO: Rename and change types and number of parameters
    public static Videotheque newInstance(String param1, String param2) {
        Videotheque fragment = new Videotheque();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_videotheque, container, false);
        /*=======================================================================================*/
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        /*=======================================================================================*/
        swipe_actualite = (SwipeRefreshLayout)view.findViewById(R.id.swipe_actualite);
        r_videos = (RecyclerView)view.findViewById(R.id.recycler_video);
        connexionInternet = new ConnexionInternet();
        swipe_actualite.setOnRefreshListener(this);
        swipe_actualite.setColorSchemeColors(Color.parseColor("#000000"));
        swipe_actualite.setRefreshing(true);

        get_all_videos();
        return  view;
    }

    /*=========================================================================================*/
    private void get_all_videos(){
        if (connexionInternet.isConnectedInternet(getActivity())) {
            db.collection("Videos").orderBy("date", Query.Direction.DESCENDING).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "Nbr vidéos: " + task.getResult().size());
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            video = new Videos();
                            video.setCategorie(document.get("categorie").toString());
                            video.setDate_sent(document.get("date").toString());
                            video.setDescription(document.get("description").toString());
                            video.setImage(document.get("image").toString());
                            video.setTitre(document.get("titre").toString());
                            video.setId_video(document.get("id_video").toString());
                            video.setVdid(document.getId());
                            videos.add(video);
                        }
                        adapter = new Videotheque_Adapter(getContext(), videos);
                        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(), 2);
                        r_videos.setLayoutManager(mLayoutManager);
                        r_videos.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
                        r_videos.setItemAnimator(new DefaultItemAnimator());
                        r_videos.setAdapter(adapter);
                        swipe_actualite.setRefreshing(false);
                        r_videos.setVisibility(View.VISIBLE);
                    } else {
                        Log.d(TAG, "Error: " + task.getException());
                    }
                }
            });
        }
    }
    /*=========================================================================================*/

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    /*@Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
*/
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRefresh() {
        swipe_actualite.postDelayed(new Runnable() {
            @Override
            public void run() {
                videos.clear();
                get_all_videos();
                adapter.notifyDataSetChanged();
                swipe_actualite.setRefreshing(false);
            }
        },2000);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
