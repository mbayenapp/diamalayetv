package com.mbayennapp.diamalayetv.Model;

/**
 * Created by Mbaye on 04/06/2018.
 */

public class Langues {
    private String key,valeur;

    public Langues() {
    }

    public Langues(String key, String valeur) {
        this.key = key;
        this.valeur = valeur;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValeur() {
        return valeur;
    }

    public void setValeur(String valeur) {
        this.valeur = valeur;
    }
}
