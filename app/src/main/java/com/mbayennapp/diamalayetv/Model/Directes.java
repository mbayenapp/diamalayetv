package com.mbayennapp.diamalayetv.Model;

/**
 * Created by bfali on 03/12/2016.
 */

public class Directes {
    private int id;
    private String id_video;

    public Directes() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getId_video() {
        return id_video;
    }

    public void setId_video(String id_video) {
        this.id_video = id_video;
    }
}