package com.mbayennapp.diamalayetv.Model;

/**
 * Created by bfali on 03/12/2016.
 */

public class Users {
    private String id;
    private String nom,prenom,adresse,sexe,indicatif,tel,email,password,ville,profil,couverture,pays,etat,statut,code_postal,type;

    public Users() {
    }

    public Users(String email) {
        this.email = email;
    }

    public Users(String id, String nom, String prenom, String adresse, String sexe, String indicatif, String tel, String email, String password, String ville, String code_postal, String profil, String couverture, String pays, String etat, String statut,String type) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.sexe = sexe;
        this.indicatif = indicatif;
        this.tel = tel;
        this.email = email;
        this.password = password;
        this.ville = ville;
        this.code_postal = code_postal;
        this.profil = profil;
        this.couverture = couverture;
        this.pays = pays;
        this.etat = etat;
        this.statut = statut;
        this.type = type;
    }

    public Users(String nom, String prenom, String adresse, String email, String password) {
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.email = email;
        this.password = password;
    }

    public Users(String sexe, String indicatif, String tel, String ville, String code_postal, String pays, String email) {
        this.sexe = sexe;
        this.tel = tel;
        this.ville = ville;
        this.code_postal = code_postal;
        this.pays = pays;
        this.email = email;
        this.indicatif = indicatif;
    }

    public Users(String id, String nom, String prenom, String adresse, String sexe, String tel, String email, String profil) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.sexe = sexe;
        this.tel = tel;
        this.email = email;
        this.profil = profil;
    }

    public Users(String id, String nom, String prenom, String indicatif, String tel, String adresse, String codepostal, String ville, String pays,String email, String profil, String sexe,String type) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.indicatif = indicatif;
        this.tel = tel;
        this.adresse = adresse;
        this.code_postal = codepostal;
        this.ville = ville;
        this.pays =  pays;
        this.email = email;
        this.profil = profil;
        this.sexe = sexe;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode_postal() {
        return code_postal;
    }

    public void setCode_postal(String code_postal) {
        this.code_postal = code_postal;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getIndicatif() {
        return indicatif;
    }

    public void setIndicatif(String indicatif) {
        this.indicatif = indicatif;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getProfil() {
        return profil;
    }

    public void setProfil(String profil) {
        this.profil = profil;
    }

    public String getCouverture() {
        return couverture;
    }

    public void setCouverture(String couverture) {
        this.couverture = couverture;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
