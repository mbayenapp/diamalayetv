package com.mbayennapp.diamalayetv.Model;

/**
 * Created by Mbaye on 16/09/2017.
 */

public class Emissions {
    private int id;
    private String date_ajout,date_emi,titre,description,enimateur,fichier,photo,duree;

    public Emissions() {
    }

    public Emissions(int id, String date_ajout, String date_emi, String titre, String description, String enimateur, String fichier,String photo,String duree) {
        this.id = id;
        this.date_ajout = date_ajout;
        this.date_emi = date_emi;
        this.titre = titre;
        this.description = description;
        this.enimateur = enimateur;
        this.fichier = fichier;
        this.photo = photo;
        this.duree = duree;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate_ajout() {
        return date_ajout;
    }

    public void setDate_ajout(String date_ajout) {
        this.date_ajout = date_ajout;
    }

    public String getDate_emi() {
        return date_emi;
    }

    public void setDate_emi(String date_emi) {
        this.date_emi = date_emi;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEnimateur() {
        return enimateur;
    }

    public void setEnimateur(String enimateur) {
        this.enimateur = enimateur;
    }

    public String getFichier() {
        return fichier;
    }

    public void setFichier(String fichier) {
        this.fichier = fichier;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDuree() {
        return duree;
    }

    public void setDuree(String duree) {
        this.duree = duree;
    }
}
