package com.mbayennapp.diamalayetv.Model;

/**
 * Created by bfali on 27/01/2017.
 */

public class Mahdi {
    private int id;
    private String titre,content;

    public Mahdi() {
    }

    public Mahdi(int id, String titre, String content) {
        this.id = id;
        this.titre = titre;
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
