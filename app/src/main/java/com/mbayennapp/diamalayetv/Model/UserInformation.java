package com.mbayennapp.diamalayetv.Model;

/**
 * Created by bfali on 12/11/2016.
 */

public class UserInformation {
    public String code_postal,tel;
    public String nom,prenom,adresse,email,password,ville,pays,sexe,profil,couverture;

    public UserInformation() {
    }

    public UserInformation(String nom, String prenom, String adresse, String email, String password, String ville, String code_postal, String pays, String tel, String sexe, String profil, String couverture) {
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.email = email;
        this.password = password;
        this.ville = ville;
        this.code_postal = code_postal;
        this.pays = pays;
        this.tel = tel;
        this.sexe = sexe;
        this.profil = profil;
        this.couverture = couverture;
    }

    public UserInformation(String nom, String prenom, String adresse, String email, String password) {
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.email = email;
        this.password = password;
    }

    public UserInformation(String tel, String code_postal, String ville, String pays, String sexe, String email) {
        this.tel = tel;
        this.code_postal = code_postal;
        this.ville = ville;
        this.pays = pays;
        this.sexe = sexe;
        this.email = email;
    }

    public UserInformation(String profil, String couverture, String email) {
        this.profil = profil;
        this.couverture = couverture;
        this.email = email;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getCode_postal() {
        return code_postal;
    }

    public void setCode_postal(String code_postal) {
        this.code_postal = code_postal;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getProfil() {
        return profil;
    }

    public void setProfil(String profil) {
        this.profil = profil;
    }

    public String getCouverture() {
        return couverture;
    }

    public void setCouverture(String couverture) {
        this.couverture = couverture;
    }
}
