package com.mbayennapp.diamalayetv.Model;

public class Indicatifs {
    private String iso,drapeau,indicatif,intitule;

    public Indicatifs() {
    }

    public Indicatifs(String iso, String drapeau, String indicatif, String intitule) {
        this.iso = iso;
        this.drapeau = drapeau;
        this.indicatif = indicatif;
        this.intitule = intitule;
    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    public String getDrapeau() {
        return drapeau;
    }

    public void setDrapeau(String drapeau) {
        this.drapeau = drapeau;
    }

    public String getIndicatif() {
        return indicatif;
    }

    public void setIndicatif(String indicatif) {
        this.indicatif = indicatif;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }
}
