package com.mbayennapp.diamalayetv.Model;

/**
 * Created by Mbaye on 02/10/2017.
 */

public class Sermon {
    private int id;
    private String titre,introduction,titre_intitule,text_intitule,titre_sermon,sermon,proprietaire;

    public Sermon() {
    }

    public Sermon(int id, String titre, String introduction, String titre_intitule, String text_intitule, String titre_sermon, String sermon, String proprietaire) {
        this.id = id;
        this.titre = titre;
        this.introduction = introduction;
        this.titre_intitule = titre_intitule;
        this.text_intitule = text_intitule;
        this.titre_sermon = titre_sermon;
        this.sermon = sermon;
        this.proprietaire = proprietaire;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getTitre_intitule() {
        return titre_intitule;
    }

    public void setTitre_intitule(String titre_intitule) {
        this.titre_intitule = titre_intitule;
    }

    public String getText_intitule() {
        return text_intitule;
    }

    public void setText_intitule(String text_intitule) {
        this.text_intitule = text_intitule;
    }

    public String getTitre_sermon() {
        return titre_sermon;
    }

    public void setTitre_sermon(String titre_sermon) {
        this.titre_sermon = titre_sermon;
    }

    public String getSermon() {
        return sermon;
    }

    public void setSermon(String sermon) {
        this.sermon = sermon;
    }

    public String getProprietaire() {
        return proprietaire;
    }

    public void setProprietaire(String proprietaire) {
        this.proprietaire = proprietaire;
    }
}
