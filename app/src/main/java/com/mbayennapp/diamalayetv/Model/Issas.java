package com.mbayennapp.diamalayetv.Model;

/**
 * Created by bfali on 13/02/2017.
 */

public class Issas {
    private int id;
    private String titre,content,image,logo;

    public Issas() {
    }

    public Issas(int id, String titre, String content, String image, String logo) {
        this.id = id;
        this.titre = titre;
        this.content = content;
        this.image = image;
        this.logo = logo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}
