package com.mbayennapp.diamalayetv.Activity;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.mbayennapp.diamalayetv.Adapter.Rubrique_Adapter;
import com.mbayennapp.diamalayetv.Model.Rubriques;
import com.mbayennapp.diamalayetv.R;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Direct extends AppCompatActivity {
    private static final String TAG = start_video.class.getSimpleName();
    private String lien = "https://www.mabcompany.com/JL_API/";
    private String token = "layenne2017";
    private TextView txt_title_direct_tv,txt_title_rubrique_tv;
    private ImageView img_start_tv;
    private String link = "";
    private MaterialRippleLayout sect_start_direct;
    private JSONObject j_data = null;
    private RecyclerView rubrique;
    private Rubrique_Adapter adapter;
    private List<Rubriques> rubriques = new ArrayList<>();
    private Rubriques rub;
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private DocumentReference darticles;
    private String id_video = "";
    private String titre = "";
    private String lieu = "";
    private ShimmerFrameLayout shimmer_view;
    /*==========================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_direct);
        /*==========================================================*/
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        /*==========================================================*/
        txt_title_direct_tv = (TextView)findViewById(R.id.txt_title_direct_tv);
        txt_title_rubrique_tv = (TextView) findViewById(R.id.txt_title_rubrique_tv);
        img_start_tv = (ImageView)findViewById(R.id.img_start_tv);
        sect_start_direct = (MaterialRippleLayout)findViewById(R.id.sect_start_direct);
        rubrique = (RecyclerView)findViewById(R.id.rubrique);
        shimmer_view = (ShimmerFrameLayout)findViewById(R.id.shimmer_view);
        /*==========================================================*/
        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeueLTStd_Roman.otf");
        Typeface typegold = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeuecondenced.otf");
        txt_title_direct_tv.setTypeface(typegold);
        txt_title_rubrique_tv.setTypeface(typegold);
        /*==========================================================*/
        Glide.with(getApplicationContext()).load(R.drawable.banierre_tv).centerCrop().crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(img_start_tv);
        db.collection("Tv").document("tpX3zAWGVB5UMoKDnR1b").get().addOnCompleteListener(Direct.this, new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()){
                    DocumentSnapshot documentSnapshot = task.getResult();
                    if (documentSnapshot != null && documentSnapshot.exists()){
                        Log.d(TAG,"tv: "+documentSnapshot.get("idvideo").toString());
                        id_video = documentSnapshot.get("idvideo").toString();
                        titre = documentSnapshot.get("titre").toString();
                        lieu = documentSnapshot.get("lieu").toString();
                    }else {
                        Log.d(TAG,"Aucun données trouvées!");
                    }
                }else {
                    Log.d(TAG,"Error: "+task.getException());
                }
            }
        });
        sect_start_direct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Start_direct.class);
                intent.putExtra("idvideo", id_video);
                intent.putExtra("lieu",lieu);
                intent.putExtra("titre",titre);
                startActivity(intent);
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                finish();
            }
        });
        try{
            get_rubriques();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void get_rubriques(){
        //Query query = db.collection("Articles").orderBy("date", Query.Direction.DESCENDING);
        db.collection("Rubriques").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    Log.d(TAG, "rubriques: " + task.getResult().size());
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        if (document.getId() != null) {
                            rub = new Rubriques();
                            rub.setNom(document.get("nom").toString());
                            rub.setImage(document.get("image").toString());
                            rubriques.add(rub);
                        }
                    }
                    if (!rubriques.isEmpty()){
                        adapter = new Rubrique_Adapter(Direct.this, rubriques);
                        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
                        rubrique.setLayoutManager(mLayoutManager);
                        rubrique.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(6), true));
                        rubrique.setItemAnimator(new DefaultItemAnimator());
                        new CountDownTimer(1000,500){
                            @Override
                            public void onTick(long millisUntilFinished) {

                            }

                            @Override
                            public void onFinish() {
                                shimmer_view.stopShimmerAnimation();
                                shimmer_view.setVisibility(View.GONE);
                                rubrique.setAdapter(adapter);
                                rubrique.setVisibility(View.VISIBLE);
                            }
                        }.start();
                    }
                } else {
                    Log.d(TAG, "Error: " + task.getException());
                }
            }
        });
    }
    /*==========================================================*/
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(getApplicationContext(),Home.class));
            finish();
        }
        return false;
    }
    /*==========================================================*/
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }


    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
    /*==========================================================*/
    @Override
    protected void onResume() {
        super.onResume();
        //mRadioManager.connect();
        shimmer_view.startShimmerAnimation();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onPause() {
        shimmer_view.stopShimmerAnimation();
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        shimmer_view.stopShimmerAnimation();
    }
    /*==========================================================*/
}
