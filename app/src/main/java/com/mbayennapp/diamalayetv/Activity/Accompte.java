package com.mbayennapp.diamalayetv.Activity;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.media.ExifInterface;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.fxn.utility.Utility;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.kosalgeek.android.photoutil.GalleryPhoto;
import com.kosalgeek.android.photoutil.ImageBase64;
import com.kosalgeek.android.photoutil.ImageLoader;
import com.kosalgeek.genasync12.AsyncResponse;
import com.kosalgeek.genasync12.EachExceptionsHandler;
import com.kosalgeek.genasync12.PostResponseAsyncTask;
import com.mbayennapp.diamalayetv.Adapter.Indicatif_Adapter;
import com.mbayennapp.diamalayetv.Helpers.SessionManagerLogin;
import com.mbayennapp.diamalayetv.Model.Indicatifs;
import com.mbayennapp.diamalayetv.Model.Users;
import com.mbayennapp.diamalayetv.R;
import com.mbayennapp.diamalayetv.Utils.Properties;
import com.wonderkiln.camerakit.CameraKit;
import com.wonderkiln.camerakit.CameraKitEventCallback;
import com.wonderkiln.camerakit.CameraKitImage;
import com.wonderkiln.camerakit.CameraView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Accompte extends AppCompatActivity {
    private static final String TAG = Accompte.class.getSimpleName();
    private String lien = "https://www.mabcompany.com/JL_API/";
    private String token = "layenne2017";
    private String nom, prenom, adresse, photo, email, tel, ville, pays, sexe,indicatif, activity;
    private Toolbar toolbar;
    private ImageView img_profil, iv;
    private TextView txt_alert_user_success, txt_titre_photo, txt_title_info_personnel,
            txt_prenom_error, txt_nom_error;
    private Button btn_no_confirm, btn_deconnect, btn_cancel_upload,
            btn_upload;
    private RelativeLayout r_confirme_manage_profil, r_loader_login, r_alert_user_success;
    private TextInputEditText txt_prenom, txt_nom, txt_email, txt_adresse,txt_telephone,
            txt_indicatif;
    private RadioButton radio_homme, radio_femme;
    private LinearLayout l_sect_validate_upload,l_sect_show_indicatif;
    private Button btn_save_info;
    private ScrollView scroll_all_info;
    private CameraView mCamera;
    private CircleImageView clickme;
    private LinearLayout fram_select_image, l_sect_manage_profil;
    private Bitmap scaled = null;
    private ImageView front;
    private FrameLayout flash;
    private ListView list_indicatifs;
    private List<Indicatifs> indicatifs = new ArrayList<>();
    private Indicatifs indica;
    private Indicatif_Adapter indicatif_adapter;
    private RelativeLayout r_sect_indicatif;
    /*=============================================================*/
    private GalleryPhoto galleryPhoto;
    String SlectedPhoto;
    private Animation animSideTop, animSideFadeOut;
    private String link;
    private SessionManagerLogin sessionManagerLogin;
    /*============================================================*/
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1000;
    public static final int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 9921;
    private final int GALLERY_REQUEST = 22131;
    private String valueGenre = "";
    /*============================================================*/
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private FirebaseStorage storage;
    private StorageReference storageRef;
    private Properties properties;
    /*============================================================*/
    private int init_valeur_time = 5700;
    private int READ_REQUEST_CODE = 4;
    private int init_sect_indicatif = 0;
    private int init_camera = 0;
    /*============================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accompte);
        /*========================================================*/
        Bundle extras = getIntent().getExtras();
        //id = extras.getString("id");
        nom = extras.getString("nom");
        prenom = extras.getString("prenom");
        adresse = extras.getString("adresse");
        photo = extras.getString("photo");
        email = extras.getString("email");
        tel = extras.getString("tel");
        ville = extras.getString("ville");
        pays = extras.getString("pays");
        sexe = extras.getString("sexe");
        indicatif = extras.getString("indicatif");
        activity = extras.getString("activity");
        /*========================================================*/
        toolbar = (Toolbar) findViewById(R.id.toolbar_accompte);
        toolbar.setTitle("Mon Profil");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        /*========================================================*/
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        storage = FirebaseStorage.getInstance();
        properties = new Properties();
        /*========================================================*/
        img_profil = (CircleImageView) findViewById(R.id.img_profil);
        btn_no_confirm = (Button) findViewById(R.id.btn_no_confirm);
        btn_deconnect = (Button) findViewById(R.id.btn_deconnect);
        r_confirme_manage_profil = (RelativeLayout) findViewById(R.id.r_confirme_manage_profil);
        r_loader_login = (RelativeLayout) findViewById(R.id.r_loader_login);
        r_alert_user_success = (RelativeLayout) findViewById(R.id.r_alert_user_success);
        txt_alert_user_success = (TextView) findViewById(R.id.txt_alert_user_success);
        txt_titre_photo = (TextView) findViewById(R.id.txt_titre_photo);
        txt_title_info_personnel = (TextView) findViewById(R.id.txt_title_info_personnel);
        txt_prenom = (TextInputEditText) findViewById(R.id.txt_prenom);
        txt_nom = (TextInputEditText) findViewById(R.id.txt_nom);
        txt_email = (TextInputEditText) findViewById(R.id.txt_email);
        txt_adresse = (TextInputEditText) findViewById(R.id.txt_adresse);
        txt_telephone = (TextInputEditText)findViewById(R.id.txt_telephone);
        txt_indicatif = (TextInputEditText)findViewById(R.id.txt_indicatif);
        btn_save_info = (Button) findViewById(R.id.btn_save_info);
        radio_homme = (RadioButton) findViewById(R.id.radio_homme);
        radio_femme = (RadioButton) findViewById(R.id.radio_femme);
        txt_prenom_error = (TextView) findViewById(R.id.txt_prenom_error);
        txt_nom_error = (TextView) findViewById(R.id.txt_nom_error);
        btn_cancel_upload = (Button) findViewById(R.id.btn_cancel_upload);
        btn_upload = (Button) findViewById(R.id.btn_upload);
        l_sect_validate_upload = (LinearLayout) findViewById(R.id.l_sect_validate_upload);
        scroll_all_info = (ScrollView) findViewById(R.id.scroll_all_info);
        iv = (ImageView) findViewById(R.id.iv);
        clickme = (CircleImageView) findViewById(R.id.clickme);
        fram_select_image = (LinearLayout) findViewById(R.id.fram_select_image);
        front = (ImageView) findViewById(R.id.front);
        l_sect_manage_profil = (LinearLayout) findViewById(R.id.l_sect_manage_profil);
        flash = (FrameLayout) findViewById(R.id.flash);
        mCamera = findViewById(R.id.camera);
        list_indicatifs = (ListView)findViewById(R.id.list_indicatifs);
        r_sect_indicatif = (RelativeLayout)findViewById(R.id.r_sect_indicatif);
        l_sect_show_indicatif = (LinearLayout) findViewById(R.id.l_sect_show_indicatif);
        /*========================================================*/
        final int permCamera = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        final int permStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        /*========================================================*/
        sessionManagerLogin = new SessionManagerLogin(this);
        galleryPhoto = new GalleryPhoto(getApplicationContext());
        storageRef = storage.getReference().child("webroot/users/"+sessionManagerLogin.getLoggedInUser().getEmail());
        /*======================================================*/
        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeueLTStd_Roman.otf");
        Typeface typegold = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeuecondenced.otf");
        txt_titre_photo.setTypeface(typeFace);
        txt_title_info_personnel.setTypeface(typegold);
        txt_prenom.setTypeface(typeFace);
        txt_nom.setTypeface(typeFace);
        txt_email.setTypeface(typeFace);
        txt_adresse.setTypeface(typeFace);
        btn_save_info.setTypeface(typegold);
        radio_homme.setTypeface(typeFace);
        radio_femme.setTypeface(typeFace);
        txt_prenom_error.setTypeface(typeFace);
        txt_nom_error.setTypeface(typeFace);
        btn_cancel_upload.setTypeface(typegold);
        btn_upload.setTypeface(typegold);
        txt_telephone.setTypeface(typeFace);
        txt_indicatif.setTypeface(typeFace);
        /*======================================================*/
        topAnimation();
        fadeOutAnimation();
        get_indicatifs();
        /*====================================================*/
        /*if (permissionCheeck != PackageManager.PERMISSION_GRANTED){
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)){

            }else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);
            }
        }*/
        /*====================================================*/
        Glide.with(getApplicationContext()).load(sessionManagerLogin.getLoggedInUser().getProfil()).centerCrop().crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(img_profil);
        try {
            txt_prenom.setText(prenom);
            txt_nom.setText(nom);
            txt_email.setText(email);
            if (!adresse.equals("")) {
                txt_adresse.setText(adresse);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!tel.equals("")){
            txt_telephone.setText(tel);
        }
        if (!indicatif.equals("")){
            txt_indicatif.setText(indicatif);
        }
        /*========================================================*/
        valueGenre = sexe;
        if (!sexe.equals("")) {
            if (sexe.equals("Homme")) {
                radio_homme.setChecked(true);
            } else {
                radio_femme.setChecked(true);
            }
        }else {
            radio_homme.setChecked(true);
        }
        /*========================================================*/
        txt_prenom.setFocusableInTouchMode(false);
        txt_nom.setFocusableInTouchMode(false);
        txt_telephone.setFocusableInTouchMode(false);
        txt_adresse.setFocusableInTouchMode(false);
        txt_indicatif.setFocusableInTouchMode(false);
        txt_email.setFocusableInTouchMode(false);
        txt_prenom.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                txt_prenom.setFocusableInTouchMode(true);
                return false;
            }
        });
        txt_nom.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                txt_nom.setFocusableInTouchMode(true);
                return false;
            }
        });
        txt_telephone.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                txt_telephone.setFocusableInTouchMode(true);
                return false;
            }
        });
        txt_adresse.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                txt_adresse.setFocusableInTouchMode(true);
                return false;
            }
        });
        txt_indicatif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        txt_indicatif.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                init_sect_indicatif = 1;
                r_sect_indicatif.setVisibility(View.VISIBLE);
                txt_indicatif.setFocusable(false);
                txt_prenom.setEnabled(false);
                txt_nom.setEnabled(false);
                txt_indicatif.setEnabled(false);
                txt_telephone.setEnabled(false);
                txt_adresse.setEnabled(false);
                radio_femme.setEnabled(false);
                radio_homme.setEnabled(false);
                l_sect_manage_profil.setEnabled(false);
                return false;
            }
        });
        list_indicatifs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Indicatifs indicAct = indicatifs.get(position);
                txt_indicatif.setText(indicAct.getIndicatif());
                txt_telephone.setText("");

                init_sect_indicatif = 0;
                r_sect_indicatif.setVisibility(View.GONE);
                txt_indicatif.setFocusable(true);
                txt_prenom.setEnabled(true);
                txt_nom.setEnabled(true);
                txt_indicatif.setEnabled(true);
                txt_telephone.setEnabled(true);
                txt_adresse.setEnabled(true);
                radio_femme.setEnabled(true);
                radio_homme.setEnabled(true);
                l_sect_manage_profil.setEnabled(true);
            }
        });
        l_sect_manage_profil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (permCamera != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(Accompte.this,
                            new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                } else {
                    init_camera = 1;
                    scroll_all_info.setVisibility(View.GONE);
                    fram_select_image.setVisibility(View.VISIBLE);
                    mCamera.start();
                    mCamera.setFocus(CameraKit.Constants.FOCUS_TAP_WITH_MARKER);
                }
            }
        });
        flash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (permCamera != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(Accompte.this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                } else {
                    if (mCamera.isStarted()) {
                        mCamera.stop();
                    }
                    /*Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
                    getIntent.setType("image/*");

                    Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    pickIntent.setType("image/*");

                    Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{pickIntent});

                    startActivityForResult(chooserIntent, 4);*/
                    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.setType("image/*");

                    startActivityForResult(intent, READ_REQUEST_CODE);
                }
            }
        });
        clickme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCamera.captureImage(new CameraKitEventCallback<CameraKitImage>() {
                    @Override
                    public void callback(CameraKitImage cameraKitImage) {
                        if (cameraKitImage.getJpeg() != null) {
                            synchronized (cameraKitImage) {
                                File photo = Utility.writeImage(cameraKitImage.getJpeg());
                                Log.d(TAG, "Image: " + photo);
                                try {
                                    //scroll_all_info.setVisibility(View.GONE);
                                    scaled = getScaledBitmap(512, getExcifCorrectedBitmap(photo));
                                    mCamera.stop();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            Log.d(TAG, "Erreur");
                        }

                    }
                });
                mCamera.captureImage();
                r_loader_login.setVisibility(View.VISIBLE);
                new CountDownTimer(init_valeur_time, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        Log.d(TAG, "Bitmap :" + scaled);
                        if (scaled != null) {
                            fram_select_image.setVisibility(View.GONE);
                            l_sect_validate_upload.setVisibility(View.VISIBLE);
                            iv.setImageBitmap(scaled);
                            r_loader_login.setVisibility(View.GONE);
                        } else {
                            if (mCamera.getFacing() == 0) {
                                init_valeur_time = 2000;
                                new CountDownTimer(init_valeur_time, 500) {
                                    @Override
                                    public void onTick(long millisUntilFinished) {

                                    }

                                    @Override
                                    public void onFinish() {
                                        if (scaled != null) {
                                            fram_select_image.setVisibility(View.GONE);
                                            l_sect_validate_upload.setVisibility(View.VISIBLE);
                                            iv.setImageBitmap(scaled);
                                            r_loader_login.setVisibility(View.GONE);
                                        }
                                    }
                                }.start();
                            }
                        }
                    }
                }.start();
            }
        });
        front.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ObjectAnimator oa1 = ObjectAnimator.ofFloat(front, "scaleX", 1f, 0f).setDuration(150);
                final ObjectAnimator oa2 = ObjectAnimator.ofFloat(front, "scaleX", 0f, 1f).setDuration(150);
                oa1.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        //front.setImageResource(R.mipmap.ic_camera_capture);
                        oa2.start();
                    }
                });
                oa1.start();
                mCamera.setFacing((mCamera.getFacing() == CameraKit.Constants.FACING_FRONT) ? CameraKit.Constants.FACING_BACK : CameraKit.Constants.FACING_FRONT);
                Log.d(TAG,"Cam :"+mCamera.getFacing());
                if (mCamera.getFacing() == 1){
                    init_valeur_time = 2000;
                }else {
                    scaled = null;
                    init_valeur_time = 5700;
                }
            }
        });
        Log.d(TAG, "Profil: " + sessionManagerLogin.getLoggedInUser().getProfil());
        btn_save_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String prenom = txt_prenom.getText().toString().trim();
                final String nom = txt_nom.getText().toString().trim();
                final String adresse = txt_adresse.getText().toString().trim();
                final String sexe = valueGenre;
                final String indicatif = txt_indicatif.getText().toString().trim();
                final String tel = txt_telephone.getText().toString().trim();
                if (prenom.equals("")) {
                    txt_prenom_error.setText(R.string.txt_prenom_error);
                    txt_prenom_error.setVisibility(View.VISIBLE);
                }
                if (nom.equals("")) {
                    txt_nom_error.setText(R.string.txt_nom_error);
                    txt_nom_error.setVisibility(View.VISIBLE);
                }
                if (!prenom.equals("") && !nom.equals("")) {
                    r_loader_login.setVisibility(View.VISIBLE);
                    txt_prenom_error.setText("");
                    txt_prenom_error.setVisibility(View.GONE);
                    txt_nom_error.setText("");
                    txt_nom_error.setVisibility(View.GONE);

                    Map<String, Object> Udp = new HashMap<>();
                    Udp.put("nom", nom);
                    Udp.put("prenom", prenom);
                    Udp.put("adresse", adresse);
                    Udp.put("genre", sexe);
                    Udp.put("indicatif",indicatif);
                    Udp.put("tel",tel);
                    db.collection("Users").document(sessionManagerLogin.getLoggedInUser().getEmail()).update(Udp).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Users userd = new Users();
                                userd.setNom(nom);
                                userd.setPrenom(prenom);
                                userd.setAdresse(adresse);
                                userd.setSexe(sexe);
                                userd.setIndicatif(indicatif);
                                userd.setTel(tel);
                                sessionManagerLogin.manageProfil(userd);
                                if (activity.equals("home")) {
                                    startActivity(new Intent(getApplicationContext(), Home.class));
                                    finish();
                                } else {
                                    finish();
                                }
                            }
                        }
                    });
                }
            }
        });
        btn_no_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                r_loader_login.setVisibility(View.VISIBLE);
                new CountDownTimer(1500, 600) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        Glide.with(getApplicationContext()).load(sessionManagerLogin.getLoggedInUser().getProfil()).centerCrop().crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(img_profil);
                        r_loader_login.setVisibility(View.GONE);
                        r_confirme_manage_profil.setVisibility(View.GONE);
                    }
                }.start();
            }
        });
        btn_deconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                r_loader_login.setVisibility(View.VISIBLE);
                r_confirme_manage_profil.setVisibility(View.GONE);
                uploadImg(sessionManagerLogin.getLoggedInUser().getTel());
            }
        });
        img_profil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (permCamera != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(Accompte.this,
                                new String[]{Manifest.permission.CAMERA},
                                REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                    } else {
                        scroll_all_info.setVisibility(View.GONE);
                        fram_select_image.setVisibility(View.VISIBLE);
                        mCamera.start();
                        mCamera.setFocus(CameraKit.Constants.FOCUS_TAP_WITH_MARKER);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        /*====================================================================*/
        btn_cancel_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (permCamera != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(Accompte.this,
                                new String[]{Manifest.permission.CAMERA},
                                REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                    } else {
                        l_sect_validate_upload.setVisibility(View.GONE);
                        scroll_all_info.setVisibility(View.GONE);
                        fram_select_image.setVisibility(View.VISIBLE);
                        mCamera.start();
                        mCamera.setFocus(CameraKit.Constants.FOCUS_TAP_WITH_MARKER);
                        //init_valeur_time = 5500;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        btn_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    if (sessionManagerLogin.getLoggedInUser().getType().equals("profil")) {
                        storage.getReference().child("webroot/users/" + sessionManagerLogin.getLoggedInUser().getEmail()).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                r_loader_login.setVisibility(View.VISIBLE);
                                iv.setDrawingCacheEnabled(true);
                                iv.buildDrawingCache();
                                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                                Bitmap imgBit = ((BitmapDrawable) iv.getDrawable()).getBitmap();
                                imgBit.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                                byte[] data = baos.toByteArray();

                                UploadTask uploadTask = storageRef.putBytes(data);
                                uploadTask.addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.d(TAG, "Erreur d'upload");
                                    }
                                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                    @Override
                                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                        Log.d(TAG, "Img upload :" + taskSnapshot.getDownloadUrl().toString());
                                        FirebaseFirestore dt_user = FirebaseFirestore.getInstance();
                                        DocumentReference docRef = dt_user.collection("Users").document(mAuth.getCurrentUser().getEmail());
                                        final Map<String, Object> userCl = new HashMap<>();
                                        userCl.put("profil", taskSnapshot.getDownloadUrl().toString());
                                        docRef.update(userCl).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    sessionManagerLogin.manageImgProfil(taskSnapshot.getDownloadUrl().toString());
                                                    Glide.with(getApplicationContext())
                                                            .load(taskSnapshot.getDownloadUrl().toString())
                                                            .centerCrop()
                                                            .crossFade()
                                                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                                                            .into(img_profil);
                                                    r_loader_login.setVisibility(View.GONE);
                                                    l_sect_validate_upload.setVisibility(View.GONE);
                                                    scroll_all_info.setVisibility(View.VISIBLE);
                                                }
                                            }
                                        });
                                    }
                                });
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.d(TAG, "Erreur de suppression");
                            }
                        });
                    }else {
                        r_loader_login.setVisibility(View.VISIBLE);
                        iv.setDrawingCacheEnabled(true);
                        iv.buildDrawingCache();
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        Bitmap imgBit = ((BitmapDrawable) iv.getDrawable()).getBitmap();
                        imgBit.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                        byte[] data = baos.toByteArray();

                        UploadTask uploadTask = storageRef.putBytes(data);
                        uploadTask.addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.d(TAG, "Erreur d'upload");
                            }
                        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                Log.d(TAG, "Img upload :" + taskSnapshot.getDownloadUrl().toString());
                                FirebaseFirestore dt_user = FirebaseFirestore.getInstance();
                                DocumentReference docRef = dt_user.collection("Users").document(mAuth.getCurrentUser().getEmail());
                                final Map<String, Object> userCl = new HashMap<>();
                                userCl.put("profil", taskSnapshot.getDownloadUrl().toString());
                                userCl.put("type","profil");
                                docRef.update(userCl).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            sessionManagerLogin.manageImgProfil(taskSnapshot.getDownloadUrl().toString());
                                            Glide.with(getApplicationContext())
                                                    .load(taskSnapshot.getDownloadUrl().toString())
                                                    .centerCrop()
                                                    .crossFade()
                                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                                    .into(img_profil);
                                            r_loader_login.setVisibility(View.GONE);
                                            l_sect_validate_upload.setVisibility(View.GONE);
                                            scroll_all_info.setVisibility(View.VISIBLE);
                                        }
                                    }
                                });
                            }
                        });
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        /*===================================================================*/
        /*btn_save_modification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!prenom.equals("") && !nom.equals("")) {
                    r_loader_login.setVisibility(View.VISIBLE);
                    JSONObject j_manage = manage_info(sessionManagerLogin.getLoggedInUser().getTel(), nom, prenom, adresse);
                    final String rep = j_manage.optString("message");
                    new CountDownTimer(1500, 500) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {
                            if (rep.equals("ok")) {
                                r_loader_login.setVisibility(View.GONE);
                                startActivity(new Intent(getApplicationContext(), Home.class));
                                finish();
                            } else {
                                r_alert_user_success.setVisibility(View.VISIBLE);
                                r_alert_user_success.startAnimation(animSideTop);
                                txt_alert_user_success.setText("Une erreur est survenue lors de la modification de vos inos!");
                                new CountDownTimer(3000, 1000) {
                                    @Override
                                    public void onTick(long l) {

                                    }

                                    @Override
                                    public void onFinish() {
                                        r_alert_user_success.setVisibility(View.GONE);
                                        r_alert_user_success.startAnimation(animSideFadeOut);
                                    }
                                }.start();
                            }
                        }
                    }.start();
                } else {
                    r_alert_user_success.setVisibility(View.VISIBLE);
                    r_alert_user_success.startAnimation(animSideTop);
                    txt_alert_user_success.setText("Vous devez indiquer votre nom et prénom!");
                    new CountDownTimer(3000, 1000) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {
                            r_alert_user_success.setVisibility(View.GONE);
                            r_alert_user_success.startAnimation(animSideFadeOut);
                        }
                    }.start();
                }
            }
        });*/
        /*========================================================*/
    }

    /*==========================================================*/
    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.radio_homme:
                radio_homme.setChecked(true);
                radio_femme.setChecked(false);
                valueGenre = "Homme";
                break;
            case R.id.radio_femme:
                radio_femme.setChecked(true);
                radio_homme.setChecked(false);
                valueGenre = "Femme";
                break;
        }
    }

    /*==========================================================*/
    //Mes méthodes overrides
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            if (init_sect_indicatif == 1){
                init_sect_indicatif = 0;
                r_sect_indicatif.setVisibility(View.GONE);
                txt_indicatif.setFocusable(true);
                txt_prenom.setEnabled(true);
                txt_nom.setEnabled(true);
                txt_indicatif.setEnabled(true);
                txt_telephone.setEnabled(true);
                txt_adresse.setEnabled(true);
                radio_femme.setEnabled(true);
                radio_homme.setEnabled(true);
                l_sect_manage_profil.setEnabled(true);
            }else {
                if (init_camera == 1){
                    init_camera = 0;
                    scroll_all_info.setVisibility(View.VISIBLE);
                    fram_select_image.setVisibility(View.GONE);
                    mCamera.stop();
                }else {
                    if (activity.equals("home")) {
                        if (mCamera.isStarted()) {
                            mCamera.stop();
                        }
                        startActivity(new Intent(getApplicationContext(), Home.class));
                        finish();
                    } else {
                        finish();
                    }
                }
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            if (init_sect_indicatif == 1){
                init_sect_indicatif = 0;
                r_sect_indicatif.setVisibility(View.GONE);
                txt_indicatif.setFocusable(true);
                txt_prenom.setEnabled(true);
                txt_nom.setEnabled(true);
                txt_indicatif.setEnabled(true);
                txt_telephone.setEnabled(true);
                txt_adresse.setEnabled(true);
                radio_femme.setEnabled(true);
                radio_homme.setEnabled(true);
                l_sect_manage_profil.setEnabled(true);
            }else {
                if (init_camera == 1){
                    init_camera = 0;
                    scroll_all_info.setVisibility(View.VISIBLE);
                    fram_select_image.setVisibility(View.GONE);
                    mCamera.stop();
                }else {
                    if (activity.equals("home")) {
                        if (mCamera.isStarted()) {
                            mCamera.stop();
                        }
                        startActivity(new Intent(getApplicationContext(), Home.class));
                        finish();
                    } else {
                        finish();
                    }
                }
            }
        }
        return false;
    }

    /*==========================================================*/
    /*============================================================*/
    private boolean uploadImg(String tel) {
        if (!SlectedPhoto.equals("")) {
            try {
                Bitmap bitmap = ImageLoader.init().from(SlectedPhoto).requestSize(512, 512).getBitmap();
                String encodedImage = ImageBase64.encode(bitmap);

                HashMap<String, String> postData = new HashMap<String, String>();
                postData.put("image", encodedImage);

                PostResponseAsyncTask task = new PostResponseAsyncTask(Accompte.this, postData, new AsyncResponse() {
                    @Override
                    public void processFinish(String s) {
                        new CountDownTimer(4000, 1000) {

                            public void onTick(long millisUntilFinished) {
                                //nothing
                            }

                            public void onFinish() {
                                r_alert_user_success.setVisibility(View.VISIBLE);
                                r_alert_user_success.startAnimation(animSideTop);
                                txt_alert_user_success.setText("Image de profil mise à jour avec succès!");
                                new CountDownTimer(3000, 1000) {
                                    @Override
                                    public void onTick(long l) {

                                    }

                                    @Override
                                    public void onFinish() {
                                        r_alert_user_success.setVisibility(View.GONE);
                                        r_alert_user_success.startAnimation(animSideFadeOut);
                                        Intent intent = new Intent(getApplicationContext(), Home.class);
                                        //intent.putExtra("active","ok");
                                        startActivity(intent);
                                        finish();
                                    }
                                }.start();
                            }
                        }.start();
                    }
                });
                task.execute(lien + "upload.php?tel=" + sessionManagerLogin.getLoggedInUser());

                task.setEachExceptionsHandler(new EachExceptionsHandler() {
                    @Override
                    public void handleIOException(IOException e) {

                    }

                    @Override
                    public void handleMalformedURLException(MalformedURLException e) {
                        Toast.makeText(getApplicationContext(), "URL Error.",
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void handleProtocolException(ProtocolException e) {
                        Toast.makeText(getApplicationContext(), "Protocol Error.",
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void handleUnsupportedEncodingException(UnsupportedEncodingException e) {
                        Toast.makeText(getApplicationContext(), "Encoding Error.",
                                Toast.LENGTH_SHORT).show();
                    }
                });
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "Result :" + requestCode);
        Log.d(TAG, "ResultCode :" + resultCode);
        try {
            if (requestCode == 4) {
                Uri uri = null;
                if (data != null) {
                    uri = data.getData();
                    galleryPhoto.setPhotoUri(uri);
                    final String photoPath = galleryPhoto.getPath();
                    File photo = new File(photoPath);
                    SlectedPhoto = photoPath;
                    scaled = getScaledBitmap(512, getExcifCorrectedBitmap(photo));
                    fram_select_image.setVisibility(View.GONE);
                    l_sect_validate_upload.setVisibility(View.VISIBLE);
                    iv.setImageBitmap(scaled);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == GALLERY_REQUEST){
            Uri uri = null;
            if (data != null) {
                r_loader_login.setVisibility(View.VISIBLE);
                uri = data.getData();
                galleryPhoto.setPhotoUri(uri);
                final String photoPath = galleryPhoto.getPath();
                SlectedPhoto = photoPath;
                new CountDownTimer(1500,600){
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        Bitmap bitmap = null;
                        try {
                            bitmap = ImageLoader.init().from(photoPath).requestSize(512, 512).getBitmap();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        img_profil.setImageBitmap(bitmap);
                        img_manage_profil.setEnabled(false);
                        nom_user.setEnabled(false);
                        prenom_user.setEnabled(false);
                        r_loader_login.setVisibility(View.GONE);
                        r_confirme_manage_profil.setVisibility(View.VISIBLE);
                    }
                }.start();
            }
        }
    }*/

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(TAG, "requestCode :" + requestCode);
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //mCamera = findViewById(R.id.camera);
                    mCamera.start();
                    mCamera.setFocus(CameraKit.Constants.FOCUS_TAP_WITH_MARKER);
                    //r_loader_login.setVisibility(View.VISIBLE);
                } else {
                    if (activity.equals("home")) {
                        startActivity(new Intent(getApplicationContext(), Home.class));
                        finish();
                    } else {
                        finish();
                    }
                }
            }
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mCamera.stop();
                    Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
                    getIntent.setType("image/*");

                    Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    pickIntent.setType("image/*");

                    Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{pickIntent});

                    startActivityForResult(chooserIntent, 4);
                } else {
                    if (activity.equals("home")) {
                        startActivity(new Intent(getApplicationContext(), Home.class));
                        finish();
                    } else {
                        finish();
                    }
                }
            }
        }
    }

    private void topAnimation() {
        animSideTop = AnimationUtils.loadAnimation(this, R.anim.push_top_in);
        animSideTop.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void fadeOutAnimation() {
        animSideFadeOut = AnimationUtils.loadAnimation(this, R.anim.disappear);
        animSideFadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

  /*  private void prepareChooser() {
        Croperino.prepareChooser(Accompte.this, "Prendre une photo...", ContextCompat.getColor(Accompte.this, R.color.colorPrimary));
    }

    private void prepareCamera() {
        Croperino.prepareCamera(Accompte.this);
    }*/

    /*==================================================================*/
    private JSONObject manage_info(String tel, String nom, String prenom, String adresse) {
        String result = "";
        JSONObject jsonObject = null;
        OkHttpClient client = new OkHttpClient();
        RequestBody body = new FormBody.Builder()
                .add("token", token)
                .add("tel", tel)
                .add("nom", nom)
                .add("prenom", prenom)
                .add("adresse", adresse)
                .build();

        Request request = new Request.Builder()
                .url(lien + "Users/manage_compte.php")
                .addHeader("Content-Type", "text/json; Charset=UTF-8")
                .post(body)
                .build();
        Response response = null;
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            response = client.newCall(request).execute();
            if (response.code() == 200) {
                result = response.body().string();
                if (result != null) {
                    jsonObject = new JSONObject(result);
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (response != null) {
                response.body().close();
            }
        }
        return jsonObject;
    }

    /*=========================================================================*/
    public static Bitmap getScaledBitmap(int maxWidth, Bitmap rotatedBitmap) {
        int nh = (int) (rotatedBitmap.getHeight() * (512.0 / rotatedBitmap.getWidth()));
        Bitmap scaled = Bitmap.createScaledBitmap(rotatedBitmap, maxWidth, nh, true);
        return scaled;
    }

    public static Bitmap getExcifCorrectedBitmap(File f) {
        BitmapFactory.Options bounds = new BitmapFactory.Options();
        bounds.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(f.getAbsolutePath(), bounds);

        BitmapFactory.Options opts = new BitmapFactory.Options();
        Bitmap bm = BitmapFactory.decodeFile(f.getAbsolutePath(), opts);
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(f.getAbsolutePath().toString());

            String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
            int orientation = orientString != null ? Integer.parseInt(orientString) : ExifInterface.ORIENTATION_NORMAL;

            int rotationAngle = 0;
            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) rotationAngle = 90;
            if (orientation == ExifInterface.ORIENTATION_ROTATE_180) rotationAngle = 180;
            if (orientation == ExifInterface.ORIENTATION_ROTATE_270) rotationAngle = 270;

            Matrix matrix = new Matrix();
            matrix.setRotate(rotationAngle, (float) bm.getWidth() / 2, (float) bm.getHeight() / 2);
            Bitmap rotatedBitmap = Bitmap.createBitmap(bm, 0, 0, bounds.outWidth, bounds.outHeight, matrix, true);
            return rotatedBitmap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    /*=========================================================================*/

    @Override
    protected void onPause() {
        super.onPause();
        if (mCamera.isStarted()) {
            mCamera.stop();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCamera.isStarted()) {
            mCamera.stop();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCamera.start();
    }
    /*=========================================================================*/
    //La liste des indicatifs
    private void get_indicatifs(){
        db.collection("public").document("parameters").collection("indicatifs").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {

            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()){
                    //Log.d(TAG,"TAG :"+task.getResult().size());
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        //Log.d(TAG,"drapeau :"+document.get("drapeau"));
                        indica = new Indicatifs();
                        indica.setIso(document.getId());
                        indica.setDrapeau(document.get("drapeau").toString());
                        indica.setIndicatif(document.get("indicatif").toString());
                        indica.setIntitule(document.get("intitule").toString());
                        indicatifs.add(indica);
                    }

                    if (indicatifs.size() > 0){
                        indicatif_adapter = new Indicatif_Adapter(Accompte.this,Accompte.this,indicatifs);
                        list_indicatifs.setAdapter(indicatif_adapter);
                    }
                }
            }
        });
    }
    /*=========================================================================*/
}
