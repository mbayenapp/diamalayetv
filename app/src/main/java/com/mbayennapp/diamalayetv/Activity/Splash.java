package com.mbayennapp.diamalayetv.Activity;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;

import com.crashlytics.android.Crashlytics;
import com.mbayennapp.diamalayetv.Helpers.SessionManagerLogin;
import com.mbayennapp.diamalayetv.Helpers.SessionManagerRegister;
import com.mbayennapp.diamalayetv.R;
import com.mbayennapp.diamalayetv.Utils.ConnexionInternet;
import com.onesignal.OneSignal;

import io.fabric.sdk.android.Fabric;

public class Splash extends AppCompatActivity {
    private static final String TAG = Splash.class.getSimpleName();
    private String lien = "https://www.mabcompany.com/JL_API/";
    private SessionManagerLogin sessionManagerLogin;
    private SessionManagerRegister sessionManagerRegister;
    ConnexionInternet connexionInternet;
    /*===============================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        /*==========================================================*/
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
        /*==========================================================*/
        try {
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancelAll();
        }catch (Exception e){
            e.printStackTrace();
        }
        /*==========================================================*/
        connexionInternet = new ConnexionInternet();
        sessionManagerLogin = new SessionManagerLogin(this);
        sessionManagerRegister = new SessionManagerRegister(this);
        /*if (connexionInternet.isConnectedInternet(Splash.this)) {
            FirebaseMessaging.getInstance().subscribeToTopic("ClientFcm");
            FirebaseInstanceId.getInstance().getToken();
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            Log.d(TAG, "Token:" + FirebaseInstanceId.getInstance().getToken());

            OkHttpClient client = new OkHttpClient();
            if (FirebaseInstanceId.getInstance().getToken() != null) {
                RequestBody body = new FormBody.Builder()
                        .add("Token", FirebaseInstanceId.getInstance().getToken())
                        .build();

                Request request = new Request.Builder()
                        .url(lien + "Users/add_token.php")
                        .post(body)
                        .build();

                try {
                    client.newCall(request).execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }*/
        final Fabric fabric = new Fabric.Builder(this)
                .kits(new Crashlytics())
                .debuggable(true)
                .build();
        Fabric.with(fabric);
        /*==========================================================*/
        //Démarrer mon activité principale
        new CountDownTimer(3000,1000){

            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                startActivity(new Intent(getApplicationContext(), Home.class));
                finish();
                /*if (sessionManagerRegister.isLoggedIn()){
                    startActivity(new Intent(getApplicationContext(), Register.class));
                    finish();
                }else {
                    if (sessionManagerLogin.isLoggedIn()) {
                        startActivity(new Intent(getApplicationContext(), Home.class));
                        finish();
                    } else {
                        startActivity(new Intent(getApplicationContext(), Login.class));
                        finish();
                    }
                }*/
            }
        }.start();
        /*==========================================================*/
    }

}
