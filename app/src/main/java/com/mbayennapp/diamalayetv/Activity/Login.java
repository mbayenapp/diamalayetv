package com.mbayennapp.diamalayetv.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.LoggingBehavior;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mbayennapp.diamalayetv.BuildConfig;
import com.mbayennapp.diamalayetv.Helpers.SessionManagerLogin;
import com.mbayennapp.diamalayetv.Model.Users;
import com.mbayennapp.diamalayetv.R;
import com.mbayennapp.diamalayetv.Utils.Properties;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Login extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    private String lien = "https://www.mabcompany.com/JL_API/";
    private String token = "layenne2017";
    private static final String TAG = Login.class.getSimpleName();
    private RelativeLayout r_ask_login, r_alert_user, r_loader_login;
    private Button btn_go_login, btn_register_user, btn_login;
    private ScrollView scroll_login;
    private Animation animSideUp, animSideTop, animSideFadeOut;
    private ImageView img_go_back;
    private TextView txt_alert_user, forgot_password,
            txt_my_txt_connect, txt_taxim_ma, txt_visite_taxim,
            txt_en_save_plus, txt_password_error, txt_email_error;
    private TextInputEditText txt_email, txt_password;
    private int init_act = 0;
    private int sdk;
    private Users user;
    private SessionManagerLogin sessionManagerLogin;
    private SignInButton sign_in_button;
    private static final int RC_SIGN_IN = 9001;
    private GoogleSignInClient mGoogleSignInClient;
    private GoogleSignInAccount account;
    private GoogleApiClient googleApiClient;
    /*=============================================================*/
    private FirebaseAuth mAuth;
    private Properties properties;
    private CallbackManager callbackManager;
    private LoginButton loginButton;
    private String myemail = "";
    /*=============================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        /*=========================================================*/
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(getApplication());
        if (BuildConfig.DEBUG) {
            FacebookSdk.setIsDebugEnabled(true);
            FacebookSdk.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
            FacebookSdk.addLoggingBehavior(LoggingBehavior.REQUESTS);
        }
        /*=========================================================*/
        properties = new Properties();
        Log.d(TAG, "Token : " + properties.getIdClient());
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(properties.getIdClient())
                .requestEmail()
                .requestProfile()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        googleApiClient = new GoogleApiClient
                .Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        callbackManager = CallbackManager.Factory.create();
        //
        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() != null) {
            mAuth.signOut();

        }
        mGoogleSignInClient.signOut();
        mGoogleSignInClient.revokeAccess();
        /*=========================================================*/
        r_ask_login = (RelativeLayout) findViewById(R.id.r_ask_login);
        btn_go_login = (Button) findViewById(R.id.btn_go_login);
        btn_register_user = (Button) findViewById(R.id.btn_register_user);
        scroll_login = (ScrollView) findViewById(R.id.scroll_login);
        img_go_back = (ImageView) findViewById(R.id.img_go_back);
        r_alert_user = (RelativeLayout) findViewById(R.id.r_alert_user);
        txt_alert_user = (TextView) findViewById(R.id.txt_alert_user);
        txt_email = (TextInputEditText) findViewById(R.id.txt_email);
        txt_password = (TextInputEditText) findViewById(R.id.txt_password);
        btn_login = (Button) findViewById(R.id.btn_login);
        forgot_password = (TextView) findViewById(R.id.forgot_password);
        r_loader_login = (RelativeLayout) findViewById(R.id.r_loader_login);
        btn_register_user = (Button) findViewById(R.id.btn_register_user);
        txt_my_txt_connect = (TextView) findViewById(R.id.txt_my_txt_connect);
        txt_taxim_ma = (TextView) findViewById(R.id.txt_taxim_ma);
        txt_visite_taxim = (TextView) findViewById(R.id.txt_visite_taxim);
        txt_en_save_plus = (TextView) findViewById(R.id.txt_en_save_plus);
        sign_in_button = (SignInButton) findViewById(R.id.sign_in_button);
        txt_password_error = (TextView) findViewById(R.id.txt_password_error);
        txt_email_error = (TextView) findViewById(R.id.txt_email_error);
        loginButton = (LoginButton) findViewById(R.id.login_button);
        /*=========================================================*/
        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeueLTStd_Roman.otf");
        Typeface typegold = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeuecondenced.otf");
        btn_go_login.setTypeface(typegold);
        btn_register_user.setTypeface(typegold);
        btn_login.setTypeface(typegold);
        txt_email.setTypeface(typeFace);
        txt_password.setTypeface(typeFace);
        txt_alert_user.setTypeface(typeFace);
        forgot_password.setTypeface(typeFace);
        txt_my_txt_connect.setTypeface(typeFace);
        txt_taxim_ma.setTypeface(typeFace);
        txt_visite_taxim.setTypeface(typeFace);
        txt_en_save_plus.setTypeface(typeFace);
        txt_password_error.setTypeface(typeFace);
        txt_email_error.setTypeface(typeFace);
        /*=========================================================*/
        downAnimation();
        topAnimation();
        fadeOutAnimation();
        sdk = android.os.Build.VERSION.SDK_INT;
        sessionManagerLogin = new SessionManagerLogin(this);
        /*=========================================================*/
        LoginManager.getInstance().logOut();
        loginButton.setPadding(30, 30, 30, 30);
        loginButton.setTypeface(typegold);
        loginButton.setReadPermissions(Arrays.asList("email", "public_profile","user_friends"));
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess : " + loginResult);
                GraphRequest mGraphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        if (response.getError() != null) {

                        } else {
                            try {
                                Log.d(TAG, "Info :" + response.toString());
                                String USERID = response.getJSONObject().get("id").toString();
                                /*new GraphRequest(
                                        AccessToken.getCurrentAccessToken(),
                                        "/{"+USERID+"}/friends",
                                        null,
                                        HttpMethod.GET,
                                        new GraphRequest.Callback() {
                                            public void onCompleted(GraphResponse response) {
                                                Log.d(TAG,"Liste des amis: "+response.toString());
                                            }
                                        }
                                ).executeAsync();*/
                                /*=================================================================*/
                                GraphRequest request = GraphRequest.newMeRequest(
                                        AccessToken.getCurrentAccessToken(),
                                        new GraphRequest.GraphJSONObjectCallback() {
                                            @Override
                                            public void onCompleted(JSONObject object, GraphResponse response) {
                                                Log.d(TAG,"Liste des amis: "+response.toString());

                                            }
                                        });

                                Bundle parameters = new Bundle();
                                parameters.putString("fields", "id,name,email,birthday,gender,first_name,last_name,picture,education,work,friends");
                                request.setParameters(parameters);
                                request.executeAsync();
                                /*=================================================================*/
                                myemail = response.getJSONObject().get("email").toString();
                                r_loader_login.setVisibility(View.VISIBLE);
                                handleFacebookAccessToken(loginResult.getAccessToken());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email");
                mGraphRequest.setParameters(parameters);
                mGraphRequest.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
                txt_email.setEnabled(true);
                txt_password.setEnabled(true);
                btn_login.setEnabled(true);
                forgot_password.setEnabled(true);
                r_loader_login.setVisibility(View.GONE);
                r_alert_user.setVisibility(View.VISIBLE);
                txt_alert_user.setText("Connexion à facebook annulée!");
                new CountDownTimer(3000, 1000) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        r_alert_user.setVisibility(View.GONE);
                        r_alert_user.startAnimation(animSideFadeOut);
                    }
                }.start();
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError: ", error);
                txt_email.setEnabled(true);
                txt_password.setEnabled(true);
                btn_login.setEnabled(true);
                forgot_password.setEnabled(true);
                r_loader_login.setVisibility(View.GONE);
                r_alert_user.setVisibility(View.VISIBLE);
                txt_alert_user.setText("Une erreur est survenue lors de la connexion à facebook!");
                new CountDownTimer(3000, 1000) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        r_alert_user.setVisibility(View.GONE);
                        r_alert_user.startAnimation(animSideFadeOut);
                    }
                }.start();
            }
        });
        /*=========================================================*/
        //Mes traitements
        btn_register_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), Register.class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                finish();
            }
        });
        btn_go_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                r_ask_login.setVisibility(View.GONE);
                scroll_login.setVisibility(View.VISIBLE);
                scroll_login.startAnimation(animSideTop);
                init_act = 1;
            }
        });
        img_go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                r_ask_login.setVisibility(View.VISIBLE);
                r_ask_login.startAnimation(animSideUp);
                scroll_login.setVisibility(View.GONE);
                init_act = 0;
            }
        });
        sign_in_button.setSize(SignInButton.SIZE_STANDARD);
        sign_in_button.setTextAlignment(SignInButton.TEXT_ALIGNMENT_GRAVITY);
        TextView txt_connect_google = (TextView) sign_in_button.getChildAt(0);
        txt_connect_google.setText(R.string.txt_connect_google);
        txt_connect_google.setTextSize(18);
        txt_connect_google.setTypeface(typegold);
        txt_connect_google.setGravity(Gravity.LEFT | Gravity.CENTER);
        txt_connect_google.setPadding(132, 0, 0, 0);
        txt_connect_google.setTextColor(Color.parseColor("#000000"));
        sign_in_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    //Intent sign =  Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                    Intent sign = mGoogleSignInClient.getSignInIntent();
                    startActivityForResult(sign, RC_SIGN_IN);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = txt_email.getText().toString().trim();
                String password = txt_password.getText().toString().trim();
                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                if (email.equals("")) {
                    txt_email_error.setVisibility(View.VISIBLE);
                    txt_email_error.setText(R.string.txt_email_empty);
                    txt_email.setHintTextColor(Color.parseColor("#F44336"));
                } else if (!email.matches(emailPattern)) {
                    txt_email_error.setVisibility(View.VISIBLE);
                    txt_email_error.setText(R.string.txt_email_invalide);
                    txt_email.setHintTextColor(Color.parseColor("#F44336"));
                } else {
                    txt_email_error.setVisibility(View.GONE);
                    txt_email_error.setText("");
                    txt_email.setHintTextColor(Color.parseColor("#898B92"));
                }
                if (password.equals("")) {
                    txt_password_error.setVisibility(View.VISIBLE);
                    txt_password_error.setText(R.string.txt_password_empty);
                } else if (password.length() < 8) {
                    txt_password_error.setVisibility(View.VISIBLE);
                    txt_password_error.setText(R.string.txt_password_short);
                } else {
                    txt_password_error.setVisibility(View.GONE);
                    txt_password_error.setText("");
                }
                if (!email.equals("") && !password.equals("") && email.matches(emailPattern) && password.length() >= 8) {
                    txt_email_error.setVisibility(View.GONE);
                    txt_email_error.setText("");
                    txt_password_error.setVisibility(View.GONE);
                    txt_password_error.setText("");
                    txt_email.setEnabled(false);
                    txt_password.setEnabled(false);
                    btn_login.setEnabled(false);
                    forgot_password.setEnabled(false);
                    r_loader_login.setVisibility(View.VISIBLE);
                    mAuth.signInWithEmailAndPassword(email, properties.getHastStatique() + password).addOnCompleteListener(Login.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                FirebaseFirestore dt_user = FirebaseFirestore.getInstance();
                                DocumentReference docRef = dt_user.collection("Users").document(mAuth.getCurrentUser().getEmail());
                                docRef.get().addOnCompleteListener(Login.this, new OnCompleteListener<DocumentSnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                        try {
                                            if (task.isSuccessful()) {
                                                DocumentSnapshot user = task.getResult();
                                                if (user.get("statut").toString().equals("_ENABLED_")) {
                                                    Users usersSess = new Users();
                                                    usersSess.setId(mAuth.getCurrentUser().getUid());
                                                    usersSess.setNom(user.get("nom").toString());
                                                    usersSess.setPrenom(user.get("prenom").toString());
                                                    if (user.get("indicatif") != null) {
                                                        usersSess.setIndicatif(user.get("indicatif").toString());
                                                        usersSess.setTel(user.get("tel").toString());
                                                    }
                                                    if (user.get("adresse") != null) {
                                                        usersSess.setAdresse(user.get("adresse").toString());
                                                    }
                                                    if (user.get("codepostal") != null) {
                                                        usersSess.setCode_postal(user.get("codepostal").toString());
                                                    }
                                                    if (user.get("ville") != null) {
                                                        usersSess.setVille(user.get("ville").toString());
                                                    }
                                                    if (user.get("pays") != null) {
                                                        usersSess.setPays(user.get("pays").toString());
                                                    }
                                                    usersSess.setEmail(mAuth.getCurrentUser().getEmail());
                                                    usersSess.setProfil(user.get("profil").toString());
                                                    usersSess.setSexe(user.get("genre").toString());
                                                    usersSess.setType(user.get("type").toString());
                                                    sessionManagerLogin.storeUserData(usersSess);
                                                    r_loader_login.setVisibility(View.GONE);
                                                    Intent intent = new Intent(getApplicationContext(), Home.class);
                                                    startActivity(intent);
                                                    finish();
                                                }
                                            } else {
                                                txt_email.setEnabled(true);
                                                txt_password.setEnabled(true);
                                                btn_login.setEnabled(true);
                                                forgot_password.setEnabled(true);
                                                r_loader_login.setVisibility(View.GONE);
                                                r_alert_user.setVisibility(View.VISIBLE);
                                                txt_alert_user.setText("Aucun utilisateur ne correspond à cette adresse email!");
                                                new CountDownTimer(3000, 1000) {
                                                    @Override
                                                    public void onTick(long l) {

                                                    }

                                                    @Override
                                                    public void onFinish() {
                                                        r_alert_user.setVisibility(View.GONE);
                                                        r_alert_user.startAnimation(animSideFadeOut);
                                                    }
                                                }.start();
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                            } else {
                                Log.d(TAG, "Error: " + task.getException());
                                txt_email.setEnabled(true);
                                txt_password.setEnabled(true);
                                btn_login.setEnabled(true);
                                forgot_password.setEnabled(true);
                                r_loader_login.setVisibility(View.GONE);
                                r_alert_user.setVisibility(View.VISIBLE);
                                txt_alert_user.setText("Adresse email ou mot de passe incorrecte!");
                                new CountDownTimer(3000, 1000) {
                                    @Override
                                    public void onTick(long l) {

                                    }

                                    @Override
                                    public void onFinish() {
                                        r_alert_user.setVisibility(View.GONE);
                                        r_alert_user.startAnimation(animSideFadeOut);
                                    }
                                }.start();
                            }
                        }
                    });
                }
            }
        });
        forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), Forgot.class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                //finish();
            }
        });
        //Fin mes traitements
        /*=========================================================*/
    }

    /*=============================================================*/
    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        Log.d(TAG, "Creadent: " + credential.getSignInMethod());
        Log.d(TAG,"USER ID: "+token.getUserId());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            scroll_login.setVisibility(View.GONE);
                            r_loader_login.setVisibility(View.VISIBLE);
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser user = mAuth.getCurrentUser();
                            Log.d(TAG, "signInWithCredential:success");
                            Log.d(TAG, "Profil :" + myemail);
                            FirebaseFirestore dt_user = FirebaseFirestore.getInstance();
                            DocumentReference docRef = dt_user.collection("Users").document(myemail);
                            docRef.get().addOnCompleteListener(Login.this, new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                    try {
                                        if (task.isSuccessful()) {
                                            DocumentSnapshot userProf = task.getResult();
                                            if (userProf.exists()) {
                                                if (userProf.get("statut").toString().equals("_ENABLED_")) {
                                                    Log.d(TAG, "Uid: " + task.getResult().getId());
                                                    Users usersSess = new Users();

                                                    usersSess.setId(mAuth.getCurrentUser().getUid());
                                                    usersSess.setNom(userProf.get("nom").toString());
                                                    usersSess.setPrenom(userProf.get("prenom").toString());
                                                    if (userProf.get("indicatif") != null) {
                                                        usersSess.setIndicatif(userProf.get("indicatif").toString());
                                                        usersSess.setTel(userProf.get("tel").toString());
                                                    }
                                                    if (userProf.get("adresse") != null) {
                                                        usersSess.setAdresse(userProf.get("adresse").toString());
                                                    }
                                                    if (userProf.get("codepostal") != null) {
                                                        usersSess.setCode_postal(userProf.get("codepostal").toString());
                                                    }
                                                    if (userProf.get("ville") != null) {
                                                        usersSess.setVille(userProf.get("ville").toString());
                                                    }
                                                    if (userProf.get("pays") != null) {
                                                        usersSess.setPays(userProf.get("pays").toString());
                                                    }
                                                    usersSess.setEmail(myemail);
                                                    usersSess.setProfil(mAuth.getCurrentUser().getPhotoUrl().toString());
                                                    usersSess.setSexe(userProf.get("genre").toString());
                                                    usersSess.setType(userProf.get("type").toString());
                                                    sessionManagerLogin.storeUserData(usersSess);
                                                    r_loader_login.setVisibility(View.GONE);
                                                    Intent intent = new Intent(getApplicationContext(), Home.class);
                                                    startActivity(intent);
                                                    finish();
                                                }
                                            } else {

                                            }
                                        } else {
                                            txt_email.setEnabled(true);
                                            txt_password.setEnabled(true);
                                            btn_login.setEnabled(true);
                                            forgot_password.setEnabled(true);
                                            r_loader_login.setVisibility(View.GONE);
                                            r_alert_user.setVisibility(View.VISIBLE);
                                            txt_alert_user.setText("Aucun utilisateur ne correspond à cette adresse email!");
                                            new CountDownTimer(3000, 1000) {
                                                @Override
                                                public void onTick(long l) {

                                                }

                                                @Override
                                                public void onFinish() {
                                                    r_alert_user.setVisibility(View.GONE);
                                                    r_alert_user.startAnimation(animSideFadeOut);
                                                }
                                            }.start();
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                            //updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(Login.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            //updateUI(null);
                        }

                        // ...
                    }
                });
    }

    /*=============================================================*/
    private void startSignInGoogle() {
        Intent sign = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(sign, RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "Code reponse: " + requestCode);

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            // Log.d(TAG,"IdToken : "+task.getResult());
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);

                Log.d(TAG, "TokenId :" + account.getIdToken());
                processLoginGoogle(account);
            } catch (ApiException e) {
                Log.d(TAG, "signInResult:failed code=" + e.getStatusCode());

                //show toast
                Toast.makeText(this, "Failed to do Sign In : " + e.getStatusCode(), Toast.LENGTH_SHORT).show();
            }
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {

    }

    private void processLoginGoogle(GoogleSignInAccount acct) {
        try {
            AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
            mAuth.signInWithCredential(credential)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                scroll_login.setVisibility(View.GONE);
                                r_loader_login.setVisibility(View.VISIBLE);
                                // Sign in success, update UI with the signed-in user's information
                                FirebaseUser user = mAuth.getCurrentUser();
                                FirebaseFirestore dt_user = FirebaseFirestore.getInstance();
                                DocumentReference docRef = dt_user.collection("Users").document(mAuth.getCurrentUser().getEmail());
                                docRef.get().addOnCompleteListener(Login.this, new OnCompleteListener<DocumentSnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                        try {
                                            if (task.isSuccessful()) {
                                                DocumentSnapshot userProf = task.getResult();
                                                if (userProf.exists()) {
                                                    if (userProf.get("statut").toString().equals("_ENABLED_")) {
                                                        Log.d(TAG, "Uid: " + task.getResult().getId());
                                                        Users usersSess = new Users();

                                                        usersSess.setId(mAuth.getCurrentUser().getUid());
                                                        usersSess.setNom(userProf.get("nom").toString());
                                                        usersSess.setPrenom(userProf.get("prenom").toString());
                                                        if (userProf.get("indicatif") != null) {
                                                            usersSess.setIndicatif(userProf.get("indicatif").toString());
                                                            usersSess.setTel(userProf.get("tel").toString());
                                                        }
                                                        if (userProf.get("adresse") != null) {
                                                            usersSess.setAdresse(userProf.get("adresse").toString());
                                                        }
                                                        if (userProf.get("codepostal") != null) {
                                                            usersSess.setCode_postal(userProf.get("codepostal").toString());
                                                        }
                                                        if (userProf.get("ville") != null) {
                                                            usersSess.setVille(userProf.get("ville").toString());
                                                        }
                                                        if (userProf.get("pays") != null) {
                                                            usersSess.setPays(userProf.get("pays").toString());
                                                        }
                                                        usersSess.setEmail(myemail);
                                                        usersSess.setProfil(mAuth.getCurrentUser().getPhotoUrl().toString());
                                                        usersSess.setSexe(userProf.get("genre").toString());
                                                        usersSess.setType(userProf.get("type").toString());
                                                        sessionManagerLogin.storeUserData(usersSess);
                                                        r_loader_login.setVisibility(View.GONE);
                                                        Intent intent = new Intent(getApplicationContext(), Home.class);
                                                        startActivity(intent);
                                                        finish();
                                                    }
                                                } else {

                                                }
                                            } else {
                                                txt_email.setEnabled(true);
                                                txt_password.setEnabled(true);
                                                btn_login.setEnabled(true);
                                                forgot_password.setEnabled(true);
                                                r_loader_login.setVisibility(View.GONE);
                                                r_alert_user.setVisibility(View.VISIBLE);
                                                txt_alert_user.setText("Aucun utilisateur ne correspond à cette adresse email!");
                                                new CountDownTimer(3000, 1000) {
                                                    @Override
                                                    public void onTick(long l) {

                                                    }

                                                    @Override
                                                    public void onFinish() {
                                                        r_alert_user.setVisibility(View.GONE);
                                                        r_alert_user.startAnimation(animSideFadeOut);
                                                    }
                                                }.start();
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                                //updateUI(user);
                            } else {
                                // If sign in fails, display a message to the user.
                                Log.w(TAG, "signInWithCredential:failure", task.getException());
                                Toast.makeText(Login.this, "Authentication failed.",
                                        Toast.LENGTH_SHORT).show();
                                //updateUI(null);
                            }

                            // ...
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*=============================================================*/
    //Mes méthodes overrides
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            if (init_act == 1) {
                r_ask_login.setVisibility(View.VISIBLE);
                r_ask_login.startAnimation(animSideUp);
                scroll_login.setVisibility(View.GONE);
                r_loader_login.setVisibility(View.GONE);
                init_act = 0;
            } else {
                startActivity(new Intent(getApplicationContext(), Home.class));
                finish();
            }
        }
        return false;
    }

    /*============================================================*/
    //La méthode qui permet de connecter un utilisateur
    private JSONObject login_user(String tel, String password) {
        JSONObject jsonObject = null;
        OkHttpClient client = new OkHttpClient();
        //
        RequestBody body = new FormBody.Builder()
                .add("token", token)
                .add("tel", tel)
                .add("password", password)
                .build();

        final Request request = new Request.Builder()
                .url(lien + "Users/login.php")
                .addHeader("Content-Type", "text/json; Charset=UTF-8")
                .post(body)
                .build();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Response response = null;
        try {
            response = client.newCall(request).execute();
            if (response.code() == 200) {
                if (response.isSuccessful()) {
                    jsonObject = new JSONObject(response.body().string());
                    Log.d(TAG, "Info user:" + jsonObject);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (response != null) {
                response.body().close();
            }
        }
        return jsonObject;
    }
    /*============================================================*/

    /*=============================================================*/
    //Mes animations
    private void downAnimation() {
        animSideUp = AnimationUtils.loadAnimation(this, R.anim.push_bottom_in);
        animSideUp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void topAnimation() {
        animSideTop = AnimationUtils.loadAnimation(this, R.anim.push_top_in);
        animSideTop.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void fadeOutAnimation() {
        animSideFadeOut = AnimationUtils.loadAnimation(this, R.anim.disappear);
        animSideFadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
