package com.mbayennapp.diamalayetv.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.kosalgeek.android.photoutil.ImageBase64;
import com.kosalgeek.android.photoutil.ImageLoader;
import com.kosalgeek.genasync12.AsyncResponse;
import com.kosalgeek.genasync12.EachExceptionsHandler;
import com.kosalgeek.genasync12.PostResponseAsyncTask;
import com.mbayennapp.diamalayetv.Helpers.SessionManagerLogin;
import com.mbayennapp.diamalayetv.Helpers.SessionManagerRegister;
import com.mbayennapp.diamalayetv.Model.Users;
import com.mbayennapp.diamalayetv.R;
import com.mbayennapp.diamalayetv.Utils.Properties;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.util.HashMap;
import java.util.Map;

public class Register extends AppCompatActivity {
    private static final String TAG = Forgot.class.getSimpleName();
    private String lien = "https://www.mabcompany.com/JL_API/";
    private String token = "layenne2017";
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 0;
    private Toolbar toolbar;
    private TextView have_a_compte, txt_alert_user,txt_terms_and_condition,
            have_ask_compte,txt_auto_link,txt_email_error,txt_nom_error,
            txt_prenom_error,txt_password_error;
    private TextInputEditText txt_name,txt_prenom,txt_email,txt_password;
    private Button btn_register;
    private RelativeLayout r_my_loader,r_alert_user;
   // private CountryCodePicker contry_code;
    private LinearLayout l_sect_first;
    private SessionManagerRegister sessionManagerRegister;
    private Animation animSideTop,animSideFadeOut;
    private ImageView img_sel_file;
    private String SlectedPhoto = "";
    private SessionManagerLogin sessionManagerLogin;
    private String code = "";
    private int statutBar = 0;
    private Handler handler;
    /*======================================================*/
    private Properties properties;
    /*======================================================*/
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    /*======================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        /*======================================================*/
        toolbar = (Toolbar)findViewById(R.id.toolbar_register);
        toolbar.setTitle("Inscription");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sessionManagerRegister = new SessionManagerRegister(this);
        sessionManagerLogin = new SessionManagerLogin(this);
        topAnimation();
        fadeOutAnimation();
        /*======================================================*/
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        properties = new Properties();
        /*======================================================*/
        /*final int permissionCheeck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS);
        if (permissionCheeck != PackageManager.PERMISSION_GRANTED){
            if (ActivityCompat.shouldShowRequestPermissionRationale(Register.this, Manifest.permission.READ_SMS)){

            }else {
                ActivityCompat.requestPermissions(Register.this,
                        new String[]{Manifest.permission.READ_SMS,Manifest.permission.RECEIVE_SMS},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);
            }
        }*/
        /*========================================================*/
        txt_email = (TextInputEditText)findViewById(R.id.txt_email);
        txt_name = (TextInputEditText) findViewById(R.id.txt_name);
        txt_prenom = (TextInputEditText) findViewById(R.id.txt_prenom);
        txt_password = (TextInputEditText)findViewById(R.id.txt_password);
        btn_register = (Button)findViewById(R.id.btn_register);
        have_a_compte = (TextView)findViewById(R.id.have_a_compte);
        r_my_loader = (RelativeLayout)findViewById(R.id.r_my_loader);
        r_alert_user = (RelativeLayout)findViewById(R.id.r_alert_user);
        txt_alert_user = (TextView)findViewById(R.id.txt_alert_user);
        //contry_code = (CountryCodePicker)findViewById(R.id.contry_code);
       // img_sel_file = (ImageView)findViewById(R.id.img_sel_file);
        txt_terms_and_condition = (TextView)findViewById(R.id.txt_terms_and_condition);
        have_ask_compte = (TextView)findViewById(R.id.have_ask_compte);
        txt_auto_link = (TextView)findViewById(R.id.txt_auto_link);
        txt_email_error = (TextView)findViewById(R.id.txt_email_error);
        txt_nom_error = (TextView)findViewById(R.id.txt_nom_error);
        txt_prenom_error = (TextView)findViewById(R.id.txt_prenom_error);
        txt_password_error = (TextView)findViewById(R.id.txt_password_error);
        /*======================================================*/
        handler = new Handler();
        //contry_code.setCountryForPhoneCode(221);
        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeueLTStd_Roman.otf");
        Typeface typegold = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeuecondenced.otf");
        txt_email.setTypeface(typeFace);
        txt_name.setTypeface(typeFace);
        txt_prenom.setTypeface(typeFace);
        txt_password.setTypeface(typeFace);
        btn_register.setTypeface(typegold);
        have_a_compte.setTypeface(typeFace);
        txt_terms_and_condition.setTypeface(typeFace);
        have_ask_compte.setTypeface(typeFace);
        txt_auto_link.setTypeface(typeFace);
        txt_email_error.setTypeface(typeFace);
        txt_nom_error.setTypeface(typeFace);
        txt_prenom_error.setTypeface(typeFace);
        txt_password_error.setTypeface(typeFace);
        /*======================================================*/
        /*======================================================*/
        if (sessionManagerRegister.isLoggedIn()){
            getSupportActionBar().setHomeButtonEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        }
        /*======================================================*/
        /*txt_one.setFilters(new InputFilter[]{new InputFilter.LengthFilter(1)});
        txt_two.setFilters(new InputFilter[]{new InputFilter.LengthFilter(1)});
        txt_tree.setFilters(new InputFilter[]{new InputFilter.LengthFilter(1)});
        txt_four.setFilters(new InputFilter[]{new InputFilter.LengthFilter(1)});*/

        final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
     /*   txt_name.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        txt_prenom.setImeOptions(EditorInfo.IME_ACTION_NEXT);*/

        /*txt_name.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                txt_name.setFocusableInTouchMode(true);
                return false;
            }
        });
        txt_prenom.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                txt_prenom.setFocusableInTouchMode(true);
                return false;
            }
        });

        txt_name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (txt_name.getText().toString().trim().equals("")){
                    txt_name.setBackgroundResource(R.drawable.border_danger);
                    txt_name.setHintTextColor(Color.parseColor("#F44336"));
                }else {
                    txt_name.setBackgroundResource(R.drawable.border_edit);
                    txt_name.setHintTextColor(Color.parseColor("#D7D6D6"));
                }
            }
        });
        txt_prenom.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (txt_prenom.getText().toString().trim().equals("")){
                    txt_prenom.setBackgroundResource(R.drawable.border_danger);
                    txt_prenom.setHintTextColor(Color.parseColor("#F44336"));
                }else {
                    txt_prenom.setBackgroundResource(R.drawable.border_edit);
                    txt_prenom.setHintTextColor(Color.parseColor("#D7D6D6"));
                }
            }
        });*/

        /*txt_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String seq = String.valueOf(charSequence);
                if (!seq.equals("")){
                    txt_name.setBackgroundResource(R.drawable.border_edit);
                    txt_name.setHintTextColor(Color.parseColor("#D7D6D6"));
                }else {
                    txt_name.setBackgroundResource(R.drawable.border_danger);
                    txt_name.setHintTextColor(Color.parseColor("#F44336"));
                }
                if (!txt_name.getText().toString().trim().equals("") && !txt_prenom.getText().toString().trim().equals("")){
                    btn_register.setBackgroundColor(Color.parseColor("#00796b"));
                    btn_register.setEnabled(true);
                }else {
                    btn_register.setBackgroundColor(Color.parseColor("#E8F5E9"));
                    btn_register.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        txt_prenom.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String seq = String.valueOf(charSequence);
                if (!seq.equals("")){
                    txt_prenom.setBackgroundResource(R.drawable.border_edit);
                    txt_prenom.setHintTextColor(Color.parseColor("#D7D6D6"));
                }else {
                    txt_prenom.setBackgroundResource(R.drawable.border_danger);
                    txt_prenom.setHintTextColor(Color.parseColor("#F44336"));
                }
                if (!txt_name.getText().toString().trim().equals("") && !txt_prenom.getText().toString().trim().equals("")){
                    btn_register.setBackgroundColor(Color.parseColor("#00796b"));
                    btn_register.setEnabled(true);
                }else {
                    btn_register.setBackgroundColor(Color.parseColor("#E8F5E9"));
                    btn_register.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });*/


        /*======================================================*/



        have_a_compte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),Login.class));
                finish();
            }
        });
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                final String email = txt_email.getText().toString().trim();
                final String name = txt_name.getText().toString().trim();
                final String prenom = txt_prenom.getText().toString().trim();
                final String password = txt_password.getText().toString().trim();
                if (email.equals("")){
                    txt_email_error.setText(R.string.txt_email_empty);
                    txt_email_error.setVisibility(View.VISIBLE);
                }else if (!email.matches(emailPattern)){
                    txt_email_error.setText(R.string.txt_email_invalide);
                    txt_email_error.setVisibility(View.VISIBLE);
                }else {
                    txt_email_error.setText("");
                    txt_email_error.setVisibility(View.GONE);
                }

                if (name.equals("")){
                    txt_nom_error.setText(R.string.txt_nom_error);
                    txt_nom_error.setVisibility(View.VISIBLE);
                }else {
                    txt_nom_error.setText("");
                    txt_nom_error.setVisibility(View.GONE);
                }

                if (prenom.equals("")){
                    txt_prenom_error.setText(R.string.txt_prenom_error);
                    txt_prenom_error.setVisibility(View.VISIBLE);
                }else {
                    txt_prenom_error.setText("");
                    txt_prenom_error.setVisibility(View.GONE);
                }

                if (password.equals("")){
                    txt_password_error.setText(R.string.txt_password_empty);
                    txt_password_error.setVisibility(View.VISIBLE);
                }else if (password.length() < 8){
                    txt_password_error.setText(R.string.txt_password_short);
                    txt_password_error.setVisibility(View.VISIBLE);
                }else {
                    txt_password_error.setText("");
                    txt_password_error.setVisibility(View.GONE);
                }

                if (!email.equals("") && email.matches(emailPattern) && !name.equals("") && !prenom.equals("") && !password.equals("") && password.length() >= 8){
                    r_my_loader.setVisibility(View.VISIBLE);
                    txt_email_error.setText("");
                    txt_email_error.setVisibility(View.GONE);
                    txt_nom_error.setText("");
                    txt_nom_error.setVisibility(View.GONE);
                    txt_prenom_error.setText("");
                    txt_prenom_error.setVisibility(View.GONE);
                    txt_password_error.setText("");
                    txt_password_error.setVisibility(View.GONE);
                    txt_email.setEnabled(false);
                    txt_name.setEnabled(false);
                    txt_prenom.setEnabled(false);
                    txt_password.setEnabled(false);
                    btn_register.setEnabled(false);
                    mAuth.createUserWithEmailAndPassword(email,properties.getHastStatique()+password).addOnCompleteListener(Register.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()){
                                mAuth.signInWithEmailAndPassword(email,properties.getHastStatique()+password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (task.isSuccessful()){
                                            final Map<String, Object> userCl = new HashMap<>();
                                            userCl.put("nom",name);
                                            userCl.put("prenom",prenom);
                                            userCl.put("profil",properties.getDefaultProfil());
                                            userCl.put("statut","_ENABLED_");
                                            userCl.put("type","default");
                                            userCl.put("genre","Homme");
                                            CollectionReference DBUser = db.collection("Users");
                                            DBUser.document(email).set(userCl).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()){
                                                        Users userInf = new Users();
                                                        userInf.setNom(name);
                                                        userInf.setPrenom(prenom);
                                                        userInf.setEmail(email);
                                                        userInf.setProfil(properties.getDefaultProfil());
                                                        sessionManagerLogin.storeUserData(userInf);
                                                        startActivity(new Intent(getApplicationContext(),Home.class));
                                                        r_my_loader.setVisibility(View.GONE);
                                                        finish();
                                                    }else {
                                                        r_my_loader.setVisibility(View.GONE);
                                                        btn_register.setEnabled(true);
                                                        txt_email.setEnabled(true);
                                                        txt_password.setEnabled(true);
                                                        txt_alert_user.setEnabled(true);
                                                        txt_prenom.setEnabled(true);
                                                        //txt_forgot_password.setEnabled(true);
                                                        //txt_go_register.setEnabled(true);
                                                        r_alert_user.setVisibility(View.VISIBLE);
                                                        txt_alert_user.setText("Une erreur est survenue lors de la création du compte!");
                                                        new CountDownTimer(4000,1000){
                                                            @Override
                                                            public void onTick(long l) {

                                                            }

                                                            @Override
                                                            public void onFinish() {
                                                                r_alert_user.setVisibility(View.GONE);
                                                            }
                                                        }.start();
                                                    }
                                                }
                                            });
                                        }else {
                                            r_my_loader.setVisibility(View.GONE);
                                            btn_register.setEnabled(true);
                                            txt_email.setEnabled(true);
                                            txt_password.setEnabled(true);
                                            txt_alert_user.setEnabled(true);
                                            txt_prenom.setEnabled(true);
                                            //txt_forgot_password.setEnabled(true);
                                            //txt_go_register.setEnabled(true);
                                            r_alert_user.setVisibility(View.VISIBLE);
                                            txt_alert_user.setText("Une erreur est survenue lors de la création du compte!");
                                            new CountDownTimer(4000,1000){
                                                @Override
                                                public void onTick(long l) {

                                                }

                                                @Override
                                                public void onFinish() {
                                                    r_alert_user.setVisibility(View.GONE);
                                                }
                                            }.start();
                                        }
                                    }
                                });
                            }else {
                                r_my_loader.setVisibility(View.GONE);
                                btn_register.setEnabled(true);
                                txt_email.setEnabled(true);
                                txt_password.setEnabled(true);
                                txt_alert_user.setEnabled(true);
                                txt_prenom.setEnabled(true);
                                //txt_forgot_password.setEnabled(true);
                                //txt_go_register.setEnabled(true);
                                r_alert_user.setVisibility(View.VISIBLE);
                                txt_alert_user.setText("Une erreur est survenue lors de la création du compte!");
                                new CountDownTimer(4000,1000){
                                    @Override
                                    public void onTick(long l) {

                                    }

                                    @Override
                                    public void onFinish() {
                                        r_alert_user.setVisibility(View.GONE);
                                    }
                                }.start();
                            }
                        }
                    });
                }
            }
        });
        /*
        * Action image profil
        * */
        /*new CroperinoConfig("IMG_" + System.currentTimeMillis() + ".jpg", "/MikeLau/Pictures", "/sdcard/MikeLau/Pictures");
        CroperinoFileUtil.setupDirectory(Register.this);*/
    }
    /*======================================================*/
    //Mes méthodes overrides
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            if (sessionManagerRegister.isLoggedIn()){
                finish();
            }else {
                startActivity(new Intent(getApplicationContext(), Login.class));
                finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            if (sessionManagerRegister.isLoggedIn()){
                finish();
            }else {
                startActivity(new Intent(getApplicationContext(), Login.class));
                finish();
            }
        }
        return false;
    }
    /*=============================================================================*/
    private boolean uploadImg(final String tel){
        if (!SlectedPhoto.equals("")) {
            try {
                Bitmap bitmap = ImageLoader.init().from(SlectedPhoto).requestSize(512, 512).getBitmap();
                String encodedImage = ImageBase64.encode(bitmap);

                HashMap<String, String> postData = new HashMap<String, String>();
                postData.put("image", encodedImage);

                PostResponseAsyncTask task = new PostResponseAsyncTask(Register.this, postData, new AsyncResponse() {
                    @Override
                    public void processFinish(String s) {
                        new CountDownTimer(4000, 1000) {

                            public void onTick(long millisUntilFinished) {
                                //nothing
                            }

                            public void onFinish() {
                                //sessionManagerLogin.storeUserData(tel);
                                Intent intent = new Intent(getApplicationContext(),Home.class);
                                        //intent.putExtra("active","ok");
                                startActivity(intent);
                                finish();
                                sessionManagerRegister.clearRegisterData();
                            }
                        }.start();
                    }
                });
                task.execute(lien+"upload.php?tel="+tel);

                task.setEachExceptionsHandler(new EachExceptionsHandler() {
                    @Override
                    public void handleIOException(IOException e) {

                    }

                    @Override
                    public void handleMalformedURLException(MalformedURLException e) {
                        Toast.makeText(getApplicationContext(), "URL Error.",
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void handleProtocolException(ProtocolException e) {
                        Toast.makeText(getApplicationContext(), "Protocol Error.",
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void handleUnsupportedEncodingException(UnsupportedEncodingException e) {
                        Toast.makeText(getApplicationContext(), "Encoding Error.",
                                Toast.LENGTH_SHORT).show();
                    }
                });
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

        }
    }
    /*=======================================================================================*/

    //Fin méthodes overrides
    /*======================================================*/


    //Fin mes fonctions
    /*======================================================*/
    private void topAnimation(){
        animSideTop = AnimationUtils.loadAnimation(this,R.anim.push_top_in);
        animSideTop.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    private void fadeOutAnimation(){
        animSideFadeOut = AnimationUtils.loadAnimation(this,R.anim.disappear);
        animSideFadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        /*if (requestCode == CroperinoFileUtil.REQUEST_CAMERA) {
            for (int i = 0; i < permissions.length; i++) {
                String permission = permissions[i];
                int grantResult = grantResults[i];

                if (permission.equals(Manifest.permission.CAMERA)) {
                    if (grantResult == PackageManager.PERMISSION_GRANTED) {
                        prepareCamera();
                    }
                }
            }
        } else if (requestCode == CroperinoFileUtil.REQUEST_EXTERNAL_STORAGE) {
            boolean wasReadGranted = false;
            boolean wasWriteGranted = false;

            for (int i = 0; i < permissions.length; i++) {
                String permission = permissions[i];
                int grantResult = grantResults[i];

                if (permission.equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    if (grantResult == PackageManager.PERMISSION_GRANTED) {
                        wasReadGranted = true;
                    }
                }
                if (permission.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    if (grantResult == PackageManager.PERMISSION_GRANTED) {
                        wasWriteGranted = true;
                    }
                }
            }

            if (wasReadGranted && wasWriteGranted) {
                prepareChooser();
            }
        }else{
            switch (requestCode) {
                case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                    // If request is cancelled, the result arrays are empty.
                    if (grantResults.length > 0
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                    } else {

                    }
                    return;
                }

                // other 'case' lines to check for other
                // permissions this app might request
            }
        }*/
    }

}
