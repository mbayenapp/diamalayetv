package com.mbayennapp.diamalayetv.Activity;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.balysv.materialripple.MaterialRippleLayout;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.mbayennapp.diamalayetv.Adapter.Rubrique_Adapter;
import com.mbayennapp.diamalayetv.Model.Rubriques;
import com.mbayennapp.diamalayetv.R;
import com.mbayennapp.diamalayetv.Utils.ConnexionInternet;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Start_direct extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {
    private static final String TAG = Start_direct.class.getSimpleName();
    private String lien = "https://www.mabcompany.com/JL_API/";
    private String token = "layenne2017";
    public static final String API_KEY = "AIzaSyAUkyEG9duEFiVRqtWc3U8iHqNZDopIlQI";
    private String VIDEO_ID = null;
    private String id_video = "";
    private MaterialRippleLayout sect_start_direct;
    private JSONObject j_data = null;
    private RecyclerView rubrique;
    private Rubrique_Adapter adapter;
    private List<Rubriques> rubriques = new ArrayList<>();
    private Rubriques rub;
    private TextView txt_lieu_video,txt_titre_video,txt_title_rubrique_tv,
            txt_title_direct_tv;
    private ConnexionInternet connexionInternet;
    private String titre;
    private String lieu;
    private ShimmerFrameLayout shimmer_view;
    /*===========================================================*/

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private DocumentReference darticles;
    /*==========================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_direct);
        /*==========================================================*/
        connexionInternet = new ConnexionInternet();
        Bundle extras = getIntent().getExtras();
        id_video = extras.getString("idvideo");
        titre = extras.getString("titre");
        lieu = extras.getString("lieu");
        VIDEO_ID = id_video;
        /*Initialisation de youtube player*/
        YouTubePlayerView youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_player_video_ID);
        youTubePlayerView.initialize(API_KEY, this);
        /*==========================================================*/
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        rubrique = (RecyclerView)findViewById(R.id.rubrique);
        /*==========================================================*/
        txt_lieu_video = (TextView)findViewById(R.id.txt_lieu_video);
        txt_titre_video = (TextView)findViewById(R.id.txt_titre_video);
        txt_title_rubrique_tv = (TextView)findViewById(R.id.txt_title_rubrique_tv);
        txt_title_direct_tv = (TextView)findViewById(R.id.txt_title_direct_tv);
        shimmer_view = (ShimmerFrameLayout)findViewById(R.id.shimmer_view);
        /*==========================================================*/
        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeueLTStd_Roman.otf");
        Typeface typegold = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeuecondenced.otf");
        txt_lieu_video.setTypeface(typeFace);
        txt_titre_video.setTypeface(typeFace);
        txt_title_rubrique_tv.setTypeface(typegold);
        txt_title_direct_tv.setTypeface(typegold);
        /*==========================================================*/
        try{
            get_rubriques();
        }catch (Exception e){
            e.printStackTrace();
        }
      /*  if (connexionInternet.isConnectedInternet(this)) {
            rubriques = get_rubriques();
            adapter = new Rubrique_Adapter(Start_direct.this, rubriques);
            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
            rubrique.setLayoutManager(mLayoutManager);
            rubrique.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
            rubrique.setItemAnimator(new DefaultItemAnimator());
            rubrique.setAdapter(adapter);
        }*/
      txt_lieu_video.setText(lieu);
        txt_titre_video.setText(titre);
        /*==========================================================*/
    }

    private void get_rubriques(){
        //Query query = db.collection("Articles").orderBy("date", Query.Direction.DESCENDING);
        db.collection("Rubriques").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    Log.d(TAG, "rubriques: " + task.getResult().size());
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        if (document.getId() != null) {
                                rub = new Rubriques();
                                rub.setNom(document.get("nom").toString());
                                rub.setImage(document.get("image").toString());
                                rubriques.add(rub);
                        }
                    }
                    if (!rubriques.isEmpty()){
                        adapter = new Rubrique_Adapter(Start_direct.this, rubriques);
                        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
                        rubrique.setLayoutManager(mLayoutManager);
                        rubrique.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(6), true));
                        rubrique.setItemAnimator(new DefaultItemAnimator());
                        new CountDownTimer(1000,500){
                            @Override
                            public void onTick(long millisUntilFinished) {

                            }

                            @Override
                            public void onFinish() {
                                shimmer_view.stopShimmerAnimation();
                                shimmer_view.setVisibility(View.GONE);
                                rubrique.setAdapter(adapter);
                                rubrique.setVisibility(View.VISIBLE);
                            }
                        }.start();
                    }
                } else {
                    Log.d(TAG, "Error: " + task.getException());
                }
            }
        });
    }
    /*==========================================================*/
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(getApplicationContext(),Direct.class));
            finish();
        }
        return false;
    }
    /*==========================================================*/
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }


    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
    /*==========================================================*/
    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
        youTubePlayer.setPlayerStateChangeListener(playerStateChangeListener);
        youTubePlayer.setPlaybackEventListener(playbackEventListener);
        if (!wasRestored) {
            //player.cuePlaylist(VIDEO_ID);
            youTubePlayer.loadVideo(VIDEO_ID);
            //youTubePlayer.setFullscreen(true);
            youTubePlayer.play();

        }
    }
    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Toast.makeText(this, "Erreur d'initialisation de la vidéo!", Toast.LENGTH_LONG).show();
    }
    private YouTubePlayer.PlaybackEventListener playbackEventListener = new YouTubePlayer.PlaybackEventListener() {
        @Override
        public void onBuffering(boolean arg0) {
        }
        @Override
        public void onPaused() {
        }
        @Override
        public void onPlaying() {
        }
        @Override
        public void onSeekTo(int arg0) {
        }
        @Override
        public void onStopped() {
        }
    };
    private YouTubePlayer.PlayerStateChangeListener playerStateChangeListener = new YouTubePlayer.PlayerStateChangeListener() {
        @Override
        public void onAdStarted() {
        }
        @Override
        public void onError(YouTubePlayer.ErrorReason arg0) {
        }
        @Override
        public void onLoaded(String arg0) {
        }
        @Override
        public void onLoading() {
        }
        @Override
        public void onVideoEnded() {
        }
        @Override
        public void onVideoStarted() {
        }
    };
    /*==========================================================*/
    @Override
    protected void onResume() {
        super.onResume();
        //mRadioManager.connect();
        shimmer_view.startShimmerAnimation();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onPause() {
        shimmer_view.stopShimmerAnimation();
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        shimmer_view.stopShimmerAnimation();
    }
}
