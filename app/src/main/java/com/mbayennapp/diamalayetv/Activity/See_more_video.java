package com.mbayennapp.diamalayetv.Activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.mbayennapp.diamalayetv.Adapter.Video_Adapter;
import com.mbayennapp.diamalayetv.Model.Articles;
import com.mbayennapp.diamalayetv.R;
import com.mbayennapp.diamalayetv.Utils.ConnexionInternet;
import com.mbayennapp.diamalayetv.Utils.Verification_Date;

import java.util.ArrayList;
import java.util.List;

public class See_more_video extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {
    private static final String TAG = See_more_video.class.getSimpleName();
    private String lien = "https://www.mabcompany.com/JL_API/";
    private String token = "layenne2017";
    public static final String API_KEY = "AIzaSyAUkyEG9duEFiVRqtWc3U8iHqNZDopIlQI";
    String VIDEO_ID = null;
    private String adid;
    private String date_sent,titre,content,lieu,photo,id_video;
    private TextView txt_title_video,txt_date_video,
            txt_content_article,txt_title_a_see;
    private ListView lst_other;
    private List<Articles> articles = new ArrayList<>();
    private Articles article;
    private Video_Adapter adapter;
    private ImageView icon_arrow_down,icon_arrow_up;
    private ConnexionInternet connexionInternet;

    /*===============================================================*/
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    /*===============================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_more_video);
        /*==========================================================*/
        Bundle extras = getIntent().getExtras();
        adid = extras.getString("adid");
        date_sent = extras.getString("date_sent");
        titre = extras.getString("titre");
        content = extras.getString("content");
        //lieu = extras.getString("lieu");
        photo = extras.getString("photo");
       // source = extras.getString("source");
        id_video = extras.getString("id_video");
        VIDEO_ID = id_video;
        /*Initialisation de youtube player*/
        YouTubePlayerView youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_player_video_ID);
        youTubePlayerView.initialize(API_KEY, this);
        connexionInternet = new ConnexionInternet();
        /*==========================================================*/
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        /*==========================================================*/
        txt_title_video = (TextView)findViewById(R.id.txt_title_video);
        txt_date_video = (TextView)findViewById(R.id.txt_date_video);
        lst_other = (ListView)findViewById(R.id.lst_videos_similaires);
        icon_arrow_down = (ImageView)findViewById(R.id.icon_arrow_down);
        icon_arrow_up = (ImageView)findViewById(R.id.icon_arrow_up);
        txt_content_article = (TextView)findViewById(R.id.txt_content_article);
        txt_title_a_see = (TextView)findViewById(R.id.txt_title_a_see);
        /*==========================================================*/
        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeueLTStd_Roman.otf");
        Typeface typegold = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeuecondenced.otf");
        txt_title_video.setTypeface(typegold);
        txt_content_article.setTypeface(typeFace);
        txt_date_video.setTypeface(typeFace);
        txt_title_a_see.setTypeface(typegold);
        /*==========================================================*/
        if (connexionInternet.isConnectedInternet(this)) {
            Verification_Date vd = new Verification_Date();
            String dt = vd.get_date(date_sent);
            txt_date_video.setText(dt);
            txt_title_video.setText(titre);
            txt_content_article.setText(content);
            /*articles = get_videos(id);
            adapter = new Video_Adapter(getApplicationContext(), articles);
            lst_other.setAdapter(adapter);
            setListViewHeightBasedOnChildren(lst_other);*/
            get_other_videos_similaires(adid);
            /*==========================================================*/
            lst_other.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Articles article = articles.get(i);
                    Intent intent = new Intent(getApplicationContext(), See_more_video.class);
                    intent.putExtra("adid",article.getAdid());
                    intent.putExtra("date_sent",article.getDate_sent());
                    intent.putExtra("titre",article.getTitre());
                    intent.putExtra("content",article.getContent());
                    intent.putExtra("photo",article.getPhoto());
                    //intent.putExtra("source",video.getSource());
                    intent.putExtra("id_video",article.getId_video());
                    startActivity(intent);
                    finish();
                }
            });
            /*==========================================================*/
            icon_arrow_down.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    icon_arrow_down.setVisibility(View.GONE);
                    icon_arrow_up.setVisibility(View.VISIBLE);
                    txt_content_article.setVisibility(View.VISIBLE);
                }
            });
            icon_arrow_up.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    icon_arrow_down.setVisibility(View.VISIBLE);
                    icon_arrow_up.setVisibility(View.GONE);
                    txt_content_article.setVisibility(View.GONE);

                }
            });
        }
    }

    /*==========================================================*/
    private void get_other_videos_similaires(final String adid){
        db.collection("Articles").orderBy("date", Query.Direction.DESCENDING).whereEqualTo("type","video").limit(4).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    Log.d(TAG, "Nbr articles: " + task.getResult().size());
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        if (!document.getId().equals(adid)) {
                            if (document.get("type").toString().equals("video")) {
                                article = new Articles();
                                article.setAdid(document.getId());
                                article.setDate_sent(document.get("date").toString());
                                article.setTitre(document.get("titre").toString());
                                article.setContent(document.get("content").toString());
                                article.setLieu(document.get("lieu").toString());
                                article.setPhoto(document.get("photo").toString());
                                article.setSource(document.get("source").toString());
                                article.setId_video(document.get("id_video").toString());
                                articles.add(article);
                            }
                        }
                    }
                    if (!articles.isEmpty()){
                        adapter = new Video_Adapter(getApplicationContext(), articles);
                        lst_other.setAdapter(adapter);
                        setListViewHeightBasedOnChildren(lst_other);
                    }
                } else {
                    Log.d(TAG, "Error: " + task.getException());
                }
            }
        });
    }
    /*==========================================================*/
    //Mes méthodes overrides
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            //startActivity(new Intent(getApplicationContext(),Home.class));
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            //startActivity(new Intent(getApplicationContext(),Home.class));
            finish();
        }
        return false;
    }
    /*==========================================================*/
    /*==========================================================*/
    //Modifier la hauteur de mon listView
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
    /*==========================================================*/

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
        youTubePlayer.setPlayerStateChangeListener(playerStateChangeListener);
        youTubePlayer.setPlaybackEventListener(playbackEventListener);
        if (!wasRestored) {
            //player.cuePlaylist(VIDEO_ID);
            youTubePlayer.loadVideo(VIDEO_ID);
            //youTubePlayer.setFullscreen(true);
            youTubePlayer.play();
        }
    }
    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Toast.makeText(this, "Erreur d'initialisation de la vidéo!", Toast.LENGTH_LONG).show();
    }
    private YouTubePlayer.PlaybackEventListener playbackEventListener = new YouTubePlayer.PlaybackEventListener() {
        @Override
        public void onBuffering(boolean arg0) {
        }
        @Override
        public void onPaused() {
        }
        @Override
        public void onPlaying() {
        }
        @Override
        public void onSeekTo(int arg0) {
        }
        @Override
        public void onStopped() {
        }
    };
    private YouTubePlayer.PlayerStateChangeListener playerStateChangeListener = new YouTubePlayer.PlayerStateChangeListener() {
        @Override
        public void onAdStarted() {
        }
        @Override
        public void onError(YouTubePlayer.ErrorReason arg0) {
        }
        @Override
        public void onLoaded(String arg0) {
        }
        @Override
        public void onLoading() {
        }
        @Override
        public void onVideoEnded() {
        }
        @Override
        public void onVideoStarted() {
        }
    };
}
