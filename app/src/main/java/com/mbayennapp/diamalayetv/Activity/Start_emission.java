package com.mbayennapp.diamalayetv.Activity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.mbayennapp.diamalayetv.Model.Emissions;
import com.mbayennapp.diamalayetv.R;
import com.mbayennapp.diamalayetv.Utils.ConnexionInternet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Start_emission extends AppCompatActivity{
    // implements MediaListener
    private static final String TAG = Start_emission.class.getSimpleName();
    private String lien = "https://www.mabcompany.com/JL_API/";
    private String token = "layenne2017";
    private ConnexionInternet connexionInternet;
    private TextView txt_title_emission,txt_title_animateur,txt_animateur,
            txt_time_inc;
    private String titre = "",animateur = "",duree = "",fichier = "", URL_MEDIA = "";
    private SeekBar seekbar;
    private TextView txt_time_dec;
    private Handler handler;
    private Runnable runnable;
    private int statutBar = 1;
    private int p1 = 0, p2 = 0, p3 = 0,p4 = 0;
    private int dmillis = 0;
    private int id = 0;
    private List<Emissions> emissions;
    private int init_next = 0,init_previous = 0;
    private ImageView go_next_tape;
    //MediaManager mediaManager = MediaManager.with(this);
    private final String RADIO_URL = "https://api.soundcloud.com/tracks/230497727/stream?client_id=06a2d17b03d3ff6ae226b007edd5595d";
    /*==============================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_emission);
        /*==============================================================*/
        handler = new Handler();
        Bundle extras = getIntent().getExtras();
        id = extras.getInt("id");
        titre = extras.getString("titre");
        animateur = extras.getString("animateur");
        duree = extras.getString("duree");
        fichier = extras.getString("fichier");
        URL_MEDIA = lien+"audio/"+fichier;
        /*==============================================================*/
        txt_title_emission = (TextView)findViewById(R.id.txt_title_emission);
        txt_title_animateur = (TextView)findViewById(R.id.txt_title_animateur);
        txt_animateur = (TextView)findViewById(R.id.txt_animateur);
        txt_time_inc = (TextView)findViewById(R.id.txt_time_inc);
        txt_time_dec = (TextView) findViewById(R.id.txt_time_dec);
        seekbar = (SeekBar)findViewById(R.id.seekbar);
        go_next_tape = (ImageView)findViewById(R.id.go_next_tape);
        /*==============================================================*/
        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/century-gothic.ttf");
        Typeface typegold = Typeface.createFromAsset(getAssets(), "fonts/century-gothic-gold.ttf");
        txt_title_emission.setTypeface(typegold);
        txt_title_animateur.setTypeface(typegold);
        txt_animateur.setTypeface(typegold);
        txt_time_inc.setTypeface(typeFace);
        //txt_time_dec.setTypeface(typeFace);
        /*==============================================================*/
        txt_title_emission.setSelected(true);
        txt_title_emission.setSingleLine(true);
        seekbar.setPadding(2,0,0,0);
        /*==============================================================*/
        txt_title_emission.setText(titre);
        txt_animateur.setText(animateur);
        //txt_time_dec.setText(duree);
        String[] pat = duree.split(":");
        p1 = Integer.parseInt(pat[0]);
        p2 = Integer.parseInt(pat[1]);
        if (p1 > 0){
            dmillis = p1 * 60 + p2;
        }else {
            dmillis = p2;
        }
        txt_time_dec.setText(String.valueOf(p1)+":"+String.valueOf(p2));
        CountDowbStart(dmillis);
        /*=============================================================*/
        emissions = get_other_emissions(id);
        /*=============================================================*/
        go_next_tape.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Emissions emission = emissions.get(init_next);
                txt_title_emission.setText(emission.getTitre());
                txt_animateur.setText(emission.getEnimateur());
                String[] pat = emission.getDuree().split(":");
                p1 = Integer.parseInt(pat[0]);
                p2 = Integer.parseInt(pat[1]);
                if (p1 > 0){
                    dmillis = p1 * 60 + p2;
                }else {
                    dmillis = p2;
                }
                txt_time_dec.setText(String.valueOf(p1)+":"+String.valueOf(p2));
                CountDowbStart(dmillis);
                init_next += 1;
            }
        });
    }
    /*==============================================================*/

    /*==============================================================*/
    public void CountDowbStart(int millis){
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (p2 > 0){
                    statutBar += 100/dmillis;
                    p2 -= 1;
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (p2 > 0){
                                p4 += 1;
                                if (p3 < 10) {
                                    txt_time_inc.setText("0" + String.valueOf(p3) + ":" + String.valueOf(p4));
                                } else if (p4 < 10) {
                                    txt_time_inc.setText(String.valueOf(p3) + ":" + "0"+String.valueOf(p4));
                                } else {
                                    txt_time_inc.setText(String.valueOf(p3) + ":" + String.valueOf(p4));
                                }
                            }
                            if (p2 == 0){
                                if (p1 > 0){
                                    p2 = 59;
                                    p1 -= 1;
                                }else {
                                    txt_time_dec.setText("00:00");
                                    txt_time_inc.setText(duree);
                                }
                            }
                            if (p1 < 10){
                                txt_time_dec.setText("0"+String.valueOf(p1)+":"+String.valueOf(p2));
                            }else if (p2 < 10){
                                txt_time_dec.setText(String.valueOf(p1)+":"+"0"+String.valueOf(p2));
                            }else {
                                txt_time_dec.setText(String.valueOf(p1) + ":" + String.valueOf(p2));
                            }
                            if (p4 == 59) {
                                p4 = 0;
                                p3 += 1;
                                if (p3 < 10) {
                                    txt_time_inc.setText("0" + String.valueOf(p3) + ":" + String.valueOf(p4));
                                } else if (p4 < 10) {
                                    txt_time_inc.setText(String.valueOf(p3) + ":" + "0"+String.valueOf(p4));
                                } else {
                                    txt_time_inc.setText(String.valueOf(p3) + ":" + String.valueOf(p4));
                                }
                            }
                        }
                    });
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //mediaManager.seekTo(seekBar.getProgress());
            }
        });

       /* mediaManager.registerListener(this);
        mediaManager.play(RADIO_URL);*/
    }
    /*==============================================================*/
    @Override
    protected void onResume() {
        super.onResume();
        //mediaManager.connect();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
       // mediaManager.disconnect();
    }

    /*@Override
    public void onMediaLoading() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                seekbar.setEnabled(false);
                //textView.setText("LOADING");
            }
        });
    }

    @Override
    public void onMediaStarted(final int totalDuration, int currentDuration) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                seekbar.setEnabled(true);
                seekbar.setMax(totalDuration / 1000);
                //textView.setText("STARTED");
            }
        });
    }

    @Override
    public void onMediaStopped() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //textView.setText("STOPPED");
            }
        });
    }*/
    /*==============================================================*/
    //La méthode qui retourne les autres emissions
    private List<Emissions> get_other_emissions(int id){
        List<Emissions> emissions = new ArrayList<>();
        Emissions emission;
        String result = "";
        String code = "";
        OkHttpClient client = new OkHttpClient();
        //
        RequestBody body = new FormBody.Builder()
                .add("token",token)
                .add("id", String.valueOf(id))
                .build();

        final Request request = new Request.Builder()
                .url(lien+"Emissions/get_other_emission.php")
                .addHeader("Content-Type","text/json; Charset=UTF-8")
                .post(body)
                .build();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        try {
            Response response = client.newCall(request).execute();
            if (response.code() == 200) {
                result = response.body().string();
                if (result != null) {
                    Log.d(TAG, "autres vidéos:" + result);
                    if (result != null) {
                        JSONArray jsonArray = new JSONArray(result);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            emission = new Emissions();
                            emission.setId(jsonObject.optInt("id"));
                            emission.setPhoto(jsonObject.optString("photo"));
                            emission.setTitre(jsonObject.optString("titre"));
                            emission.setDate_ajout(jsonObject.optString("date_ajout"));
                            emission.setDate_emi(jsonObject.optString("date_emi"));
                            emission.setDuree(jsonObject.optString("duree"));
                            emission.setDescription(jsonObject.optString("description"));
                            emission.setEnimateur(jsonObject.optString("animateur"));
                            emission.setFichier(jsonObject.getString("fichier"));
                            emissions.add(emission);
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return emissions;
    }
    /*==============================================================*/
}
