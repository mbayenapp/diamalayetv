package com.mbayennapp.diamalayetv.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareButton;
import com.facebook.share.widget.ShareDialog;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mbayennapp.diamalayetv.Fragments.Issa;
import com.mbayennapp.diamalayetv.Fragments.Khalif;
import com.mbayennapp.diamalayetv.Fragments.Parametre;
import com.mbayennapp.diamalayetv.Fragments.Sermons;
import com.mbayennapp.diamalayetv.Fragments.Site;
import com.mbayennapp.diamalayetv.Fragments.Terms;
import com.mbayennapp.diamalayetv.Fragments.Videotheque;
import com.mbayennapp.diamalayetv.Fragments.home_fragment;
import com.mbayennapp.diamalayetv.Helpers.SessionManagerLogin;
import com.mbayennapp.diamalayetv.Helpers.SessionManagerRadio;
import com.mbayennapp.diamalayetv.Model.Users;
import com.mbayennapp.diamalayetv.R;
import com.taishi.library.Indicator;

import co.mobiwise.library.RadioListener;
import co.mobiwise.library.RadioManager;
import de.hdodenhof.circleimageview.CircleImageView;

public class Home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, RadioListener {
    private final String[] RADIO_URL = {"http://radios.xalimasn.com:9016/listen.pls"};
    //private final String[] RADIO_URL = {"http://173.236.30.50:9010/;"};
    //private final String[] RADIO_URL = {"http://rockfm.rockfm.com.tr:9450"};
    private String title ="";
    private Fragment fragment = null;
    private static final String TAG = Home.class.getSimpleName();
    private Users user;
    private SessionManagerLogin sessionManagerLogin;
    private CircleImageView img_profil;
    private TextView txt_nom,txt_email,txt_title_sect_confirme_manage,
            txt_text_sect_confirm;
    private View navHeader;
    private LinearLayout l_user_off_line,l_user_on_line,l_frame;
    private String link;
    private ActionBarDrawerToggle toggle;
    private DrawerLayout drawer;
    private RelativeLayout r_loader_login;
    private int start_fragment = 0,start_logout = 0;
    private MaterialRippleLayout l_sect_login_user,l_sect_logout_user;
    private RelativeLayout r_confirme_manage_profil,r_sect_radio;
    private Animation animSideTop,animSideFadeOut;
    private Button btn_no_confirm,btn_deconnect;
    private FrameLayout container_body;
    private FloatingActionButton fab;
    private ImageView start_radio;
    private ProgressBar p_start_radio;
    private TextView txt_title_emission,txt_animateur_emission,
            txt_title_radio,txt_dev_radio,txt_act_on,txt_act_two,txt_act_three;
   // private RadioManager mRadioManager;
    private Bitmap imgStart,imgStop;
    private SessionManagerRadio sessionManagerRadio;
    private Indicator indicator_radio;
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private DocumentReference dbuser;
    private RelativeLayout r_preloader;
    private ShimmerFrameLayout shimmer_view;
    private ShareButton fb_share_button;
    private CallbackManager callbackManager;
    private ShareDialog shareDialog;

    /*===============================================================================*/
    private RadioManager mRadioManager;
    /*===============================================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        /*========================================================================*/
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        /*========================================================================*/
        mRadioManager = RadioManager.with(this);
        try{
            mRadioManager.registerListener(this);
            mRadioManager.enableNotification(true);
        }catch (Exception e){
            e.printStackTrace();
        }
        /*========================================================================*/
        topAnimation();
        fadeOutAnimation();
        sessionManagerLogin = new SessionManagerLogin(this);
        sessionManagerRadio = new SessionManagerRadio(this);
        navHeader = navigationView.getHeaderView(0);
        img_profil = (CircleImageView)navHeader.findViewById(R.id.img_profil_home);
        txt_nom = (TextView)navHeader.findViewById(R.id.txt_name_profil_home);
        txt_email = (TextView)navHeader.findViewById(R.id.txt_email_profil_home);
        l_user_off_line = (LinearLayout)navHeader.findViewById(R.id.l_user_off_line);
        l_user_on_line = (LinearLayout)navHeader.findViewById(R.id.l_user_on_line);
        txt_act_on = (TextView)navHeader.findViewById(R.id.txt_act_on);
        txt_act_two = (TextView)navHeader.findViewById(R.id.txt_act_two);
        txt_act_three = (TextView)navHeader.findViewById(R.id.txt_act_three);
        fb_share_button = (ShareButton)navHeader.findViewById(R.id.fb_share_button);
        r_loader_login = (RelativeLayout)findViewById(R.id.r_loader_login);
        l_sect_login_user = (MaterialRippleLayout) navHeader.findViewById(R.id.l_sect_login_user);
        l_sect_logout_user = (MaterialRippleLayout) navHeader.findViewById(R.id.l_sect_logout_user);
        r_confirme_manage_profil = (RelativeLayout)findViewById(R.id.r_confirme_manage_profil);
        r_sect_radio = (RelativeLayout)findViewById(R.id.r_sect_radio);
        btn_no_confirm = (Button)findViewById(R.id.btn_no_confirm);
        btn_deconnect = (Button)findViewById(R.id.btn_deconnect);
        container_body = (FrameLayout)findViewById(R.id.container_body);
        txt_title_sect_confirme_manage = (TextView)findViewById(R.id.txt_title_sect_confirme_manage);
        txt_text_sect_confirm = (TextView)findViewById(R.id.txt_text_sect_confirm);
        l_frame = (LinearLayout)findViewById(R.id.l_frame);
        start_radio = (ImageView)findViewById(R.id.start_radio);
        p_start_radio = (ProgressBar)findViewById(R.id.p_start_radio);
        txt_title_emission = (TextView)findViewById(R.id.txt_title_emission);
        txt_animateur_emission = (TextView)findViewById(R.id.txt_animateur_emission);
        txt_title_radio = (TextView)findViewById(R.id.txt_title_radio);
        txt_dev_radio = (TextView)findViewById(R.id.txt_dev_radio);
        indicator_radio = (Indicator)findViewById(R.id.indicator_radio);
        r_preloader = (RelativeLayout)findViewById(R.id.r_preloader);
        shimmer_view = (ShimmerFrameLayout)findViewById(R.id.shimmer_view);
        /*========================================================================*/
       /* mRadioManager = RadioManager.with(getApplicationContext());
        mRadioManager.registerListener(this);
        mRadioManager.setLogging(true);*/
        /*========================================================================*/
        if (sessionManagerRadio.isLoggedIn()) {
            imgStop = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_pause_radio);
            start_radio.setImageBitmap(imgStop);
            indicator_radio.setVisibility(View.VISIBLE);
        }else {
            imgStop = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_play);
            start_radio.setImageBitmap(imgStop);
            indicator_radio.setVisibility(View.GONE);
        }
        /*========================================================================*/
        initializeUI();
        new CountDownTimer(3000,1000){
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                shimmer_view.stopShimmerAnimation();
                r_preloader.setVisibility(View.GONE);
                r_sect_radio.setVisibility(View.VISIBLE);
            }
        }.start();
        /*========================================================================*/
        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeueLTStd_Roman.otf");
        Typeface typegold = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeuecondenced.otf");
        txt_title_sect_confirme_manage.setTypeface(typegold);
        txt_text_sect_confirm.setTypeface(typeFace);
        txt_title_emission.setTypeface(typegold);
        txt_animateur_emission.setTypeface(typeFace);
        txt_title_radio.setTypeface(typegold);
        txt_dev_radio.setTypeface(typeFace);
        txt_act_on.setTypeface(typegold);
        txt_act_two.setTypeface(typegold);
        txt_act_three.setTypeface(typegold);
        btn_no_confirm.setTypeface(typegold);
        btn_deconnect.setTypeface(typegold);
        fb_share_button.setTypeface(typeFace);
        /*========================================================================*/
        txt_title_emission.setSelected(true);
        txt_title_emission.setSingleLine(true);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*r_loader_login.setVisibility(View.VISIBLE);
                fab.setVisibility(View.GONE);
                r_sect_radio.setVisibility(View.GONE);
                r_sect_radio.startAnimation(animSideFadeOut);
                new CountDownTimer(1500,500){
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {

                    }
                }.start();*/
                startActivity(new Intent(getApplicationContext(),Direct.class));
                finish();
            }
        });
        //Mon fragment par défaut
        title = getString(R.string.title_home);
        fragment = new home_fragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body, fragment);
        fragmentTransaction.commit();
        getSupportActionBar().setTitle(title);
        /*========================================================================*/
        if (mAuth.getCurrentUser() != null){
            /*dbuser = db.collection("Users").document(mAuth.getCurrentUser().getUid());
            dbuser.get().addOnCompleteListener(Home.this, new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()){
                        DocumentSnapshot Resuser = task.getResult();
                        Log.d(TAG,"Resultat: "+Resuser.get("prenom"));
                        user = new Users();
                        user.setNom(Resuser.get("nom").toString());
                        user.setPrenom(Resuser.get("prenom").toString());
                        user.setAdresse(Resuser.get("adresse").toString());
                        user.setIndicatif(Resuser.get("indicatif").toString());
                        user.setTel(Resuser.get("tel").toString());
                        user.setCode_postal(Resuser.get("codepostal").toString());
                        user.setVille(Resuser.get("ville").toString());
                        user.setPays(Resuser.get("pays").toString());
                        user.setProfil(Resuser.get("profil").toString());
                        user.setEmail(mAuth.getCurrentUser().getEmail());
                        user.setTel(Resuser.get("tel").toString());
                        user.setVille(Resuser.get("ville").toString());
                        user.setPays(Resuser.get("pays").toString());
                        *//*===============================================================*//*

                    }
                }
            });*/
            if (sessionManagerLogin.isLoggedIn()){
                l_sect_logout_user.setVisibility(View.VISIBLE);
                l_sect_login_user.setVisibility(View.GONE);
                l_user_on_line.setVisibility(View.VISIBLE);
                l_user_off_line.setVisibility(View.GONE);
                Glide.with(getApplicationContext()).load(sessionManagerLogin.getLoggedInUser().getProfil()).centerCrop().crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(img_profil);
                txt_nom.setText(sessionManagerLogin.getLoggedInUser().getPrenom() + " " + sessionManagerLogin.getLoggedInUser().getNom());
                txt_email.setText(sessionManagerLogin.getLoggedInUser().getEmail());
            }
        }else{
            l_user_on_line.setVisibility(View.GONE);
            l_user_off_line.setVisibility(View.VISIBLE);
            l_sect_logout_user.setVisibility(View.GONE);
            l_sect_login_user.setVisibility(View.VISIBLE);
        }
        /*=========================================================================*/
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
        fb_share_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.closeDrawers();
            }
        });

        //@ TODO: 28/05/2018
        // Link à modifier
        // */
        final ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse("https://developers.facebook.com"))
                .build();
        new CountDownTimer(500,100){
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                fb_share_button.setShareContent(content);
            }
        }.start();
        /*=========================================================================*/
        l_user_off_line.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawers();
            }
        });
        l_user_on_line.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent  intent = new Intent(getApplicationContext(),Accompte.class);
                intent.putExtra("id",sessionManagerLogin.getLoggedInUser().getId());
                intent.putExtra("nom",sessionManagerLogin.getLoggedInUser().getNom());
                intent.putExtra("prenom",sessionManagerLogin.getLoggedInUser().getPrenom());
                intent.putExtra("photo",sessionManagerLogin.getLoggedInUser().getProfil());
                intent.putExtra("adresse",sessionManagerLogin.getLoggedInUser().getAdresse());
                intent.putExtra("email",sessionManagerLogin.getLoggedInUser().getEmail());
                intent.putExtra("tel",sessionManagerLogin.getLoggedInUser().getTel());
                intent.putExtra("ville",sessionManagerLogin.getLoggedInUser().getVille());
                intent.putExtra("pays",sessionManagerLogin.getLoggedInUser().getPays());
                intent.putExtra("sexe",sessionManagerLogin.getLoggedInUser().getSexe());
                intent.putExtra("indicatif",sessionManagerLogin.getLoggedInUser().getIndicatif());
                intent.putExtra("activity","home");
                startActivity(intent);
                finish();
            }
        });
        l_sect_login_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),Login.class));
                //overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);
                finish();
            }
        });
        l_sect_logout_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fab.setVisibility(View.GONE);
                r_sect_radio.startAnimation(animSideFadeOut);
                fab.startAnimation(animSideFadeOut);
                drawer.closeDrawers();
                start_logout = 1;
                l_frame.setVisibility(View.GONE);
                container_body.setEnabled(false);
                r_sect_radio.setVisibility(View.GONE);
                new CountDownTimer(600,200){
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        r_confirme_manage_profil.setVisibility(View.VISIBLE);
                    }
                }.start();
                //r_confirme_manage_profil.startAnimation(animSideTop);

            }
        });
        btn_no_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                container_body.setEnabled(true);
                l_frame.setVisibility(View.VISIBLE);
                r_sect_radio.setVisibility(View.VISIBLE);
                fab.setVisibility(View.VISIBLE);
                r_sect_radio.startAnimation(animSideTop);
                fab.startAnimation(animSideTop);
                r_confirme_manage_profil.setVisibility(View.GONE);
                r_confirme_manage_profil.startAnimation(animSideFadeOut);
            }
        });
        btn_deconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sessionManagerLogin.isLoggedIn()){
                    r_confirme_manage_profil.setVisibility(View.GONE);
                    //r_confirme_manage_profil.startAnimation(animSideFadeOut);
                    r_loader_login.setVisibility(View.VISIBLE);
                    sessionManagerLogin.clearUserLoginData();
                    toolbar.setVisibility(View.GONE);
                    if (mAuth.getCurrentUser() != null){
                        mAuth.signOut();
                    }
                    new CountDownTimer(1500,500){
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {
                            startActivity(new Intent(getApplicationContext(),Home.class));
                            finish();
                        }
                    }.start();
                }
            }
        });
        /*========================================================================*/
        /*new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/{1230214043725144}/friends",
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        Log.d(TAG,"Liste des amis: "+response.toString());
                    }
                }
        ).executeAsync();*/
        /*========================================================================*/
    }

    /*============================================================================*/
    public void initializeUI() {

        start_radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mRadioManager.isPlaying()){
                    try{
                        p_start_radio.setVisibility(View.VISIBLE);
                        mRadioManager.startRadio(RADIO_URL[0]);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }else {
                    mRadioManager.stopRadio();
                    imgStop = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_play);
                    start_radio.setImageBitmap(imgStop);
                }
            }
        });
    }

    /*============================================================================*/
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            if (start_fragment == 1){
                start_fragment = 0;
                fragment = new home_fragment();
                title = getString(R.string.title_home);
                fab.setVisibility(View.VISIBLE);

                if (fragment != null) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.container_body, fragment);
                    fragmentTransaction.commit();

                    // set the toolbar title
                    getSupportActionBar().setTitle(title);
                }
            }else {
                if (start_logout == 1){
                    start_logout = 0;
                    container_body.setEnabled(true);
                    l_frame.setVisibility(View.VISIBLE);
                    r_sect_radio.setVisibility(View.VISIBLE);
                    fab.setVisibility(View.VISIBLE);
                    r_sect_radio.startAnimation(animSideTop);
                    fab.startAnimation(animSideTop);
                    r_confirme_manage_profil.setVisibility(View.GONE);
                    r_confirme_manage_profil.startAnimation(animSideFadeOut);
                }else {
                    finish();
                }
            }
        }
        return false;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        if (id == R.id.nav_home) {
            fragment = new home_fragment();
            title = getString(R.string.title_home);
            fab.setVisibility(View.VISIBLE);
        } else if (id == R.id.nav_video) {
            fragment = new Videotheque();
            title = getString(R.string.txt_video);
            start_fragment = 1;
            fab.setVisibility(View.GONE);
        } else if (id == R.id.nav_tv) {
            startActivity(new Intent(getApplicationContext(),Direct.class));
            finish();
        } //else if (id == R.id.nav_radio) {
            /*fragment = new Emission();
            title = getString(R.string.txt_title_emission);
            start_fragment = 1;*/
        /*}*/ else if (id == R.id.nav_setting) {
            fragment = new Parametre();
            title = getString(R.string.txt_setting);
            fab.setVisibility(View.GONE);
            start_fragment = 1;
        } else if (id == R.id.nav_contect) {
            startActivity(new Intent(getApplicationContext(),Contact.class));
            finish();
        }else if (id == R.id.nav_terms) {
            fragment = new Terms();
            title = getString(R.string.txt_terms_and_condition);
            start_fragment = 1;
            fab.setVisibility(View.GONE);
        }else if (id == R.id.nav_confrerie){
            startActivity(new Intent(getApplicationContext(),Confrerie.class));
            finish();
        }else if (id == R.id.nav_Issa){
            start_fragment = 1;
            fragment = new Issa();
            title = getString(R.string.txt_issa_fragmen);
            fab.setVisibility(View.GONE);
        }else if (id == R.id.nav_khalifs){
            start_fragment = 1;
            fragment = new Khalif();
            title = getString(R.string.txt_khalif_mahdi);
            fab.setVisibility(View.GONE);
        }else if (id == R.id.nav_sermon){
            start_fragment = 1;
            fragment = new Sermons();
            title = getString(R.string.txt_title_sermon);
            fab.setVisibility(View.GONE);
        }else if(id == R.id.nav_sites){
            start_fragment = 1;
            fragment = new Site();
            title = getString(R.string.txt_site_priere);
            fab.setVisibility(View.GONE);
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();

            // set the toolbar title
            getSupportActionBar().setTitle(title);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {

            fragment = new home_fragment();
            title = getString(R.string.title_home);
            fab.setVisibility(View.VISIBLE);
        }
        return super.onOptionsItemSelected(item);
    }

    /*============================================================================*/
    /*======================================================================*/
    private void topAnimation(){
        animSideTop = AnimationUtils.loadAnimation(this,R.anim.incoming);
        animSideTop.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    private void fadeOutAnimation(){
        animSideFadeOut = AnimationUtils.loadAnimation(this,R.anim.disappear);
        animSideFadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        //mRadioManager.connect();
        shimmer_view.startShimmerAnimation();
    }

    @Override
    protected void onStart() {
        super.onStart();
        try{
            mRadioManager.connect();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        /*try {
            if (mRadioManager.isPlaying()) {
                mRadioManager.disconnect();
            }
        }catch (Exception e){
            e.printStackTrace();
        }*/
    }
    /*@Override
    public void onRadioLoading() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //TODO Do UI works here.
                //txt.setText("RADIO STATE : LOADING...");
                p_start_radio.setVisibility(View.VISIBLE);
            }
        });
    }*/

    @Override
    public void onPause() {
       shimmer_view.stopShimmerAnimation();
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        shimmer_view.stopShimmerAnimation();
    }

    @Override
    public void onRadioConnected() {

    }

    @Override
    public void onRadioStarted() {
        imgStop = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_pause_radio);
        start_radio.setImageBitmap(imgStop);
        p_start_radio.setVisibility(View.GONE);
    }

    @Override
    public void onRadioStopped() {

    }

    @Override
    public void onMetaDataReceived(String s, String s1) {

    }
    /*@Override
    public void onRadioConnected() {

    }

    @Override
    public void onRadioStarted() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //TODO Do UI works here.
               // mTextViewControl.setText("RADIO STATE : PLAYING...");
                p_start_radio.setVisibility(View.GONE);
                imgStop = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_pause_radio);
                start_radio.setImageBitmap(imgStop);
                sessionManagerRadio.storeStatutRadio("on");
                indicator_radio.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onRadioStopped() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //TODO Do UI works here
                //mTextViewControl.setText("RADIO STATE : STOPPED.");
                imgStop = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_play);
                start_radio.setImageBitmap(imgStop);
                sessionManagerRadio.clearStatutRadion();
                indicator_radio.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onMetaDataReceived(String s, String s1) {
        //TODO Check metadata values. Singer name, song name or whatever you have.
    }

    @Override
    public void onError() {

    }*/
}
