package com.mbayennapp.diamalayetv.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mbayennapp.diamalayetv.R;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Contact extends AppCompatActivity {
    private static final String TAG = Contact.class.getSimpleName();
    private String lien = "https://www.mabcompany.com/JL_API/";
    private String token = "layenne2017";
    private Toolbar toolbar;
    private TextView txt_my_txt_register;
    private EditText txt_name,txt_email,txt_message;
    private Button btn_send_email;
    private LinearLayout l_sect_alert_email;
    private TextView txt_alert_email_invalide,txt_alert_user;
    private RelativeLayout r_my_loader,r_alert_user;
    private String result = "";
    /*==================================================================*/
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private DocumentReference dbuser;
    /*==================================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        /*==================================================================*/
        toolbar = (Toolbar)findViewById(R.id.toolbar_contact);
        toolbar.setTitle("Nous contacter");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        /*==================================================================*/
        txt_my_txt_register = (TextView) findViewById(R.id.txt_my_txt_register);
        txt_name = (EditText)findViewById(R.id.txt_name);
        txt_email = (EditText)findViewById(R.id.txt_email);
        txt_message = (EditText)findViewById(R.id.txt_message);
        btn_send_email = (Button)findViewById(R.id.btn_send_email);
        l_sect_alert_email = (LinearLayout)findViewById(R.id.l_sect_alert_email);
        txt_alert_email_invalide = (TextView)findViewById(R.id.txt_alert_email_invalide);
        r_my_loader = (RelativeLayout)findViewById(R.id.r_my_loader);
        r_alert_user = (RelativeLayout)findViewById(R.id.r_alert_user);
        txt_alert_user = (TextView)findViewById(R.id.txt_alert_user);
        /*==================================================================*/
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        /*==================================================================*/
        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeueLTStd_Roman.otf");
        Typeface typegold = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeuecondenced.otf");
        txt_my_txt_register.setTypeface(typeFace);
        txt_email.setTypeface(typeFace);
        txt_name.setTypeface(typeFace);
        txt_message.setTypeface(typeFace);
        btn_send_email.setTypeface(typegold);
        txt_alert_email_invalide.setTypeface(typeFace);
        txt_alert_user.setTypeface(typeFace);
        /*==================================================================*/
        btn_send_email.setEnabled(false);
        btn_send_email.setBackgroundColor(Color.parseColor("#E8F5E9"));
        final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        /*==================================================================*/
        txt_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String cheq = String.valueOf(charSequence);
                if (!cheq.equals("")) {
                    if (!txt_name.getText().toString().equals("") && !txt_email.getText().toString().equals("") &&
                            !txt_message.getText().toString().equals("")) {
                        btn_send_email.setBackgroundColor(Color.parseColor("#097233"));
                        btn_send_email.setEnabled(true);
                        txt_name.setBackgroundResource(R.drawable.border_edit);
                        txt_name.setHintTextColor(Color.parseColor("#D7D6D6"));
                    }else {
                        btn_send_email.setBackgroundColor(Color.parseColor("#097233"));
                        btn_send_email.setEnabled(false);
                        txt_name.setBackgroundResource(R.drawable.border_danger);
                        txt_name.setHintTextColor(Color.parseColor("#F44336"));
                    }
                }else {
                    btn_send_email.setBackgroundColor(Color.parseColor("#E8F5E9"));
                    btn_send_email.setEnabled(false);
                    txt_name.setBackgroundResource(R.drawable.border_danger);
                    txt_name.setHintTextColor(Color.parseColor("#F44336"));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        txt_email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String cheq = String.valueOf(charSequence);
                if (!cheq.equals("")) {
                    if (!txt_name.getText().toString().equals("") && !txt_email.getText().toString().equals("") &&
                            !txt_message.getText().toString().equals("")) {
                        btn_send_email.setBackgroundColor(Color.parseColor("#097233"));
                        btn_send_email.setEnabled(true);
                        txt_email.setBackgroundResource(R.drawable.border_edit);
                        txt_email.setHintTextColor(Color.parseColor("#D7D6D6"));
                    }else {
                        btn_send_email.setBackgroundColor(Color.parseColor("#097233"));
                        btn_send_email.setEnabled(false);
                        txt_email.setBackgroundResource(R.drawable.border_danger);
                        txt_email.setHintTextColor(Color.parseColor("#F44336"));
                    }
                }else {
                    btn_send_email.setBackgroundColor(Color.parseColor("#E8F5E9"));
                    btn_send_email.setEnabled(false);
                    txt_email.setBackgroundResource(R.drawable.border_danger);
                    txt_email.setHintTextColor(Color.parseColor("#F44336"));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        txt_message.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String cheq = String.valueOf(charSequence);
                if (!cheq.equals("")) {
                    if (!txt_name.getText().toString().equals("") && !txt_email.getText().toString().equals("") &&
                            !txt_message.getText().toString().equals("")) {
                        btn_send_email.setBackgroundColor(Color.parseColor("#097233"));
                        btn_send_email.setEnabled(true);
                        txt_message.setBackgroundResource(R.drawable.border_edit);
                        txt_message.setHintTextColor(Color.parseColor("#D7D6D6"));
                    }else {
                        btn_send_email.setBackgroundColor(Color.parseColor("#097233"));
                        btn_send_email.setEnabled(false);
                        txt_message.setBackgroundResource(R.drawable.border_danger);
                        txt_message.setHintTextColor(Color.parseColor("#F44336"));
                    }
                }else {
                    btn_send_email.setBackgroundColor(Color.parseColor("#E8F5E9"));
                    btn_send_email.setEnabled(false);
                    txt_message.setBackgroundResource(R.drawable.border_danger);
                    txt_message.setHintTextColor(Color.parseColor("#F44336"));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        txt_name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (txt_name.getText().toString().trim().equals("")){
                    txt_name.setBackgroundResource(R.drawable.border_danger);
                    txt_name.setHintTextColor(Color.parseColor("#F44336"));
                }else {
                    txt_name.setBackgroundResource(R.drawable.border_edit);
                    txt_name.setHintTextColor(Color.parseColor("#D7D6D6"));
                }
            }
        });
        txt_email.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (txt_email.getText().toString().trim().equals("")){
                    txt_email.setBackgroundResource(R.drawable.border_danger);
                    txt_email.setHintTextColor(Color.parseColor("#F44336"));
                }else {
                    txt_email.setBackgroundResource(R.drawable.border_edit);
                    txt_email.setHintTextColor(Color.parseColor("#D7D6D6"));
                }
            }
        });
        txt_message.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (txt_message.getText().toString().trim().equals("")){
                    txt_message.setBackgroundResource(R.drawable.border_danger);
                    txt_message.setHintTextColor(Color.parseColor("#F44336"));
                }else {
                    txt_message.setBackgroundResource(R.drawable.border_edit);
                    txt_message.setHintTextColor(Color.parseColor("#D7D6D6"));
                }
            }
        });
        btn_send_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nom = txt_name.getText().toString().trim();
                String email = txt_email.getText().toString().trim();
                String message = txt_message.getText().toString().trim();
                if (email.matches(emailPattern)){
                    txt_name.setEnabled(false);
                    txt_email.setEnabled(false);
                    txt_message.setEnabled(false);
                    btn_send_email.setEnabled(false);
                    r_my_loader.setVisibility(View.VISIBLE);
                    Calendar now = Calendar.getInstance();
                    contact(now.getTime().toString(),nom,email,message,"not seen");
                }else {
                    txt_email.setBackgroundResource(R.drawable.border_danger);
                    txt_email.setHintTextColor(Color.parseColor("#F44336"));
                    l_sect_alert_email.setVisibility(View.VISIBLE);
                    new CountDownTimer(4000,1000){
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {
                            l_sect_alert_email.setVisibility(View.GONE);
                        }
                    }.start();
                }
            }
        });
    }
    /*==================================================================*/
    public void contact(String date,String nom,String email,String message,String statut){
        Map<String,Object> contact = new HashMap<>();
        contact.put("date",date);
        contact.put("nom",nom);
        contact.put("email",email);
        contact.put("message",message);
        contact.put("statut",statut);

        db.collection("Contact").document(email+"-"+date).set(contact).addOnSuccessListener(Contact.this, new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                r_my_loader.setVisibility(View.GONE);
                r_alert_user.setVisibility(View.VISIBLE);
                txt_alert_user.setText("Votre message a été envoyé!");
                new CountDownTimer(2500,500){
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        startActivity(new Intent(getApplicationContext(), Home.class));
                        finish();
                    }
                }.start();
            }
        }).addOnFailureListener(Contact.this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                r_my_loader.setVisibility(View.GONE);
                txt_name.setEnabled(true);
                txt_email.setEnabled(true);
                txt_message.setEnabled(true);
                btn_send_email.setEnabled(true);
            }
        });
    }
    /*==================================================================*/
    //Mes méthodes overrides
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            startActivity(new Intent(getApplicationContext(), Home.class));
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(getApplicationContext(), Home.class));
            finish();
        }
        return false;
    }
    /*==================================================================*/
    /*==================================================================*/
    //La méthode qui envoi un message de contact
    private String send_msg(String nom,String email,String message){
        String result = "";
        OkHttpClient client = new OkHttpClient();
        RequestBody body = new FormBody.Builder()
                .add("token",token)
                .add("nom",nom)
                .add("email",email)
                .add("message",message)
                .build();

        Request request = new Request.Builder()
                .url(lien+"contact.php")
                .addHeader("Content-Type","text/json; Charset=UTF-8")
                .post(body)
                .build();
        Response response = null;
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            response = client.newCall(request).execute();
            if (response.code() == 200) {
                result = response.body().string();
                Log.d(TAG, "Réponse du serveur: " + result);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (response != null){
                response.body().close();
            }
        }
        return result;
    }
    /*==================================================================*/
}
