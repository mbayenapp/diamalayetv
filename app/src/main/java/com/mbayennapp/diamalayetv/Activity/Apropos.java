package com.mbayennapp.diamalayetv.Activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.mbayennapp.diamalayetv.R;

public class Apropos extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView txt_facebook,txt_instagram,txt_twitter,
            txt_mention_legal,txt_confidentialite,txt_condition,
            txt_copyright;
    private MaterialRippleLayout l_facebook,l_twitter,l_instagram,
            l_condition,l_confidentialite;
    /*==========================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apropos);
        /*================================================================*/
        toolbar = findViewById(R.id.toolbar_apropos);
        toolbar.setTitle("À propos");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        /*================================================================*/
        txt_facebook = (TextView)findViewById(R.id.txt_facebook);
        txt_instagram = (TextView)findViewById(R.id.txt_instagram);
        txt_twitter = (TextView)findViewById(R.id.txt_twitter);
        l_facebook = (MaterialRippleLayout)findViewById(R.id.l_facebook);
        l_twitter = (MaterialRippleLayout)findViewById(R.id.l_twitter);
        l_instagram = (MaterialRippleLayout)findViewById(R.id.l_instagram);
        l_condition = (MaterialRippleLayout)findViewById(R.id.l_condition);
        txt_mention_legal = (TextView)findViewById(R.id.txt_mention_legal);
        txt_confidentialite = (TextView)findViewById(R.id.txt_confidentialite);
        txt_condition = (TextView)findViewById(R.id.txt_condition);
        l_confidentialite = (MaterialRippleLayout) findViewById(R.id.l_confidentialite);
        txt_copyright = (TextView)findViewById(R.id.txt_copyright);
        /*================================================================*/
        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeueLTStd_Roman.otf");
        Typeface typegold = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeuecondenced.otf");
        txt_facebook.setTypeface(typeFace);
        txt_twitter.setTypeface(typeFace);
        txt_instagram.setTypeface(typeFace);
        txt_mention_legal.setTypeface(typegold);
        txt_confidentialite.setTypeface(typeFace);
        txt_condition.setTypeface(typeFace);
        txt_copyright.setTypeface(typeFace);
        /*================================================================*/

        /*================================================================*/
        l_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url_link = "https://www.facebook.com/layenedigital/";
                Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url_link));
                startActivity(myIntent);
            }
        });
        l_twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url_link = "https://twitter.com/akhloulahi";
                Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url_link));
                startActivity(myIntent);
            }
        });
        l_instagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url_link = "https://www.instagram.com/layenedigital/";
                Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url_link));
                startActivity(myIntent);
            }
        });
        /*================================================================*/
    }
    /*================================================================*/
    //Mes méthodes overrides
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            finish();
        }
        return false;
    }
    /*================================================================*/
}
