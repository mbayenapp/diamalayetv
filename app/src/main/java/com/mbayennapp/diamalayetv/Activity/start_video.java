package com.mbayennapp.diamalayetv.Activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.mbayennapp.diamalayetv.Adapter.Other_Video_Adapter;
import com.mbayennapp.diamalayetv.Helpers.SessionManagerLogin;
import com.mbayennapp.diamalayetv.Model.Videos;
import com.mbayennapp.diamalayetv.R;
import com.mbayennapp.diamalayetv.Utils.ConnexionInternet;
import com.mbayennapp.diamalayetv.Utils.Verification_Date;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class start_video extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener{
    private static final String TAG = start_video.class.getSimpleName();
    private String lien = "https://www.mabcompany.com/JL_API/";
    private String token = "layenne2017";
    public static final String API_KEY = "AIzaSyAUkyEG9duEFiVRqtWc3U8iHqNZDopIlQI";
    private TextView txt_title_video,txt_title_suivre,txt_content_article
            ,txt_dev_categorie_video,txt_date_sent_video,txt_lieu_video,txt_categorie_video;
    private ImageView icon_arrow_down,icon_arrow_up,
            i_share_video,img_favorite_out,img_favorite_in;
    String VIDEO_ID = null;
    private int id;
    private String date_sent,titre,content,lieu,photo,id_video,categorie,vdid;
    private ConnexionInternet connexionInternet;
    private List<Videos> videos = new ArrayList<>();
    private Videos video;
    private Other_Video_Adapter adapter;
    private ListView lst_other_video;
    private ScrollView scroll_other;
    private SessionManagerLogin sessionManagerLogin;
    private String rep_is_f = "";

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private DocumentReference darticles;
    /*====================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_video);
        /*====================================================*/
        Bundle extras = getIntent().getExtras();
        id = extras.getInt("id");
        date_sent = extras.getString("date_sent");
        titre = extras.getString("titre");
        content = extras.getString("content");
        vdid = extras.getString("vdid");
        //lieu = extras.getString("lieu");
        photo = extras.getString("photo");
        categorie = extras.getString("categorie");
        id_video = extras.getString("id_video");
        VIDEO_ID = id_video;
        /*Initialisation de youtube player*/
        YouTubePlayerView youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_player_video_ID);
        youTubePlayerView.initialize(API_KEY, this);
        connexionInternet = new ConnexionInternet();
        sessionManagerLogin = new SessionManagerLogin(this);
        /*====================================================*/
        Log.d(TAG,"categorie: "+categorie);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        /*====================================================*/
        txt_title_video = (TextView)findViewById(R.id.txt_title_video);
        icon_arrow_down = (ImageView)findViewById(R.id.icon_arrow_down);
        icon_arrow_up = (ImageView)findViewById(R.id.icon_arrow_up);
        i_share_video = (ImageView)findViewById(R.id.i_share_video);
        img_favorite_out = (ImageView)findViewById(R.id.img_favorite_out);
        img_favorite_in = (ImageView)findViewById(R.id.img_favorite_in);
        txt_title_suivre = (TextView)findViewById(R.id.txt_title_suivre);
        txt_content_article = (TextView)findViewById(R.id.txt_content_article);
        lst_other_video = (ListView)findViewById(R.id.lst_other_video);
        scroll_other = (ScrollView)findViewById(R.id.scroll_other);
        txt_categorie_video = (TextView)findViewById(R.id.txt_categorie_video);
        txt_dev_categorie_video = (TextView)findViewById(R.id.txt_dev_categorie_video);
        txt_date_sent_video = (TextView)findViewById(R.id.txt_date_sent_video);
        //txt_lieu_video = (TextView)findViewById(R.id.txt_lieu_video);
        txt_categorie_video = (TextView)findViewById(R.id.txt_categorie_video);
        /*====================================================*/
        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeueLTStd_Roman.otf");
        Typeface typegold = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeuecondenced.otf");
        txt_title_video.setTypeface(typeFace);
        txt_title_suivre.setTypeface(typegold);
        txt_content_article.setTypeface(typeFace);
        txt_categorie_video.setTypeface(typeFace);
        txt_dev_categorie_video.setTypeface(typeFace);
        txt_date_sent_video.setTypeface(typeFace);
       // txt_lieu_video.setTypeface(typeFace);
        txt_categorie_video.setTypeface(typegold);
        /*====================================================*/
        icon_arrow_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                icon_arrow_down.setVisibility(View.GONE);
                icon_arrow_up.setVisibility(View.VISIBLE);
                txt_content_article.setVisibility(View.VISIBLE);
            }
        });
        icon_arrow_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                icon_arrow_down.setVisibility(View.VISIBLE);
                icon_arrow_up.setVisibility(View.GONE);
                txt_content_article.setVisibility(View.GONE);

            }
        });
        txt_title_video.setText(titre);
        txt_content_article.setText(content);
        txt_dev_categorie_video.setText("  "+categorie);
        Verification_Date vd = new Verification_Date();
        String dt = vd.get_date(date_sent);
        txt_date_sent_video.setText(dt);
        //txt_lieu_video.setText(titre);
        /*===================================================*/
        if (!sessionManagerLogin.isLoggedIn()){
            img_favorite_out.setVisibility(View.GONE);
        }
        connexionInternet = new ConnexionInternet();
        get_other_videos_similaires(vdid);

        setListViewHeightBasedOnChildren(lst_other_video);
        scroll_other.scrollTo(0,0);
        lst_other_video.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Videos video = videos.get(i);
                Intent intent = new Intent(getApplicationContext(), start_video.class);
                intent.putExtra("id",video.getId());
                intent.putExtra("date_sent",video.getDate_sent());
                intent.putExtra("titre",video.getTitre());
                intent.putExtra("content",video.getDescription());
                intent.putExtra("photo",video.getImage());
                intent.putExtra("categorie",video.getCategorie());
                intent.putExtra("id_video",video.getId_video());
                intent.putExtra("vdid",video.getVdid());
                startActivity(intent);
                finish();
            }
        });
        i_share_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent share = new Intent(android.content.Intent.ACTION_SEND);
                share.setType("text/plain");
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                share.putExtra(Intent.EXTRA_SUBJECT, titre);
                share.putExtra(Intent.EXTRA_TEXT, "https://www.youtube.com/watch?v="+id_video);
                startActivity(Intent.createChooser(share, "Partager cette vidéo!"));
            }
        });
        img_favorite_in.setVisibility(View.GONE);
        img_favorite_out.setVisibility(View.VISIBLE);
        /*rep_is_f = is_my_favorite(id,sessionManagerLogin.getLoggedInUser());
        if (rep_is_f.equals("ok")){

        }
        img_favorite_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sessionManagerLogin.isLoggedIn()){
                    String rep = remove_favoirite(id,sessionManagerLogin.getLoggedInUser());
                    if (rep.equals("ok")){
                        rep_is_f = "no";
                        img_favorite_in.setVisibility(View.GONE);
                        img_favorite_out.setVisibility(View.VISIBLE);
                    }
                }else {
                    Toast.makeText(getApplicationContext(),"Connexion nécessaire",Toast.LENGTH_LONG).show();
                }
            }
        });
        img_favorite_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sessionManagerLogin.isLoggedIn()){
                    if (!rep_is_f.equals("ok")){
                        String rep = add_favoris(id,sessionManagerLogin.getLoggedInUser());
                        if (rep.equals("ok")){
                            rep_is_f = "ok";
                            img_favorite_in.setVisibility(View.VISIBLE);
                            img_favorite_out.setVisibility(View.GONE);
                        }
                    }else {
                        String rep = remove_favoirite(id,sessionManagerLogin.getLoggedInUser());
                        if (rep.equals("ok")){
                            rep_is_f = "no";
                            img_favorite_in.setVisibility(View.GONE);
                            img_favorite_out.setVisibility(View.VISIBLE);
                        }
                    }
                }else {
                    Toast.makeText(getApplicationContext(),"Connexion nécessaire",Toast.LENGTH_LONG).show();
                }
            }
        });*/
        /*===================================================*/
    }
    /*====================================================*/
    private void get_other_videos_similaires(final String vdid){
        //Query query = db.collection("Articles").orderBy("date", Query.Direction.DESCENDING);
        db.collection("Videos").orderBy("date", Query.Direction.DESCENDING).limit(5).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    Log.d(TAG, "Nbr articles: " + task.getResult().size());
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        if (!document.getId().equals(vdid)) {
                            if (document.get("categorie").toString().equals(categorie)) {
                                video = new Videos();
                                video.setCategorie(document.get("categorie").toString());
                                video.setDate_sent(document.get("date").toString());
                                video.setDescription(document.get("description").toString());
                                video.setImage(document.get("image").toString());
                                video.setTitre(document.get("titre").toString());
                                video.setId_video(document.get("id_video").toString());
                                video.setVdid(document.getId());
                                videos.add(video);
                            }
                        }
                    }
                    if (!videos.isEmpty()){
                        adapter = new Other_Video_Adapter(getApplicationContext(),videos);
                        lst_other_video.setAdapter(adapter);
                    }
                } else {
                    Log.d(TAG, "Error: " + task.getException());
                }
            }
        });
    }
    /*====================================================*/
    /*====================================================*/
    //Modifier la hauteur de mon listView
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
    /*==========================================================*/
    //La méthode qui permet d'ajouter un article à ces favoris
    private String add_favoris(int id_article,String tel){
        String result = "";
        OkHttpClient client = new OkHttpClient();
        RequestBody body = new FormBody.Builder()
                .add("token",token)
                .add("id", String.valueOf(id_article))
                .add("tel", tel)
                .build();

        final Request request = new Request.Builder()
                .url(lien+"videos/add_favorite.php")
                .addHeader("Content-Type","text/json; Charset=UTF-8")
                .post(body)
                .build();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Response response = null;
        try {
            response = client.newCall(request).execute();
            if (response.code() == 200) {
                JSONObject jsonObject = new JSONObject(response.body().string());
                result = jsonObject.optString("message");
                Log.d(TAG, "Réponse du serveur: " + jsonObject);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (response != null) {
                response.body().close();
            }
        }
        return result;
    }
    //La méthode qui retire un article des favorie
    private String remove_favoirite(int id_article,String tel){
        String result = "";
        OkHttpClient client = new OkHttpClient();
        RequestBody body = new FormBody.Builder()
                .add("token",token)
                .add("id", String.valueOf(id_article))
                .add("tel", tel)
                .build();

        final Request request = new Request.Builder()
                .url(lien+"videos/remove_favorite.php")
                .addHeader("Content-Type","text/json; Charset=UTF-8")
                .post(body)
                .build();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Response response = null;
        try {
            response = client.newCall(request).execute();
            if (response.code() == 200) {
                JSONObject jsonObject = new JSONObject(response.body().string());
                result = jsonObject.optString("message");
                Log.d(TAG, "Réponse du serveur: " + jsonObject);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (response != null) {
                response.body().close();
            }
        }
        return result;
    }
    /*=======================================================================*/
    //La méthode qui vérifie si un article est dans mes favoris
    private String is_my_favorite(int id_article,String tel){
        String result = "";
        OkHttpClient client = new OkHttpClient();
        RequestBody body = new FormBody.Builder()
                .add("token",token)
                .add("id", String.valueOf(id_article))
                .add("tel", tel)
                .build();

        final Request request = new Request.Builder()
                .url(lien+"videos/is_my_favorite.php")
                .addHeader("Content-Type","text/json; Charset=UTF-8")
                .post(body)
                .build();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Response response = null;
        try {
            response = client.newCall(request).execute();
            if (response.code() == 200) {
                JSONObject jsonObject = new JSONObject(response.body().string());
                result = jsonObject.optString("message");
                Log.d(TAG, "Réponse favoris: " + jsonObject);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (response != null) {
                response.body().close();
            }
        }
        return result;
    }
    /*======================================================================*/

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
        youTubePlayer.setPlayerStateChangeListener(playerStateChangeListener);
        youTubePlayer.setPlaybackEventListener(playbackEventListener);
        if (!wasRestored) {
            //player.cuePlaylist(VIDEO_ID);
            youTubePlayer.loadVideo(VIDEO_ID);
            //youTubePlayer.setFullscreen(true);
            youTubePlayer.play();

        }
    }
    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Toast.makeText(this, "Erreur d'initialisation de la vidéo!", Toast.LENGTH_LONG).show();
    }
    private YouTubePlayer.PlaybackEventListener playbackEventListener = new YouTubePlayer.PlaybackEventListener() {
        @Override
        public void onBuffering(boolean arg0) {
        }
        @Override
        public void onPaused() {
        }
        @Override
        public void onPlaying() {
        }
        @Override
        public void onSeekTo(int arg0) {
        }
        @Override
        public void onStopped() {
        }
    };
    private YouTubePlayer.PlayerStateChangeListener playerStateChangeListener = new YouTubePlayer.PlayerStateChangeListener() {
        @Override
        public void onAdStarted() {
        }
        @Override
        public void onError(YouTubePlayer.ErrorReason arg0) {
        }
        @Override
        public void onLoaded(String arg0) {
        }
        @Override
        public void onLoading() {
        }
        @Override
        public void onVideoEnded() {
        }
        @Override
        public void onVideoStarted() {
        }
    };
    /*====================================================*/
}
