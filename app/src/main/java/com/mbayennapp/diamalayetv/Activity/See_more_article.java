package com.mbayennapp.diamalayetv.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.widget.ShareButton;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.mbayennapp.diamalayetv.Adapter.Comment_Adapter;
import com.mbayennapp.diamalayetv.Adapter.Other_Adapter;
import com.mbayennapp.diamalayetv.Helpers.SessionManagerLogin;
import com.mbayennapp.diamalayetv.Model.Articles;
import com.mbayennapp.diamalayetv.Model.Comments;
import com.mbayennapp.diamalayetv.R;
import com.mbayennapp.diamalayetv.Utils.ConnexionInternet;
import com.mbayennapp.diamalayetv.Utils.Verification_Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class See_more_article extends AppCompatActivity{
    private static final String TAG = See_more_article.class.getSimpleName();
    private String lien = "https://www.mabcompany.com/JL_API/";
    private String token = "layenne2017";
    private Toolbar toolbar;
    private ImageView img_article,img_favorite_in,img_favorite_out,
            img_start_video,img_send_message,img_start_comment;
    private TextView txt_titre,txt_lieu,
            txt_date_article,txt_favorite,txt_no_favorite,
            txt_titre_lieux,txt_title_article_lire,txt_alert_user;
    private TextView txt_content_article,txt_title_comment;
    private String adid = "";
    private String date_sent,titre,content,lieu,photo,id_video,source,type;
    private ConnexionInternet connexionInternet;
    private RelativeLayout r_sect_actionuser,r_alert_user;
    private Animation animeSectShare,animBottom;
    private ScrollView scroll_sect_see_more;
    private ListView lst_other_article;
    private LinearLayout l_share,l_favorite,l_form_add_message,
            l_sect_option_see_more;
    private SessionManagerLogin sessionManagerLogin;
    private String rep_is_f = "";
    private Other_Adapter adapter;
    private List<Articles> articles = new ArrayList<>();
    private Articles article;
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private DocumentReference darticles;
    private NestedScrollView scrollView;
    private ShareButton fb_share_button;
    private EditText txt_message;
    private List<Comments> comments = new ArrayList<>();
    private Comments comment;
    private Comment_Adapter comment_adapter;
    private RecyclerView lst_comments;
    /*===========================================================*/
    private int init_scroll = 0;
    /*===========================================================*/
    private CallbackManager callbackManager;
    private ShareDialog shareDialog;
    /*===========================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_more_article);
        /*========================================================*/
        toolbar = (Toolbar)findViewById(R.id.toolbar_see_more_article);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        /*========================================================*/
        Bundle extras = getIntent().getExtras();
        adid = extras.getString("adid");
        date_sent = extras.getString("date_sent");
        titre = extras.getString("titre");
        content = extras.getString("content");
        lieu = extras.getString("lieu");
        photo = extras.getString("photo");
        source = extras.getString("source");
        id_video = extras.getString("id_video");
        type = extras.getString("type");
        connexionInternet = new ConnexionInternet();
        sessionManagerLogin = new SessionManagerLogin(this);
        /*========================================================*/
        topAnimation();
        BottomAnimation();
        /*================================================================================*/
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        get_all_articles(adid);
        /*================================================================================*/
        img_article = (ImageView)findViewById(R.id.img_article);
        txt_lieu = (TextView)findViewById(R.id.txt_lieu_article);
        txt_titre = (TextView)findViewById(R.id.txt_title_article);
        txt_content_article = (TextView) findViewById(R.id.txt_content_article);
        txt_date_article = (TextView)findViewById(R.id.txt_date_article);
        r_sect_actionuser = (RelativeLayout)findViewById(R.id.r_sect_actionuser);
        //scroll_sect_see_more = (ScrollView)findViewById(R.id.scroll_sect_see_more);
        lst_other_article = (ListView)findViewById(R.id.lst_other_article);
        l_share = (LinearLayout)findViewById(R.id.l_share);
        l_favorite = (LinearLayout)findViewById(R.id.l_favorite);
        img_favorite_in = (ImageView)findViewById(R.id.img_favorite_in);
        img_favorite_out = (ImageView)findViewById(R.id.img_favorite_out);
        txt_favorite = (TextView)findViewById(R.id.txt_favorite);
        txt_no_favorite = (TextView)findViewById(R.id.txt_no_favorite);
        txt_titre_lieux = (TextView)findViewById(R.id.txt_titre_lieux);
        txt_title_article_lire = (TextView)findViewById(R.id.txt_title_article_lire);
        //txt_partage = (TextView)findViewById(R.id.txt_partage);
        img_start_video = (ImageView)findViewById(R.id.img_start_video);
        scrollView = (NestedScrollView)findViewById(R.id.scrollView);
        fb_share_button = (ShareButton)findViewById(R.id.fb_share_button);
        txt_message = (EditText)findViewById(R.id.txt_message);
        img_send_message = (ImageView)findViewById(R.id.img_send_message);
        r_alert_user = (RelativeLayout)findViewById(R.id.r_alert_user);
        txt_alert_user = (TextView)findViewById(R.id.txt_alert_user) ;
        lst_comments = (RecyclerView) findViewById(R.id.lst_comments);
        txt_title_comment = (TextView)findViewById(R.id.txt_title_comment);
        img_start_comment = (ImageView)findViewById(R.id.img_start_comment);
        l_form_add_message = (LinearLayout)findViewById(R.id.l_form_add_message);
        l_sect_option_see_more = (LinearLayout)findViewById(R.id.l_sect_option_see_more);
        /*========================================================*/
        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeueLTStd_Roman.otf");
        Typeface typegold = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeuecondenced.otf");
        txt_titre.setTypeface(typegold);
        txt_lieu.setTypeface(typegold);
        txt_date_article.setTypeface(typegold);
        txt_content_article.setTypeface(typeFace);
        txt_titre_lieux.setTypeface(typegold);
        txt_title_article_lire.setTypeface(typegold);
        //txt_partage.setTypeface(typeFace);
        txt_favorite.setTypeface(typeFace);
        txt_no_favorite.setTypeface(typeFace);
        fb_share_button.setTypeface(typeFace);
        txt_message.setTypeface(typeFace);
        txt_alert_user.setTypeface(typeFace);
        txt_title_comment.setTypeface(typegold);
        /*========================================================*/
        if (connexionInternet.isConnectedInternet(this)){

            //Voire si l'article est dans mes favoris
            is_my_favorite(adid);
            /*====================================================*/
            if (!adid.equals("")){
                String link_img = photo;
                Glide.with(this).load(link_img).centerCrop().crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(img_article);
                txt_titre.setText(titre);
                txt_lieu.setText(lieu);
                txt_content_article.setText(content);
                Verification_Date v = new Verification_Date();
                String dt = v.get_date(date_sent);
                txt_date_article.setText(dt);
                scrollView.setScrollY(0);
            }
            /*====================================================*/
            /*rep_is_f = is_my_favorite(article.getId(),sessionManagerLogin.getLoggedInUser());

            txt_no_favorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (sessionManagerLogin.isLoggedIn()){
                        String rep = remove_favoirite(article.getId(),sessionManagerLogin.getLoggedInUser());
                        if (rep.equals("ok")){
                            rep_is_f = "no";
                            img_favorite_in.setVisibility(View.GONE);
                            img_favorite_out.setVisibility(View.VISIBLE);
                            txt_favorite.setVisibility(View.VISIBLE);
                            txt_no_favorite.setVisibility(View.GONE);
                        }
                    }else {
                        Toast.makeText(getApplicationContext(),"Connexion nécessaire",Toast.LENGTH_LONG).show();
                    }
                }
            });*/

            img_start_comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    l_sect_option_see_more.setVisibility(View.GONE);
                    l_form_add_message.setVisibility(View.VISIBLE);
                }
            });

            lst_other_article.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Articles article = articles.get(i);
                    Intent intent = new Intent(getApplicationContext(), See_more_article.class);
                    intent.putExtra("adid",article.getAdid());
                    intent.putExtra("date_sent",article.getDate_sent());
                    intent.putExtra("titre",article.getTitre());
                    intent.putExtra("content",article.getContent());
                    intent.putExtra("lieu",article.getLieu());
                    intent.putExtra("photo",article.getPhoto());
                    intent.putExtra("source",article.getSource());
                    intent.putExtra("id_video",article.getId_video());
                    intent.putExtra("type",article.getType());
                    startActivity(intent);
                    finish();
                }
            });
            shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                @Override
                public void onSuccess(Sharer.Result result) {

                }

                @Override
                public void onCancel() {

                }

                @Override
                public void onError(FacebookException error) {

                }
            });
            try {
                // à voire

                URL url = new URL(photo);

                Bitmap imgBit = BitmapFactory.decodeFile(String.valueOf(url));
                Log.d(TAG,"ImgBit : "+imgBit);
                SharePhoto sharePhoto = new SharePhoto.Builder().setImageUrl(Uri.parse(photo)).build();
                final ShareLinkContent content = new ShareLinkContent.Builder()
                        .setContentUrl(Uri.parse(photo))
                        .build();
                fb_share_button.setShareContent(content);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            //Action de partage
            l_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    /*Intent share = new Intent(android.content.Intent.ACTION_SEND);
                    share.setType("text/plain");
                    share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                    share.putExtra(Intent.EXTRA_SUBJECT, titre);
                    share.putExtra(Intent.EXTRA_TEXT, Html.fromHtml("<p>" + content + "</p>"));
                    startActivity(Intent.createChooser(share, "Partager cet article"));*/


                }
            });

            //Envoyer un commentaire
            img_send_message.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String message = txt_message.getText().toString().trim();
                    if (sessionManagerLogin.isLoggedIn()){
                        if (message.equals("")){
                            r_alert_user.setVisibility(View.VISIBLE);
                            r_alert_user.startAnimation(animeSectShare);
                            txt_alert_user.setText("Vous devez indiquer votre commentaire.");
                            new CountDownTimer(5000,1000){
                                @Override
                                public void onTick(long millisUntilFinished) {

                                }

                                @Override
                                public void onFinish() {
                                    r_alert_user.setVisibility(View.GONE);
                                    //r_alert_user.startAnimation(animeSectShare);
                                }
                            }.start();
                        }else {
                            Calendar now = Calendar.getInstance();
                            int year = now.get(Calendar.YEAR);
                            int month = now.get(Calendar.MONTH) + 1;
                            int day = now.get(Calendar.DAY_OF_MONTH);
                            int hour = now.get(Calendar.HOUR);
                            int minute = now.get(Calendar.MINUTE);
                            String monRight = "";
                            String dayRight = "";
                            if (day < 10){
                                dayRight = "0"+day;
                            }else {
                                dayRight = String.valueOf(day);
                            }
                            if (month < 10){
                                monRight = "0"+month;
                            }else {
                                monRight = String.valueOf(month);
                            }
                            if (monRight != "" && dayRight != ""){
                                String date = dayRight+"/"+monRight+"/"+year+" "+hour+":"+minute;
                                comment_article(sessionManagerLogin.getLoggedInUser().getEmail(),message,sessionManagerLogin.getLoggedInUser().getProfil(),adid,date);
                            }
                        }
                    }else {
                      r_alert_user.setVisibility(View.VISIBLE);
                      r_alert_user.startAnimation(animeSectShare);
                      txt_alert_user.setText("Vous devez vous connecter pour poster un commentaire.");
                      new CountDownTimer(5000,1000){
                          @Override
                          public void onTick(long millisUntilFinished) {

                          }

                          @Override
                          public void onFinish() {
                              r_alert_user.setVisibility(View.GONE);
                              //r_alert_user.startAnimation(animeSectShare);
                          }
                      }.start();
                    }
                }
            });

            //Action d'ajout au favoris
            l_favorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Map<String,Object> favorie = new HashMap<>();
                    favorie.put(adid,"true");
                    if (sessionManagerLogin.isLoggedIn()) {
                        if (!rep_is_f.equals("ok")) {
                            db.collection("Favorites").document(mAuth.getCurrentUser().getUid()).update(favorie).addOnCompleteListener(See_more_article.this, new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        rep_is_f = "ok";
                                        img_favorite_in.setVisibility(View.VISIBLE);
                                        img_favorite_out.setVisibility(View.GONE);
                                        txt_favorite.setVisibility(View.GONE);
                                        txt_no_favorite.setVisibility(View.VISIBLE);
                                    } else {

                                    }
                                }
                            });
                        } else {
                            Map<String,Object> delField = new HashMap<>();
                            delField.put(adid, FieldValue.delete());
                            db.collection("Favorites").document(mAuth.getCurrentUser().getUid()).update(delField).addOnCompleteListener(See_more_article.this, new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        rep_is_f = "no";
                                        img_favorite_in.setVisibility(View.GONE);
                                        img_favorite_out.setVisibility(View.VISIBLE);
                                        txt_favorite.setVisibility(View.VISIBLE);
                                        txt_no_favorite.setVisibility(View.GONE);
                                    } else {

                                    }
                                }
                            });
                        }
                    }else {

                    }
                    /*if (sessionManagerLogin.isLoggedIn()){
                        if (!rep_is_f.equals("ok")){
                            String rep = add_favoris(article.getId(),sessionManagerLogin.getLoggedInUser());
                            if (rep.equals("ok")){
                                rep_is_f = "ok";
                                img_favorite_in.setVisibility(View.VISIBLE);
                                img_favorite_out.setVisibility(View.GONE);
                                txt_favorite.setVisibility(View.GONE);
                                txt_no_favorite.setVisibility(View.VISIBLE);
                            }
                        }else {
                            String rep = remove_favoirite(article.getId(),sessionManagerLogin.getLoggedInUser());
                            if (rep.equals("ok")){
                                rep_is_f = "no";
                                img_favorite_in.setVisibility(View.GONE);
                                img_favorite_out.setVisibility(View.VISIBLE);
                                txt_favorite.setVisibility(View.VISIBLE);
                                txt_no_favorite.setVisibility(View.GONE);
                            }
                        }
                    }else {
                        Toast.makeText(getApplicationContext(),"Connexion nécessaire",Toast.LENGTH_LONG).show();
                    }*/
                }
            });


        }
        if (type.equals("vidéo")){
            img_start_video.setVisibility(View.VISIBLE);
        }
        img_start_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), See_more_video.class);
                intent.putExtra("id",article.getId());
                intent.putExtra("date_sent",article.getDate_sent());
                intent.putExtra("titre",article.getTitre());
                intent.putExtra("content",article.getContent());
                intent.putExtra("lieu",article.getLieu());
                intent.putExtra("photo",article.getPhoto());
                intent.putExtra("source",article.getSource());
                intent.putExtra("id_video",article.getId_video());
                startActivity(intent);
            }
        });

        scrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY > oldScrollY){
                    if (scrollY > oldScrollY) {
                        r_sect_actionuser.setVisibility(View.VISIBLE);
                        r_sect_actionuser.startAnimation(animeSectShare);
                    }
                }
            }
        });
    }

    /*============================================================*/
    /* La méthode qui ajoute un commentaire */
    private  void comment_article(String email, String message, String profil, final String article, String date){
        final Map<String, Object> comment = new HashMap<>();
        comment.put("article",article);
        comment.put("date",date);
        comment.put("email",email);
        comment.put("message",message);
        comment.put("profil",profil);
        db.collection("Comments").document().set(comment).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    txt_message.setText("");
                    l_sect_option_see_more.setVisibility(View.VISIBLE);
                    l_form_add_message.setVisibility(View.GONE);
                    get_comments(article);
                }
            }
        });
    }
    /* Fin */
    /* La méthode qui retourne les commentaires */
    private void get_comments(String article){
        db.collection("Comments").whereEqualTo("article",article).orderBy("date", Query.Direction.DESCENDING).limit(10).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                Log.d(TAG, "Commentaires: " + task.getResult().size());
                if (task.isSuccessful()){
                    try {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            Log.d(TAG,"Comment: "+document.getId());
                            comment = new Comments();
                            comment.setArticle(document.get("article").toString());
                            comment.setDate(document.get("date").toString());
                            comment.setEmail(document.get("email").toString());
                            comment.setMessage(document.get("message").toString());
                            comment.setProfil(document.get("profil").toString());
                            comments.add(comment);
                        }
                        if (comments.size() > 0){
                            comment_adapter = new Comment_Adapter(See_more_article.this,comments);
                            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 1);
                            lst_comments.setHasFixedSize(false);
                            lst_comments.setNestedScrollingEnabled(false);
                            lst_comments.setLayoutManager(mLayoutManager);
                            lst_comments.setAdapter(comment_adapter);
                            lst_comments.setVisibility(View.VISIBLE);
                            lst_comments.setScrollY(0);
                            scrollView.scrollTo(0,0);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        });
    }
    /* Fin */
    private void get_all_articles(final String adid){
        //Query query = db.collection("Articles").orderBy("date", Query.Direction.DESCENDING);
        db.collection("Articles").orderBy("date", Query.Direction.DESCENDING).limit(4).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    Log.d(TAG, "Nbr articles: " + task.getResult().size());
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        if (!document.getId().equals(adid)) {
                            article = new Articles();
                            article.setAdid(document.getId());
                            article.setDate_sent(document.get("date").toString());
                            article.setTitre(document.get("titre").toString());
                            article.setContent(document.get("content").toString());
                            article.setPhoto(document.get("photo").toString());
                            article.setSource(document.get("source").toString());
                            article.setLieu(document.get("lieu").toString());
                            article.setType(document.get("type").toString());
                            article.setId_video(document.get("id_video").toString());
                            articles.add(article);
                        }
                    }
                    if (!articles.isEmpty()){
                        adapter = new Other_Adapter(getApplicationContext(),articles);
                        lst_other_article.setAdapter(adapter);
                        get_comments(adid);
                        lst_other_article.setScrollY(0);
                        scrollView.scrollTo(0,0);
                    }
                } else {
                    Log.d(TAG, "Error: " + task.getException());
                }
            }
        });
    }
    //La méthode qui vérifie si l'article est dans mes favoris
    private void is_my_favorite(final String adid){
        if (sessionManagerLogin.isLoggedIn()) {
            Log.d(TAG,"uid: "+mAuth.getCurrentUser().getUid());
            db.collection("Favorites").document(mAuth.getCurrentUser().getUid()).get().addOnCompleteListener(See_more_article.this, new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "Result: " + task.getResult().getData());
                        Log.d(TAG,"Key :"+task.getResult().get(adid));
                        if (task.getResult().exists()) {
                            if (task.getResult().get(adid) != null) {
                                rep_is_f = "ok";
                                img_favorite_in.setVisibility(View.VISIBLE);
                                img_favorite_out.setVisibility(View.GONE);
                                txt_no_favorite.setVisibility(View.VISIBLE);
                                txt_favorite.setVisibility(View.GONE);
                            }
                        }
                    } else {
                        Log.d(TAG, "Error: " + task.getException());
                    }
                }
            });
        }
    }
    /*============================================================*/
    //Mes méthodes overrides
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            finish();
        }
        return false;
    }
    /*============================================================*/
    //Mes animations
    private void topAnimation(){
        animBottom = AnimationUtils.loadAnimation(this,R.anim.grow_from_bottom);
        animBottom.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    private void BottomAnimation(){
        animeSectShare = AnimationUtils.loadAnimation(this,R.anim.push_top_in);
        animeSectShare.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    /*============================================================*/
    //La méthode qui récupére l'article dont l'id est fournit en paramètre
    private Articles get_Article(int id){
        Articles article = new Articles();
        String result = "";
        OkHttpClient client = new OkHttpClient();
        RequestBody body = new FormBody.Builder()
                .add("token",token)
                .add("id", String.valueOf(id))
                .build();

        final Request request = new Request.Builder()
                .url(lien+"Articles/search_article.php")
                .addHeader("Content-Type","text/json; Charset=UTF-8")
                .post(body)
                .build();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Response response = null;
        try {
            response = client.newCall(request).execute();
            if (response.code() == 200) {
                result = response.body().string();
                Log.d(TAG, "Articles: " + result);
                if (result != null) {
                    JSONObject jsonObject = new JSONObject(result);
                    article.setId(jsonObject.optInt("id"));
                    article.setDate_sent(jsonObject.optString("date_sent"));
                    article.setTitre(jsonObject.optString("titre"));
                    article.setContent(jsonObject.optString("content"));
                    article.setLieu(jsonObject.optString("lieu"));
                    article.setPhoto(jsonObject.optString("photo"));
                    article.setSource(jsonObject.optString("source"));
                    article.setType(jsonObject.optString("type"));
                    if (jsonObject.optString("type").equals("vidéo")) {
                        article.setId_video(jsonObject.optString("id_video"));
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }finally {
            if (response != null) {
                response.body().close();
            }
        }
        return  article;
    }
    /*=======================================================================*/
    //La méthode qui permet d'ajouter un article à ces favoris
    private String add_favoris(int id_article,String tel){
        String result = "";
        OkHttpClient client = new OkHttpClient();
        RequestBody body = new FormBody.Builder()
                .add("token",token)
                .add("id", String.valueOf(id_article))
                .add("tel", tel)
                .build();

        final Request request = new Request.Builder()
                .url(lien+"Articles/add_favorite.php")
                .addHeader("Content-Type","text/json; Charset=UTF-8")
                .post(body)
                .build();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Response response = null;
        try {
            response = client.newCall(request).execute();
            if (response.code() == 200) {
                JSONObject jsonObject = new JSONObject(response.body().string());
                result = jsonObject.optString("message");
                Log.d(TAG, "Réponse du serveur: " + jsonObject);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (response != null) {
                response.body().close();
            }
        }
        return result;
    }
    //La méthode qui retire un article des favorie
    private String remove_favoirite(int id_article,String tel){
        String result = "";
        OkHttpClient client = new OkHttpClient();
        RequestBody body = new FormBody.Builder()
                .add("token",token)
                .add("id", String.valueOf(id_article))
                .add("tel", tel)
                .build();

        final Request request = new Request.Builder()
                .url(lien+"Articles/remove_favorite.php")
                .addHeader("Content-Type","text/json; Charset=UTF-8")
                .post(body)
                .build();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Response response = null;
        try {
            response = client.newCall(request).execute();
            if (response.code() == 200) {
                JSONObject jsonObject = new JSONObject(response.body().string());
                result = jsonObject.optString("message");
                Log.d(TAG, "Réponse du serveur: " + jsonObject);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (response != null) {
                response.body().close();
            }
        }
        return result;
    }
    /*=======================================================================*/

    /*=======================================================================*/
    private List<Articles> get_other_articles(int id){
        List<Articles> articles = new ArrayList<>();
        Articles article;
        String result = "";
        OkHttpClient client = new OkHttpClient();
        RequestBody body = new FormBody.Builder()
                .add("token",token)
                .add("id", String.valueOf(id))
                .build();

        final Request request = new Request.Builder()
                .url(lien+"Articles/list_other_article.php")
                .addHeader("Content-Type","text/json; Charset=UTF-8")
                .post(body)
                .build();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Response response = null;
        try {
            response = client.newCall(request).execute();
            if (response.code() == 200) {
                result = response.body().string();
                Log.d(TAG, "Articles: " + result);
                if (result != null) {
                    JSONArray jsonArray = new JSONArray(result);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        article = new Articles();
                        article.setId(jsonObject.optInt("id"));
                        article.setDate_sent(jsonObject.optString("date_sent"));
                        article.setTitre(jsonObject.optString("titre"));
                        article.setContent(jsonObject.optString("content"));
                        article.setLieu(jsonObject.optString("lieu"));
                        article.setPhoto(jsonObject.optString("photo"));
                        article.setSource(jsonObject.optString("source"));
                        article.setType(jsonObject.optString("type"));
                        articles.add(article);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }finally {
            if (response != null) {
                response.body().close();
            }
        }
        return articles;
    }
}
