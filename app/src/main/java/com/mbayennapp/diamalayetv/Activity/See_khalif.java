package com.mbayennapp.diamalayetv.Activity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mbayennapp.diamalayetv.R;

public class See_khalif extends AppCompatActivity {
    private Toolbar toolbar;
    private String lien = "https://www.mabcompany.com/JL_API/";
    private String token = "layenne2017";
    private ImageView img_article;
    private TextView txt_titre,txt_lieu,
            txt_date_article,txt_titre_lieux;
    private TextView txt_content_article;
    /*================================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_khalif);
        /*================================================================*/
        Bundle extras = getIntent().getExtras();
        final int id = extras.getInt("id");
        final  String nom = extras.getString("nom");
        final String date_in = extras.getString("date_in");
        final String date_out = extras.getString("date_out");
        final String description = extras.getString("description");
        final String photo = extras.getString("photo");
        /*================================================================*/
        toolbar = (Toolbar)findViewById(R.id.toolbar_see_khalif);
        toolbar.setTitle("Détails du Khalif");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        /*================================================================*/
        img_article = (ImageView)findViewById(R.id.img_article);
        txt_titre = (TextView)findViewById(R.id.txt_title_article);
        txt_lieu = (TextView)findViewById(R.id.txt_titre_lieux);
        txt_titre_lieux = (TextView)findViewById(R.id.txt_lieu_article);
        txt_date_article = (TextView)findViewById(R.id.txt_date_article);
        txt_content_article = (TextView)findViewById(R.id.txt_content_article);
        /*================================================================*/
        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeueLTStd_Roman.otf");
        Typeface typegold = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeuecondenced.otf");
        /*================================================================*/
        txt_titre.setTypeface(typegold);
        txt_lieu.setTypeface(typeFace);
        txt_titre_lieux.setTypeface(typeFace);
        txt_content_article.setTypeface(typeFace);
        /*================================================================*/
        txt_titre.setText(nom);
        if (!date_out.equals("NULL")) {
            txt_titre_lieux.setText(date_in + "-" + date_out);
        }else {
            txt_titre_lieux.setText("Khalif actuel");
        }
        txt_content_article.setText(description);
        String link_img = photo;
        Glide.with(getApplicationContext()).load(link_img).centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL).into(img_article);
        /*================================================================*/
    }
    /*================================================================*/
    //Mes méthodes overrides
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            finish();
        }
        return false;
    }
    /*================================================================*/
    /*================================================================*/
}
