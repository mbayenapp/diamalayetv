package com.mbayennapp.diamalayetv.Activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.mbayennapp.diamalayetv.R;
import com.mbayennapp.diamalayetv.Utils.ConnexionInternet;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Forgot extends AppCompatActivity {
    private static final String TAG = Forgot.class.getSimpleName();
    private String lien = "https://www.mabcompany.com/JL_API/";
    private String token = "layenne2017";
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 0;
    private Toolbar toolbar;
    private TextView txt_my_text_forgot,txt_alert_user;
    private Button btn_go_next_step;
    private RelativeLayout r_loader_login,r_alert_user,r_msg_succes;
    private int statutBar = 0;
    private Handler handler;
    private ConnexionInternet connexionInternet;
    private Animation animSideTop,animSideFadeOut;
    private LinearLayout l_sect_first_form;
    private TextInputEditText txt_email;
    private TextView txt_email_error,txt_my_title_success,txt_my_success_conv;
    private String code = "";
    /*=========================================================*/
    private FirebaseAuth mAuth;
    /*=========================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);
        /*=========================================================*/
        toolbar = (Toolbar)findViewById(R.id.toolbar_forgot);
        toolbar.setTitle("Mot de passe oublié");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        connexionInternet = new ConnexionInternet();
        /*=========================================================*/
        mAuth = FirebaseAuth.getInstance();

        final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        /*=========================================================*/
        final int permissionCheeck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS);
        if (permissionCheeck != PackageManager.PERMISSION_GRANTED){
            if (ActivityCompat.shouldShowRequestPermissionRationale(Forgot.this, Manifest.permission.READ_SMS)){

            }else {
                ActivityCompat.requestPermissions(Forgot.this,
                        new String[]{Manifest.permission.READ_SMS,Manifest.permission.RECEIVE_SMS},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);
            }
        }
        /*========================================================*/
        handler = new Handler();
        txt_my_text_forgot = (TextView)findViewById(R.id.txt_my_text_forgot);
        btn_go_next_step = (Button)findViewById(R.id.btn_go_next_step);
        r_loader_login = (RelativeLayout)findViewById(R.id.r_loader_login);
        r_alert_user = (RelativeLayout)findViewById(R.id.r_alert_user);
        txt_alert_user = (TextView)findViewById(R.id.txt_alert_user);

        l_sect_first_form = (LinearLayout)findViewById(R.id.l_sect_first_form);
        txt_email = (TextInputEditText)findViewById(R.id.txt_email);
        txt_email_error = (TextView)findViewById(R.id.txt_email_error);
        txt_my_title_success = (TextView)findViewById(R.id.txt_my_title_success);
        txt_my_success_conv = (TextView)findViewById(R.id.txt_my_success_conv);
        r_msg_succes = (RelativeLayout)findViewById(R.id.r_msg_succes);
        /*=========================================================*/
        topAnimation();
        fadeOutAnimation();
        Typeface typeFace = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeueLTStd_Roman.otf");
        Typeface typegold = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeuecondenced.otf");
        /*=========================================================*/
        txt_my_text_forgot.setTypeface(typeFace);
        btn_go_next_step.setTypeface(typegold);
        txt_email_error.setTypeface(typeFace);
        txt_my_title_success.setTypeface(typegold);
        txt_my_success_conv.setTypeface(typeFace);
        /*=========================================================*/
        /*===========================================================*/

        /*=========================================================*/
        /*
        * Mes actions clicks
        * */

        r_msg_succes.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                txt_email.setText("");
                txt_email.setEnabled(false);
                btn_go_next_step.setEnabled(false);
                r_msg_succes.setVisibility(View.GONE);
                //startActivity(new Intent(getApplicationContext(),Login.class));
                finish();
                return false;
            }
        });

        btn_go_next_step.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = txt_email.getText().toString().trim();
                if (email.equals("")){
                    txt_email_error.setText(R.string.txt_email_empty);
                    txt_email_error.setVisibility(View.VISIBLE);
                }else if (!email.matches(emailPattern)){
                    txt_email_error.setText(R.string.txt_email_invalide);
                    txt_email_error.setVisibility(View.VISIBLE);
                }else {
                    r_loader_login.setVisibility(View.VISIBLE);
                    txt_email_error.setText("");
                    txt_email_error.setVisibility(View.GONE);
                    mAuth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()){
                                r_loader_login.setVisibility(View.GONE);
                                r_msg_succes.setVisibility(View.VISIBLE);
                            }else {
                                r_alert_user.setVisibility(View.VISIBLE);
                                txt_alert_user.setText("Une erreur est survenue lors de la ré-initialisation du mot de pase!");
                                new CountDownTimer(5000,1000){
                                    @Override
                                    public void onTick(long millisUntilFinished) {

                                    }

                                    @Override
                                    public void onFinish() {
                                        r_alert_user.setVisibility(View.GONE);
                                        r_alert_user.startAnimation(animSideFadeOut);
                                    }
                                }.start();
                            }
                        }
                    });
                }
            }
        });

        /*
        * Fin mes actions clicks
        * */
        /*=========================================================*/
    }
    /*=========================================================*/
    //Mes méthodes overrides
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            //startActivity(new Intent(getApplicationContext(),Login.class));
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            //startActivity(new Intent(getApplicationContext(),Login.class));
            finish();
        }
        return false;
    }

    private void topAnimation(){
        animSideTop = AnimationUtils.loadAnimation(this,R.anim.push_top_in);
        animSideTop.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    private void fadeOutAnimation(){
        animSideFadeOut = AnimationUtils.loadAnimation(this,R.anim.disappear);
        animSideFadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    //Fin méthode overrides
    /*=========================================================*/
    //Mes méthodes
    private JSONObject send_sms(String tel){
        String result = "";
        JSONObject jsonObject = null;
        OkHttpClient client = new OkHttpClient();
        RequestBody body = new FormBody.Builder()
                .add("token",token)
                .add("tel",tel)
                .build();

        Request request = new Request.Builder()
                .url(lien+"Users/send_code.php")
                .addHeader("Content-Type","text/json; Charset=UTF-8")
                .post(body)
                .build();
        Response response = null;
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            response = client.newCall(request).execute();
            if (response.code() == 200) {
                result = response.body().string();
                if (result != null) {
                    jsonObject = new JSONObject(result);
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }finally {
            if (response != null){
                response.body().close();
            }
        }
        return jsonObject;
    }


    private JSONObject verify_code(String tel, String code){
        String result = "";
        JSONObject jsonObject = null;
        OkHttpClient client = new OkHttpClient();
        RequestBody body = new FormBody.Builder()
                .add("token",token)
                .add("tel",tel)
                .add("code",code)
                .build();

        Request request = new Request.Builder()
                .url(lien+"Users/verify_code.php")
                .addHeader("Content-Type","text/json; Charset=UTF-8")
                .post(body)
                .build();
        Response response = null;
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            response = client.newCall(request).execute();
            if (response.code() == 200) {
                result = response.body().string();
                Log.d(TAG, "Etat: " + result);
                if (result != null) {
                    jsonObject = new JSONObject(result);
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }finally {
            if (response != null){
                response.body().close();
            }
        }

        return jsonObject;
    }

    private JSONObject manage_password(String tel,String password){
        String result = "";
        JSONObject jsonObject = null;
        OkHttpClient client = new OkHttpClient();
        RequestBody body = new FormBody.Builder()
                .add("token",token)
                .add("tel",tel)
                .add("password",password)
                .build();

        Request request = new Request.Builder()
                .url(lien+"Users/manage_password.php")
                .addHeader("Content-Type","text/json; Charset=UTF-8")
                .post(body)
                .build();
        Response response = null;
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            response = client.newCall(request).execute();
            if (response.code() == 200) {
                result = response.body().string();
                Log.d(TAG, "Modification du mot de passe: " + result);
                if (result != null) {
                    jsonObject = new JSONObject(result);
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }finally {
            if (response != null){
                response.body().close();
            }
        }

        return jsonObject;
    }

    //Fin mes méthodes
    /*=========================================================*/
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                } else {

                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}
