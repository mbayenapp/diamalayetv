package com.mbayennapp.diamalayetv.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mbayennapp.diamalayetv.Model.Khalifs;
import com.mbayennapp.diamalayetv.R;

import java.util.List;

/**
 * Created by Mbaye on 25/09/2017.
 */

public class Khalifs_Adapter extends BaseAdapter {
    private static final String TAG = "My_khalif";
    private Context context;
    private List<Khalifs> khalifsItems;
    private LayoutInflater inflater;
    private String lien = "https://www.mabcompany.com/JL_API/";
    private String token = "layenne2017";
    private ImageView img_khalif;
    private TextView name_khalif,periode,dev_khalif;

    public Khalifs_Adapter(Context context, List<Khalifs> khalifsItems) {
        this.context = context;
        this.khalifsItems = khalifsItems;
    }

    @Override
    public int getCount() {
        return khalifsItems.size();
    }

    @Override
    public Object getItem(int i) {
        return khalifsItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (inflater == null) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        /*==================================================================*/
        Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/HelveticaNeueLTStd_Roman.otf");
        Typeface typegold = Typeface.createFromAsset(context.getAssets(), "fonts/HelveticaNeuecondenced.otf");
        /*==================================================================*/
        Khalifs khalif = khalifsItems.get(i);
        view = inflater.inflate(R.layout.khalif_item, null);
        img_khalif = (ImageView) view.findViewById(R.id.img_sect_actualite_text);
        name_khalif = (TextView) view.findViewById(R.id.txt_title_actualite_text);
        periode = (TextView) view.findViewById(R.id.txt_time_khalifat);
        dev_khalif = (TextView) view.findViewById(R.id.txt_dev_actualite_text);
        /*==================================================================*/
        name_khalif.setTypeface(typegold);
        periode.setTypeface(typeFace);
        dev_khalif.setTypeface(typeFace);
        /*==================================================================*/
        String link_img = khalif.getPhoto();
        Glide.with(context).load(link_img).diskCacheStrategy(DiskCacheStrategy.ALL).into(img_khalif);
        name_khalif.setText(khalif.getNom());
        dev_khalif.setText(khalif.getDescription());
        if (!khalif.getDate_out().equals("NULL")) {
            periode.setText(khalif.getDate_in() + "-" + khalif.getDate_out());
        }else{
            periode.setText("Khalif actuel");
        }
        /*==================================================================*/
        return view;
    }
}
