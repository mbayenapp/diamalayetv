package com.mbayennapp.diamalayetv.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ahmadrosid.svgloader.SvgLoader;
import com.mbayennapp.diamalayetv.Model.Indicatifs;
import com.mbayennapp.diamalayetv.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class Indicatif_Adapter extends BaseAdapter {
    private Context context;
    private Activity activity;
    private LayoutInflater inflater;
    private List<Indicatifs> indicatifs;
    /*======================================================================*/

    public Indicatif_Adapter(Context context,Activity activity, List<Indicatifs> indicatifs) {
        this.context = context;
        this.indicatifs = indicatifs;
        this.activity = activity;
    }
    /*======================================================================*/

    @Override
    public int getCount() {
        return indicatifs.size();
    }

    @Override
    public Object getItem(int position) {
        return indicatifs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        /*======================================================================*/
        if (inflater == null)
            inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.indicatif_item,null);
        /*======================================================================*/
        TextView txt_pays = (TextView) convertView.findViewById(R.id.txt_pays);
        CircleImageView img_down = (CircleImageView) convertView.findViewById(R.id.img_drapeau);
        /*======================================================================*/
        Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/HelveticaNeueLTStd_Roman.otf");
        Typeface typegold = Typeface.createFromAsset(context.getAssets(), "fonts/HelveticaNeuecondenced.otf");
        txt_pays.setTypeface(typeFace);
        /*======================================================================*/
        Indicatifs indicatif = indicatifs.get(position);
        txt_pays.setText(indicatif.getIntitule());
        Uri uri = Uri.parse(indicatif.getDrapeau());
        //Glide.with(context).load(uri).centerCrop().crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(img_down);

        SvgLoader.pluck()
                .with(activity)
                .setPlaceHolder(R.mipmap.ic_launcher, R.mipmap.ic_launcher)
                .load(indicatif.getDrapeau(), img_down);
        /*======================================================================*/
        return convertView;
    }
}
