package com.mbayennapp.diamalayetv.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mbayennapp.diamalayetv.Model.Sites;
import com.mbayennapp.diamalayetv.R;

import java.util.List;

/**
 * Created by Mbaye on 03/10/2017.
 */

public class Site_Adapter extends BaseAdapter {
    private String lien = "https://www.mabcompany.com/JL_API/";
    private String token = "layenne2017";
    private Context context;
    private List<Sites> sites;
    private LayoutInflater inflater;
    private ImageView img_khalif;
    private TextView name_khalif,dev_khalif;

    public Site_Adapter(Context context, List<Sites> sites) {
        this.context = context;
        this.sites = sites;
    }

    @Override
    public int getCount() {
        return sites.size();
    }

    @Override
    public Object getItem(int i) {
        return sites.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (inflater == null) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        /*==================================================================*/
        Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/HelveticaNeueLTStd_Roman.otf");
        Typeface typegold = Typeface.createFromAsset(context.getAssets(), "fonts/HelveticaNeuecondenced.otf");
        /*==================================================================*/
        Sites site = sites.get(i);
        view = inflater.inflate(R.layout.site_item, null);
        img_khalif = (ImageView) view.findViewById(R.id.img_sect_actualite_text);
        name_khalif = (TextView) view.findViewById(R.id.txt_title_actualite_text);
        dev_khalif = (TextView) view.findViewById(R.id.txt_dev_actualite_text);
        /*==================================================================*/
        name_khalif.setTypeface(typegold);
        dev_khalif.setTypeface(typeFace);
        /*==================================================================*/
        String link_img = site.getImage();
        Glide.with(context).load(link_img).centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL).into(img_khalif);
        name_khalif.setText(site.getTitre());
        dev_khalif.setText(site.getDescription());
        /*==================================================================*/
        return view;
    }
}
