package com.mbayennapp.diamalayetv.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mbayennapp.diamalayetv.Model.Comments;
import com.mbayennapp.diamalayetv.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Mbaye on 01/06/2018.
 */

public class Comment_Adapter extends RecyclerView.Adapter<Comment_Adapter.MyViewHolder> {
    private Context context;
    private List<Comments>  comments;
    private LayoutInflater inflater;

    public Comment_Adapter(Context context, List<Comments> comments) {
        this.context = context;
        this.comments = comments;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.comment_item, parent, false);

        return new Comment_Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Comments comment = comments.get(position);
        Glide.with(context).load(comment.getProfil()).centerCrop().crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.img_profil_user_comment);
        holder.txt_message.setText(comment.getMessage());
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return comments.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder{
        public ImageView img_profil_user_comment;
        public TextView txt_message;
        public CardView card_video;
        public MyViewHolder(View itemView) {
            super(itemView);
            img_profil_user_comment = (CircleImageView)itemView.findViewById(R.id.img_profil_user_comment);
            txt_message = (TextView)itemView.findViewById(R.id.txt_message);
            Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/HelveticaNeueLTStd_Roman.otf");
            Typeface typegold = Typeface.createFromAsset(context.getAssets(), "fonts/HelveticaNeuecondenced.otf");
            txt_message.setTypeface(typeFace);
        }

    }
}
