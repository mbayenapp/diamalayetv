package com.mbayennapp.diamalayetv.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mbayennapp.diamalayetv.Activity.Rubrique;
import com.mbayennapp.diamalayetv.Model.Rubriques;
import com.mbayennapp.diamalayetv.R;

import java.util.List;

/**
 * Created by bfali on 03/12/2016.
 */

public class Rubrique_Adapter extends RecyclerView.Adapter<Rubrique_Adapter.MyViewHolder> {
    private static final String TAG = Rubrique_Adapter.class.getSimpleName();
    private Activity context;
    private List<Rubriques> rubriquesItems;

    public Rubrique_Adapter(Activity context, List<Rubriques> rubriquesItems) {
        this.context = context;
        this.rubriquesItems = rubriquesItems;
    }

    @Override
    public Rubrique_Adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rubrique_item, parent, false);

        return new Rubrique_Adapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(Rubrique_Adapter.MyViewHolder holder, int position) {
        final Rubriques rubrique = rubriquesItems.get(position);
        holder.txt_titre_video.setText(rubrique.getNom());
        String link_img = rubrique.getImage();
        Glide.with(context).load(link_img).crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.img_video);
        holder.card_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context.getApplicationContext(),Rubrique.class);
                intent.putExtra("categorie",rubrique.getNom());
                context.startActivity(intent);
                context.overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                context.finish();
            }
        });
        holder.img_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context.getApplicationContext(),Rubrique.class);
                intent.putExtra("categorie",rubrique.getNom());
                context.startActivity(intent);
                context.overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                context.finish();
            }
        });
    }


    @Override
    public int getItemCount() {
        return rubriquesItems.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public ImageView img_video;
        public TextView txt_titre_video;
        public CardView card_video;
        public MyViewHolder(View itemView) {
            super(itemView);
            img_video = (ImageView)itemView.findViewById(R.id.img_tv_rubrique);
            txt_titre_video = (TextView)itemView.findViewById(R.id.txt_nom_tv_runbrique);
            card_video = (CardView)itemView.findViewById(R.id.card_tv);
            Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/HelveticaNeueLTStd_Roman.otf");
            Typeface typegold = Typeface.createFromAsset(context.getAssets(), "fonts/HelveticaNeuecondenced.otf");
            txt_titre_video.setTypeface(typegold);
        }

    }

}
