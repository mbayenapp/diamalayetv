package com.mbayennapp.diamalayetv.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mbayennapp.diamalayetv.Model.Sermon;
import com.mbayennapp.diamalayetv.R;

import java.util.List;

/**
 * Created by Mbaye on 02/10/2017.
 */

public class Sermons_Adapter extends BaseAdapter {
    private Context context;
    private List<Sermon> sermonsItems;
    private LayoutInflater inflater;
    private int init_drag = 0;

    public Sermons_Adapter(Context context, List<Sermon> sermonsItems) {
        this.context = context;
        this.sermonsItems = sermonsItems;
    }

    @Override
    public int getCount() {
        return sermonsItems.size();
    }

    @Override
    public Object getItem(int i) {
        return sermonsItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (inflater == null)
            inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null)
            view = inflater.inflate(R.layout.sermon_item,null);
        /*=================================================================*/
        final LinearLayout l_dev_sermon = (LinearLayout)view.findViewById(R.id.l_dev_sermon);
        LinearLayout l_titre_sermon = (LinearLayout)view.findViewById(R.id.l_titre_sermon);
        LinearLayout l_sect_icon = (LinearLayout)view.findViewById(R.id.l_sect_icon);
        TextView txt_title = (TextView)view.findViewById(R.id.txt_title_sermons);
        TextView txt_introduction = (TextView) view.findViewById(R.id.txt_introduction_sermons);
        TextView txt_title_intitule = (TextView) view.findViewById(R.id.txt_titre_intitule_sermons);
        TextView txt_dev_intitule = (TextView) view.findViewById(R.id.txt_intitule_sermons);
        TextView txt_titre_sermon = (TextView) view.findViewById(R.id.txt_titre_sermon);
        TextView txt_content = (TextView) view.findViewById(R.id.txt_content_sermons);
        final ImageView img_down = (ImageView)view.findViewById(R.id.img_dev_sermon);
        final ImageView img_up = (ImageView)view.findViewById(R.id.img_up_sermon);
        /*=================================================================*/
        Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/HelveticaNeueLTStd_Roman.otf");
        Typeface typegold = Typeface.createFromAsset(context.getAssets(), "fonts/HelveticaNeuecondenced.otf");
        /*=================================================================*/
        txt_title.setTypeface(typegold);
        txt_introduction.setTypeface(typeFace);
        txt_title_intitule.setTypeface(typegold);
        txt_dev_intitule.setTypeface(typeFace);
        txt_titre_sermon.setTypeface(typegold);
        txt_content.setTypeface(typeFace);
        /*=================================================================*/
        Sermon sermon = sermonsItems.get(i);
        txt_title.setText(sermon.getTitre());
        txt_introduction.setText(sermon.getIntroduction());
        txt_title_intitule.setText(sermon.getTitre_intitule());
        txt_dev_intitule.setText(sermon.getText_intitule());
        txt_titre_sermon.setText(sermon.getTitre_sermon());
        txt_content.setText(sermon.getSermon());
        /*=================================================================*/
        l_titre_sermon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (init_drag == 0) {
                    init_drag = 1;
                    l_dev_sermon.setVisibility(View.VISIBLE);
                    img_down.setVisibility(View.GONE);
                    img_up.setVisibility(View.VISIBLE);
                }else {
                    init_drag = 0;
                    l_dev_sermon.setVisibility(View.GONE);
                    img_down.setVisibility(View.VISIBLE);
                    img_up.setVisibility(View.GONE);
                }
            }
        });
        l_sect_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (init_drag == 0) {
                    init_drag = 1;
                    l_dev_sermon.setVisibility(View.VISIBLE);
                    img_down.setVisibility(View.GONE);
                    img_up.setVisibility(View.VISIBLE);
                }else {
                    init_drag = 0;
                    l_dev_sermon.setVisibility(View.GONE);
                    img_down.setVisibility(View.VISIBLE);
                    img_up.setVisibility(View.GONE);
                }
            }
        });
        /*img_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                l_dev_sermon.setVisibility(View.VISIBLE);
                img_down.setVisibility(View.GONE);
                img_up.setVisibility(View.VISIBLE);
            }
        });
        img_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                l_dev_sermon.setVisibility(View.GONE);
                img_down.setVisibility(View.VISIBLE);
                img_up.setVisibility(View.GONE);
            }
        });*/
        /*=================================================================*/
        return view;
    }
}
