package com.mbayennapp.diamalayetv.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mbayennapp.diamalayetv.Activity.start_video;
import com.mbayennapp.diamalayetv.Model.Videos;
import com.mbayennapp.diamalayetv.R;

import java.util.List;

/**
 * Created by Mbaye on 11/09/2017.
 */

public class Videotheque_Adapter extends RecyclerView.Adapter<Videotheque_Adapter.MyViewHolder> {
    private static final String TAG = Videotheque_Adapter.class.getSimpleName();
    private Context context;
    private List<Videos> videosItems;

    public Videotheque_Adapter(Context context, List<Videos> videosItems) {
        this.context = context;
        this.videosItems = videosItems;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.videotheque_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Videos video = videosItems.get(position);
        holder.txt_titre_video.setText(video.getTitre());
        holder.txt_categorie.setText(video.getCategorie());
        String link_img = video.getImage();
        Glide.with(context).load(link_img).centerCrop().crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.img_video);
        holder.card_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, start_video.class);
                intent.putExtra("id",video.getId());
                intent.putExtra("date_sent",video.getDate_sent());
                intent.putExtra("titre",video.getTitre());
                intent.putExtra("content",video.getDescription());
                intent.putExtra("photo",video.getImage());
                intent.putExtra("categorie",video.getCategorie());
                intent.putExtra("id_video",video.getId_video());
                intent.putExtra("vdid",video.getVdid());
                context.startActivity(intent);
            }
        });
        holder.img_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, start_video.class);
                intent.putExtra("id",video.getId());
                intent.putExtra("date_sent",video.getDate_sent());
                intent.putExtra("titre",video.getTitre());
                intent.putExtra("content",video.getDescription());
                intent.putExtra("photo",video.getImage());
                intent.putExtra("categorie",video.getCategorie());
                intent.putExtra("id_video",video.getId_video());
                intent.putExtra("vdid",video.getVdid());
                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return videosItems.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public ImageView img_video;
        public TextView txt_titre_video,txt_categorie;
        public CardView card_video;
        public MyViewHolder(View itemView) {
            super(itemView);
            img_video = (ImageView)itemView.findViewById(R.id.image_video);
            txt_titre_video = (TextView)itemView.findViewById(R.id.title_video);
            txt_categorie = (TextView)itemView.findViewById(R.id.categorie);
            card_video = (CardView)itemView.findViewById(R.id.mcard_video);
            Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/HelveticaNeueLTStd_Roman.otf");
            Typeface typegold = Typeface.createFromAsset(context.getAssets(), "fonts/HelveticaNeuecondenced.otf");
            txt_titre_video.setTypeface(typeFace);
            txt_categorie.setTypeface(typeFace);
        }

    }
}
