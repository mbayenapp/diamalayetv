package com.mbayennapp.diamalayetv.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mbayennapp.diamalayetv.Model.Mahdi;
import com.mbayennapp.diamalayetv.R;

import java.util.List;

/**
 * Created by bfali on 27/01/2017.
 */

public class Mahdi_Adapter extends BaseAdapter {
    private Context context;
    private List<Mahdi> mahdis;
    private LayoutInflater inflater;

    public Mahdi_Adapter(Context context, List<Mahdi> mahdis) {
        this.context = context;
        this.mahdis = mahdis;
    }

    @Override
    public int getCount() {
        return mahdis.size();
    }

    @Override
    public Object getItem(int i) {
        return mahdis.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (inflater == null)
            inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null)
            view = inflater.inflate(R.layout.mahdi_item,null);
        TextView txt_titre = (TextView)view.findViewById(R.id.txt_titre_mahdi);
        TextView txt_content = (TextView)view.findViewById(R.id.txt_content_mahdi);
        /*=================================================================*/
        Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/HelveticaNeueLTStd_Roman.otf");
        Typeface typegold = Typeface.createFromAsset(context.getAssets(), "fonts/HelveticaNeuecondenced.otf");
        txt_content.setTypeface(typeFace);
        txt_titre.setTypeface(typegold);
            /*=================================================================*/
        final Mahdi mahdi = mahdis.get(i);
        txt_titre.setText(mahdi.getTitre());
        txt_content.setText(mahdi.getContent());
        return view;
    }
}
