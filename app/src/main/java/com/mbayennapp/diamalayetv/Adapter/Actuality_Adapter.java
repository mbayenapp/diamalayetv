package com.mbayennapp.diamalayetv.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mbayennapp.diamalayetv.Helpers.SessionManagerLogin;
import com.mbayennapp.diamalayetv.Model.Articles;
import com.mbayennapp.diamalayetv.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mbaye on 17/08/2017.
 */

public class Actuality_Adapter extends BaseAdapter {
    private static final String TAG = "My_Tag";
    private Context context;
    private List<Articles> articlesItems;
    private LayoutInflater inflater;
    private String lien = "http://www.mabcompany.com/JL_API/";
    private String token = "layenne2017";
    private List<String> rep_is_my_f = new ArrayList<>();
    private String rep = "";
    private SessionManagerLogin sessionManagerLogin;
    private String result,res;
    ImageView img_act;
    TextView dev_act_texte,txt_description;

    public Actuality_Adapter(Context context, List<Articles> articlesItems) {
        this.context = context;
        this.articlesItems = articlesItems;
    }

    @Override
    public int getCount() {
        return articlesItems.size();
    }

    @Override
    public Object getItem(int position) {
        return articlesItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (inflater == null) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/HelveticaNeueLTStd_Roman.otf");
        Typeface typegold = Typeface.createFromAsset(context.getAssets(), "fonts/HelveticaNeuecondenced.otf");
        final Articles article = articlesItems.get(i);
        sessionManagerLogin = new SessionManagerLogin(context);
        if (article.getType().equals("article")) {
            view = inflater.inflate(R.layout.actualite_texte_item, null);
            img_act = (ImageView) view.findViewById(R.id.img_sect_actualite_text);
            dev_act_texte = (TextView) view.findViewById(R.id.txt_dev_actualite_text);
            txt_description = (TextView)view.findViewById(R.id.txt_description);
        }else {
            view = inflater.inflate(R.layout.actualite_video_item,null);
            img_act = (ImageView) view.findViewById(R.id.img_sect_actualite);
            dev_act_texte = (TextView) view.findViewById(R.id.txt_dev_actualite_video);
        }

        /*============================================================*/
        dev_act_texte.setTypeface(typegold);
        if (article.getType().equals("article")) {
            txt_description.setTypeface(typeFace);
        }
        /*============================================================*/
        String link_img = article.getPhoto();
        Glide.with(context).load(link_img).centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL).into(img_act);
        dev_act_texte.setText(article.getTitre());
        if (article.getType().equals("article")) {
            txt_description.setText(article.getContent());
        }
        return view;
    }

    /*=======================================================================*/
    /*=======================================================================*/
}
