package com.mbayennapp.diamalayetv.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mbayennapp.diamalayetv.Model.Articles;
import com.mbayennapp.diamalayetv.R;

import java.util.List;

import jp.wasabeef.glide.transformations.BlurTransformation;

/**
 * Created by Mbaye on 18/08/2017.
 */

public class Video_Adapter extends BaseAdapter {
    private String lien = "http://www.mabcompany.com/JL_API/";
    private Context context;
    private List<Articles> videosItems;
    private LayoutInflater inflater;

    public Video_Adapter(Context context, List<Articles> videosItems) {
        this.context = context;
        this.videosItems = videosItems;
    }

    @Override
    public int getCount() {
        return videosItems.size();
    }

    @Override
    public Object getItem(int i) {
        return videosItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (inflater == null) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        view = inflater.inflate(R.layout.video_item, null);
        final Articles article = videosItems.get(i);
        ImageView img_act;
        TextView lieu_act_texte,dev_act_texte;
        img_act = (ImageView) view.findViewById(R.id.img_article_video);
        lieu_act_texte = (TextView) view.findViewById(R.id.txt_title_actualite_video);
        /*=============================================================*/
        Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/century-gothic.ttf");
        Typeface typegold = Typeface.createFromAsset(context.getAssets(), "fonts/century-gothic-gold.ttf");
        lieu_act_texte.setTypeface(typegold);
        //dev_act_texte.setTypeface(typeFace);
        /*=============================================================*/
        Glide.with(context).load(article.getPhoto()).crossFade().bitmapTransform(new BlurTransformation(context,3)).diskCacheStrategy(DiskCacheStrategy.ALL).into(img_act);
        lieu_act_texte.setText(article.getTitre());
        return view;
    }
}
