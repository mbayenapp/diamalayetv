package com.mbayennapp.diamalayetv.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mbayennapp.diamalayetv.Model.Videos;
import com.mbayennapp.diamalayetv.R;

import java.util.List;

/**
 * Created by Mbaye on 12/09/2017.
 */

public class Other_Video_Adapter  extends BaseAdapter{
    private Context context;
    private List<Videos> articlesItems;
    private LayoutInflater inflater;

    public Other_Video_Adapter(Context context, List<Videos> articlesItems) {
        this.context = context;
        this.articlesItems = articlesItems;
    }

    @Override
    public int getCount() {
        return articlesItems.size();
    }

    @Override
    public Object getItem(int i) {
        return articlesItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if (inflater == null) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        view = inflater.inflate(R.layout.other_video_item, null);
        final Videos article = articlesItems.get(i);
        ImageView img_act;
        TextView lieu_act_texte,dev_act_texte;
        img_act = (ImageView) view.findViewById(R.id.img_video);
        lieu_act_texte = (TextView) view.findViewById(R.id.txt_title_other_video);
        dev_act_texte = (TextView) view.findViewById(R.id.txt_title_content_video);
        /*=============================================================*/
        Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/HelveticaNeueLTStd_Roman.otf");
        Typeface typegold = Typeface.createFromAsset(context.getAssets(), "fonts/HelveticaNeuecondenced.otf");
        lieu_act_texte.setTypeface(typegold);
        dev_act_texte.setTypeface(typeFace);
        /*=============================================================*/
        String link_img = article.getImage();
        Glide.with(context).load(link_img).centerCrop().crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(img_act);
        lieu_act_texte.setText(article.getTitre());
        dev_act_texte.setText(article.getDescription());
        return view;
    }
}
