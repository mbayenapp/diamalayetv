package com.mbayennapp.diamalayetv.Utils;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.LayoutInflater;

import com.google.firebase.messaging.RemoteMessage;
import com.mbayennapp.diamalayetv.Activity.Home;
import com.mbayennapp.diamalayetv.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Libasse on 5/23/2016.
 */
public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService{
    String statut = "";
    NotificationManager manager;
    private static final int NOTIFICATION_ID = 002;
    private NotificationManager mNotificationManager;
    private static final String TAG ="MyFirebaseMsgService";
    String title;
    LayoutInflater inflater;
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        String j_data = null;
        JSONArray j_don_course = null;
        JSONObject j_object_course = null;
        if (remoteMessage.getData().size() > 0){
            try {
                if (remoteMessage.getData().get("course") != null) {
                    j_object_course = new JSONObject(remoteMessage.getData().get("message"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d(TAG,"Données du message: "+ remoteMessage.getData().get("course"));
        }


        showNotification(remoteMessage.getData().get("message"));
        //buildNotification(remoteMessage.getData().get("message"));
        //statut = remoteMessage.getData().get("message");
    }

    private void showNotification(String message) {
        Intent i;
        if (isActivityRunning(getApplicationContext())){
            i = new Intent();
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }else {
            i = new Intent(getApplicationContext(), Home.class);
            i.putExtra("active","no");
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(i);
        }

        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,i, PendingIntent.FLAG_UPDATE_CURRENT);
        RingtoneManager ringtoneManager = new RingtoneManager(getApplicationContext());
        Uri defaultSoundUri = ringtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setContentText(message)
                .setPriority(Notification.PRIORITY_MAX)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent).setShowWhen(true);

        manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        manager.notify(002,builder.build());
    }






    public boolean isActivityRunning(Context ctx) {
        ActivityManager activityManager = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = activityManager.getRunningTasks(Integer.MAX_VALUE);

        for (ActivityManager.RunningTaskInfo task : tasks) {
            if (ctx.getPackageName().equalsIgnoreCase(task.baseActivity.getPackageName()))
                return true;
        }

        return false;
    }
}
